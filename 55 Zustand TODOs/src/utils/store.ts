import { create } from "zustand";
import { persist } from "zustand/middleware";

interface todoType {
  title: string;
  id: string;
}

interface deleteTodoType {
  id: string;
}

interface TodoStoreType {
  todos: todoType[];
  addTodo: (newTodo: todoType) => void;
  deleteTodo: (deleteTodo: deleteTodoType) => void;
}

export const useStore = create<TodoStoreType>()(
  persist(
    (set, get) => ({
      todos: [],
      addTodo: (newTodo: todoType) =>
        set({
          todos: [...get().todos, newTodo],
        }),
      deleteTodo: (deleteTodo: deleteTodoType) =>
        set((state) => ({
          todos: state.todos.filter(({ id }) => id !== deleteTodo.id),
        })),
    }),
    {
      name: "zustand-todos",
    }
  )
);
