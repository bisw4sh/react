import { Accordion, AccordionItem, Button, Checkbox } from "@nextui-org/react";
import { useStore } from "../utils/store";

interface TaskType {
  title: string;
  id: string;
}

export default function TaskAcc() {
  const remove = useStore((state) => state.deleteTodo);
  const tasks = useStore((state) => state.todos);

  return (
    <div className="w-full flex flex-col justify-center items-center pt-4 px-4 md:px-16">
      <Accordion>
        {tasks.map(({ title, id }: TaskType) => {
          return (
            <AccordionItem key={id} aria-label={title} title={title}>
              <div className="flex max-md:flex-col justify-between items-center gap-1">
                <div>{id}</div>

                <div className="space-x-2">
                  <Button
                    color="danger"
                    variant="bordered"
                    onClick={() => remove({ id })}
                  >
                    Delete
                  </Button>

                  <Checkbox
                    defaultSelected={false}
                    color="success"
                    lineThrough={true}
                  >
                    Disable
                  </Checkbox>
                </div>
              </div>
            </AccordionItem>
          );
        })}
      </Accordion>
    </div>
  );
}
