import { Divider } from "@nextui-org/react";
import { Link, Outlet } from "react-router-dom";
import ThemeSwitcher from "./ThemeSwitcher";

export default function App() {
  return (
    <>
      <div className="w-full p-4 flex justify-between text-2xl font-semibold underline-offset-4 drop-shadow-2xl	 shadow-lg">
        <div className="flex h-5 items-center space-x-4 text-small">
          <Link
            to="/"
            className="cursor-pointer hover:decoration-wavy hover:underline"
          >
            Home
          </Link>
          <Divider orientation="vertical" />
          <Link
            to="/signin"
            className="cursor-pointer hover:decoration-wavy hover:underline"
          >
            Sign In
          </Link>
          <Divider orientation="vertical" />
          <Link
            to="/signup"
            className="cursor-pointer hover:decoration-wavy hover:underline"
          >
            Sign Up
          </Link>
        </div>
        <div>
          <ThemeSwitcher />
        </div>
      </div>
      <Outlet />
    </>
  );
}
