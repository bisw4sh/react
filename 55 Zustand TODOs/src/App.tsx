import { Input, Button } from "@nextui-org/react";
import { useState, useRef } from "react";
import { v4 as uuidv4 } from "uuid";
import TaskAcc from "./component/TaskAcc";
import { useStore } from "./utils/store";

interface TaskType {
  title: string;
  id: string;
}

export default function App() {
  const [newTodo, setNewTodo] = useState<TaskType>({ title: "", id: "" });
  const add = useStore((state) => state.addTodo);
  const inputRef = useRef<HTMLInputElement>(null!);

  // const setTasks =
  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    setNewTodo({ ...newTodo, title: e.currentTarget.value });
  };

  const handleSubmit = (
    e: React.SyntheticEvent<HTMLFormElement, SubmitEvent>
  ) => {
    e.preventDefault();
    add({ ...newTodo, id: uuidv4() });
    setNewTodo({ title: "", id: "" });
    inputRef?.current?.focus();
  };

  return (
    <div className={`relative px-4 md:px-16 py-4`}>
      <h1 className="text-3xl font-bold text-center">TODOs App</h1>
      <form
        action=""
        onSubmit={handleSubmit}
        className="flex gap-5 justify-center items-center"
      >
        <Input
          type="text"
          variant={"underlined"}
          label="Enter your task"
          labelPlacement="outside"
          className="w-2/3"
          value={newTodo?.title}
          onChange={handleChange}
          isRequired={true}
          ref={inputRef}
        />
        <Button
          color="default"
          variant="ghost"
          radius="sm"
          fullWidth={false}
          type="submit"
        >
          Add
        </Button>
      </form>
      <TaskAcc />
    </div>
  );
}
