import { useState, useEffect } from "react";
import { useQuery } from "@tanstack/react-query";
import fetchPokimons from "./utility/fetchPokimons";
import CardCustom from "./comps/CardCustom";
import { Input } from "@/components/ui/input";

export default function App() {
  const [filterPokemon, setFilterPokemon] = useState<string>("");
  const [pokeDetails, setPokeDetails] = useState<PokemonDetails[]>([]);
  const POKEMON_FETCH_URL = import.meta.env.VITE_POKI_URI as string;

  const { data, isLoading, isError } = useQuery<Pokemon[]>({
    queryKey: ["pokemons", POKEMON_FETCH_URL],
    queryFn: () => fetchPokimons(POKEMON_FETCH_URL),
    staleTime: 60 * 60 * 1000, // 1 hr
  });

  useEffect(() => {
    if (data) {
      setPokeDetails(data);
    }
  }, [data]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilterPokemon(event.target.value);
  };

  const filtered_by_keywords = (keywords: string): PokemonDetails[] => {
    return (
      pokeDetails?.filter((single_poke) => {
        return (
          single_poke!.name!.toLowerCase().includes(keywords.toLowerCase()) ||
          single_poke!.type!.some((t) =>
            t.toLowerCase().includes(keywords.toLowerCase())
          )
        );
      }) || []
    );
  };

  const filteredPokemons = filtered_by_keywords(filterPokemon);

  return (
    <main className="min-h-screen bg-gradient-to-r from-indigo-500 to-fuchsia-600 p-2 flex flex-col gap-2 justify-center items-center">
      <h1 className="text-3xl font-bold underline">Pokémon Arena</h1>
      <Input
        className="w-1/2 placeholder-rose-500"
        placeholder="Enter keywords to filter"
        value={filterPokemon}
        onChange={handleInputChange}
      />

      <div>
        {isLoading && <p>Loading...</p>}
        {isError && <p>Error loading data</p>}
        {data && (
          <section className="flex gap-2 flex-wrap justify-center items-center">
            {filteredPokemons.map(
              (
                { name, image_url, type }: Omit<PokemonDetails, "url">,
                idx: number
              ) => {
                return (
                  <CardCustom
                    name={name}
                    image_url={image_url}
                    type={type}
                    key={idx}
                  />
                );
              }
            )}
          </section>
        )}
      </div>
    </main>
  );
}
