const fetchPokimons = async (poke_url: string): Promise<Pokemon[]> => {
  try {
    const response = await fetch(poke_url);
    if (!response.ok) {
      throw new Error(`Failed to fetch data: ${response.statusText}`);
    }

    const poki_data: PokemonApiResponse = await response.json();

    const sanitized_data: Pokemon[] = await Promise.all(
      poki_data.results.map(async ({ url, name }) => {
        const poki_url = url.split("/");
        const poki_index = poki_url[poki_url.length - 2];
        const image_url = `${import.meta.env.VITE_POKI_IMG}${poki_index}.png`;

        // Getting the type information
        const type_response = await fetch(url);
        if (!type_response.ok) {
          throw new Error(
            `Failed to fetch type data: ${type_response.statusText}`
          );
        }

        const type_obj = await type_response.json();
        const sanitized_types = type_obj.types.map(
          (typeInfo: PokemonType) => typeInfo.type.name
        );

        return { name, image_url, url, type: sanitized_types };
      })
    );

    return sanitized_data;
  } catch (error) {
    console.error("Error fetching Pokemons:", error);
    throw error;
  }
};

export default fetchPokimons;
