import { useEffect } from 'react';
import useStorage from '../hooks/useStorage.jsx';
import { motion } from 'framer-motion';
import PropTypes from 'prop-types';

const ProgressBar = ({ file, setFile }) => {
  const { progress, url } = useStorage(file);

  useEffect(() => {
    if (url) {
      setFile(null);
    }
  }, [url, setFile]);

  return (
    <motion.div className="progress-bar"
      initial={{ width: 0 }}
      animate={{ width: progress + '%' }}
    ></motion.div>
  );
}

ProgressBar.propTypes = {
  file: PropTypes.object.isRequired, // Adjust the type accordingly based on the actual data type of 'file'.
  setFile: PropTypes.func.isRequired,
};

export default ProgressBar;