// Import the functions you need from the SDKs you need
import * as firebase from "firebase/app";
import 'firebase/compat/storage'
import 'firebase/compact/firegram'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBWsJLQyjYKP6V7nGe2dzRUrKohJB30sZs",
  authDomain: "firegram-154fb.firebaseapp.com",
  projectId: "firegram-154fb",
  storageBucket: "firegram-154fb.appspot.com",
  messagingSenderId: "703787583434",
  appId: "1:703787583434:web:a459beaffd34d4464db133"
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig)

firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFirestore, timestamp};
