import { useState } from 'react'

const InputFields = ({states, setStates}) => {

    const numbers = '1234567890'
    const alphaNUMERIC = 'abcdefghijklmnopqrstuvwxyz'
    const alphanumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    const special = '!@#$%^&*()[]{}:;<>/?'

    const [result, setResult] = useState('Result Here')

    const {length, uppercase, lowercase, number, symbols } = states

    const handleInputChange = (e) => {
        const { name, value, type, checked } = e.target;
        setStates((prevState) => ({
            ...prevState,
            [name]: type === 'checkbox' ? checked : value,
        }))
        }

        const handleResult = () => {
    let strtouse = '';
    setResult('');

    if (uppercase) {
        strtouse +=  alphaNUMERIC;
    }

    if (lowercase) {
        strtouse += alphanumeric;
    }

    if (symbols) {
        strtouse += special;
    }

    if (number) {
        strtouse += numbers;
    }

    // Generate the password using strtouse and the specified length
    let password = '';
    for (let i = 0; i < Number(length); i++) {
        const randomIndex = Math.floor(Math.random() * strtouse.length);
        password += strtouse[randomIndex];
    }

    setResult(password);
    }

    const handleClick = (e) => {
        e.preventDefault()
        setStates([{length: 10,
            uppercase: false,
            lowercase: false,
            number: false,
            symbols : false }])
            handleResult()
    }

  return (
    <div className="InputFields">

      <h2>Password Generator</h2>

      <label htmlFor="display">
        <input type="text" id="display" placeholder={result} disabled />
      </label>

      <label htmlFor="length">
        Password Length:
        <input
          type="number"
          value={length}
          name="length"
          id="length"
          onChange={handleInputChange}
        />
      </label>

      <label htmlFor="uppercase">
        Add Uppercase Letters:
        <input
          type="checkbox"
          checked={uppercase}
          name="uppercase"
          id="uppercase"
          onChange={handleInputChange}
        />
      </label>

      <label htmlFor="lowercase">
        Add Lowercase Letters:
        <input
          type="checkbox"
          checked={lowercase}
          name="lowercase"
          id="lowercase"
          onChange={handleInputChange}
        />
      </label>

      <label htmlFor="number">
        Include Numbers:
        <input
          type="checkbox"
          checked={number}
          name="number"
          id="number"
          onChange={handleInputChange}
        />
      </label>

      <label htmlFor="symbols">
        Include Symbols:
        <input
          type="checkbox"
          checked={symbols}
          name="symbols"
          id="symbols"
          onChange={handleInputChange}
        />
      </label>

      <button type="submit" className="btn" onClick={handleClick}>
        Generate Password
      </button>

    </div>
  )
}

export default InputFields