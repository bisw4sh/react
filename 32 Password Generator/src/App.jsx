import { useState } from 'react'
import InputFields from './InputFields.jsx'

const App = () => {


  const [states, setStates] = useState([{length: 10,
                                        uppercase: false,
                                        lowercase: false,
                                        number: false,
                                        symbols : false }])


  return (
    <>
      <InputFields states={states} setStates={setStates}/>
    </>
  )
}

export default App