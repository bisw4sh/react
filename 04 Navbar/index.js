const navbar = (
    <div className="navbar">
        <ul>
            <li>Home</li>
            <li>Blogs</li>
            <li>Contacts</li>
            <li>News</li>
            <li>Results</li>
        </ul>
    </div>
)
ReactDOM.render(navbar, document.getElementById('root'))