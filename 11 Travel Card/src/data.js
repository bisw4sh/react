export default [
    {
        title : "Mount Fuji",
        location : "Japan",
        googleMapsUrl : "https://goo.gl/maps/nzVkCvLSMYX1JzyK6",
        startDate : "12 Jan, 2021",
        endDate : "24 Jan, 2021",
        description : "Mount Fuji, Japanese Fuji-san, also spelled Fujisan, also called Fujiyama or Fuji no Yama, highest mountain in Japan. It rises to 12,388 feet (3,776 metres) near the Pacific Ocean coast in Yamanashi and Shizuoka ken (prefectures) of central Honshu, about 60 miles (100 km) west of the Tokyo-Yokohama metropolitan area.",
        imageUrl: "https://cdn.britannica.com/47/80547-050-8B316D38/Field-green-tea-Mount-Fuji-Shizuoka-prefecture.jpg"
    },
    {
        title : "Sydney Opera House",
        location : "Australia",
        googleMapsUrl : "https://www.google.com.np/travel/entity/key/ChQI0YTr0PWa_pkxGggvbS8wNl9ubRAE?hl=en&gl=np&gsas=1&sa=X&utm_campaign=sharing&ts=CAESABoECgIaACoECgAaAA&rp=OAFIAg&ap=MAA&ei=QnbfZKfjKMSOrcUP14G1wAw&ved=0CAAQ5JsGahcKEwiQ-KXIsamBAxUAAAAAHQAAAAAQAw&utm_medium=link_btn&utm_source=poi",
        startDate : "27 May, 2021",
        endDate : "8 Jun, 2021",
        description : "Sydney Opera House, opera house located on Port Jackson (Sydney Harbour), New South Wales, Australia. Its unique use of a series of gleaming white sail-shaped shells as its roof structure makes it one of the most-photographed buildings in the world.",
        imageUrl: "https://upload.wikimedia.org/wikipedia/commons/a/a0/Sydney_Australia._%2821339175489%29.jpg"
    },    
    {
        title : "Geinrangerfjord",
        location : "Norway",
        googleMapsUrl : "https://goo.gl/maps/wPngwU6GwrLAGnk69",
        startDate : "01 Oct, 2021",
        endDate : "18 Nov, 2021",
        description : "The Geirangerfjord is often considered the jewel in Norway's crown of fjords. Surrounded by majestic mountain peaks, 800-metre-high cliffs and cascading waterfalls, the UNESCO-listed fjord is an impressive 15 kilometres long and 260 metres deep.",
        imageUrl: "https://www.traveloffpath.com/wp-content/uploads/2023/07/Aerial-View-Of-A-Cruise-Ship-Entering-Geirangerfjord-In-Norway-Northern-Europe-Scandinavia.jpg"
    }
]