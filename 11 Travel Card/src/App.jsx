import data from "./data.js"
import "./Cards.css"

const Returner = (props) => {
  return (
  <div className="card">
    <div className="coverImg"><img src={props.imageUrl} alt="Image" className="cImg"/></div>
    <div className="information" key={props.imageUrl}>
      <i className="fa-solid fa-location-pin"></i> <span className="location">{props.location}</span> <a href={props.googleMapsUrl} className="locationUrl">View on googlemaps</a>
      <h3 className="title">{props.title}</h3>
      <section className="dates">{props.startDate} - {props.endDate}</section> 
      <span className="desc">{props.description}</span>
    </div>
  </div>
  )
}

function App() {
const notes = data.map( note => <Returner key={note.imageUrl} {...note} />)

  return (
    <div className="whole">
    {notes}
    </div>
  ) 
}

export default App