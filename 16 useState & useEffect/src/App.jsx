import React, { useState, useEffect } from 'react'
import "./App.css"

const App = () => {
  const [count, setCount] = useState(0)
  const [isChanged, setIsChanged] = useState(false)

  const handleIncrement = () => setCount( count + 1)
  const handleDecrement = () => setCount( count - 1)

  useEffect(() => {
    setIsChanged(!isChanged)
  }, [count])

  return (
    <div className={isChanged ? 'compo red' : 'compo blue'}>
      <button onClick={() => handleIncrement()}>+</button>
      <h1>{count}</h1>
      <button onClick={() => handleDecrement()}>-</button>
    </div>
  )

}
export default App