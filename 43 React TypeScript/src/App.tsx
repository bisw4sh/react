import { useState, useEffect } from "react";
import { MdDarkMode, MdSunny } from "react-icons/md";

function App() {
  const [theme, setTheme] = useState<string | null>(null);

  useEffect(() => {
    if (
      localStorage.theme === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }, []);

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    const newTheme = theme === "dark" ? "light" : "dark";
    setTheme(newTheme);
    localStorage.setItem("theme", newTheme);
  };

  return (
    <div className="h-screen relative dark:bg-black">
      <button
        onClick={handleThemeSwitch}
        className="absolute right-8 top-8 text-3xl"
      >
        {theme === "dark" ? (
          <MdDarkMode className="fill-blue-400" />
        ) : (
          <MdSunny className="fill-yellow-500" />
        )}
      </button>
    </div>
  );
}

export default App;