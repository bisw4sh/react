import "./App.css"

const HEX = "0123456789abcdef"

const randNum = () =>{
    let retstr = ''
    for(let i = 0 ; i < 6 ; i++){
      retstr += HEX[Math.floor(Math.random() * HEX.length)]  
  }
    return retstr
}

const Square = () => {
  let bgColor = randNum()
  let colorCode = `#${bgColor}`
  return (
    <div className="square" style={{backgroundColor:colorCode}}>#{bgColor}</div>
  )
}

const Box = () => {
  const squares = [];
  for (let i = 0; i < 32; i++) {
    squares.push(<Square key={i} index={i} />)
  }
  return squares;
}

const App = () => {
  return (
    <div className="Content">
      <h1>30 Days of React</h1>
      <h3>Hexadecimal Colors</h3>
      <div className="box">
        <Box />
      </div>
    </div>
  )
}

export default App