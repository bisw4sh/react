import "./App.css"
import profile from "./assets/br4ke.jpg"
import tick from "./assets/tick.png"

const Image = () => {
  return (
    <img src={profile} alt="PROFILE" className="profile"/>
  )
}

function App() {
  return (
  <>
  <Image />
  <div className="nameW"><h2>BISWASH DHUNGANA</h2><img src={tick} alt="tick" className="tick"/></div> 

  <h4>Software Engineer, Nepal</h4>
  <h2>SKILLS</h2>

  <div className="skills">
    <span>HTML</span><span>CSS</span><span>Sass</span><span>JS</span>
    <span>React</span><span>Redux</span><span>Node</span><span>MongoDB</span>
    <span>Python</span><span>Flask</span><span>Django</span><span>NumPy</span>
    <span>Pandas</span><span>Data Analysis</span><span>MYSQL</span><span>GraphQL</span>
    <span>D3.js</span><span>Gatsby</span><span>Docker</span><span>Heroku</span>
    <span>Git</span>
  </div>

  <div className="clockW"><i className="fa-regular fa-clock"></i> Joined on Aug 30, 2020</div>

  </>
  )
}

export default App