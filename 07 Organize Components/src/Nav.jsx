import List from "./List"
import ReactLogo from "./assets/react.svg"

const Nav = () => {
  return (
    <nav>
    <header>
    <img src={ReactLogo} alt="React Logo" className="logo"/>
    <List />
    </header>
</nav>
  )
}

export default Nav