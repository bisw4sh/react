import "./style.css"
import Nav from "./Nav.jsx"
import Desc from "./Desc.jsx"
import Footer from "./Footer.jsx"

function App() {
  return (
  <>
    <Nav />
    <Desc />
    <Footer />
  </>
  ) 
}

export default App