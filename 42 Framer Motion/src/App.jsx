import Page from './comp/Page.jsx'

const App = () => {

  return (
    <div className="h-max flex flex-col justify-center items-center bg-teal-300">

      <Page data={'First'}/>
      <Page data={'Second'}/>
      <Page data={'Third'}/>
      <Page data={'Fourth'}/>
      <Page data={'Fifth'}/>
      <Page data={'Sixth'}/>
      <Page data={'Seventh'}/>
      <Page data={'Eighth'}/>
      <Page data={'Nineth'}/>
      <Page data={'Tenth '}/>

    </div>
  )
}

export default App