# 1 - 11

Followed [this](https://youtu.be/bMknfKXIFA8?si=x1BHawJgYkm5aAlC)

# 13 - 17

Followed [this](https://github.com/Asabeneh/30-Days-Of-React)

# 18

Followed [this](https://tailwindcss.com/docs/guides/vite)

# 19

Followed [this](https://youtu.be/vUe91uOx7R0?si=T4onaD4hUpqzKJsG)

# 20

Followed [this](https://youtu.be/CHGHuF24Cjw?si=c6MH8KnOkZrdIbZq)

# 21 - 26

Made on own following the idea & design from [here](https://www.freecodecamp.org/news/react-projects-for-beginners-easy-ideas-with-code/)

# 27

Followed [this](https://youtu.be/59IXY5IDrBA?si=Ieb77YUzYfcudU0r)

# 29 - 32

Made on own following the idea & design from [here](https://www.interviewbit.com/blog/react-projects/)

# 33 - 34

Intellectual creation 🤣

# 35

[Tailwind Docs](https://tailwindcss.com/docs/installation)

# 36

They have their own respective documentation inside the directory

# 37

YouTube & Instagram Clone using Tailwind CSS with light/dark mode
Live at [YouTube](https://yt-biswash.netlify.app/)

# 38

Rock Paper Scissors
Live [at](https://rps-br4ke.netlify.app/)

# 39-47

Build on own to see

# 48

Live @ [todos](https://todos-bisw4sh.netlify.app/)

# 49

Context API [Docs](https://legacy.reactjs.org/docs/context.html)

# 50 - 53

React Router DOM [v6](https://reactrouter.com/en/main/routers/picking-a-router)

# 54

Zustand State Management Solution
[[RES-1](https://github.com/pmndrs/zustand) ----
[RES-2](https://docs.pmnd.rs/zustand/getting-started/introduction) ----
[RES-3](https://zustand-demo.pmnd.rs/) ----
[RES-4](https://refine.dev/blog/zustand-react-state/)]

# 55 - 58  

Run dev server to see the working.

# 59

[Live @](https://bisw4sh-multistage.netlify.app/)