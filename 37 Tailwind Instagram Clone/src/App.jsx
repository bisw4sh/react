import { useState, useEffect } from 'react'
import Left from './Components/Left.jsx';
import Feed from './Components/Feed.jsx';
import Right from './Components/Right.jsx';

export default function App() {

  const [theme, setTheme] = useState(null);

  useEffect(() => {
    if(localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)){
      setTheme('dark')
    }
    else {
      setTheme('light')
    }
  }, [])

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark")
    } else {
      document.documentElement.classList.remove("dark")
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem('theme', `${theme === "dark" ? "light" : "dark"}`)
  }

  return (
    <div className="h-fit dark:bg-black dark:text-gray-300 select-none flex flex-row justify-around">
      <Left handleThemeSwitch={handleThemeSwitch} />
      <Feed />
      <Right />
    </div>
  );

}