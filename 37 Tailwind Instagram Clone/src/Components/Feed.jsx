import Story from './Story.jsx';
import Posts from './Posts.jsx';

const Feed = () => {
  return (
    <div className='w-5/12 flex flex-col'>
        <Story />
        <Posts />
    </div>
  )
}

export default Feed