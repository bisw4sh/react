
const Right = () => {
  return (
    <div className=" w-fit mr-32 mt-8 max-xl:hidden cursor-pointer">

        <div className="flex justify-between items-center p-3 gap-1">

            <div className="profile-picture h-12 w-12 object-contain">
                <img src="https://avatars.githubusercontent.com/u/59287553?v=4"
                alt="" className="object-fill rounded-full"/>
            </div>

            <div>
            <div className="username font-semibold">bisw4sh</div>
            <div className="full-name text-gray-700 text-sm">Biswash Dhungana</div>
            </div>

            <div className="switch pl-16 text-sm text-blue-500 font-medium">Switch</div>
        </div>

        <div className="suggestion f-w flex justify-between mt-4">
            <span className="font-semibold text-gray-500">Suggested for you</span>
            <span className="font-medium">See All</span>
        </div>

        <div className="suggested-friend">
            {
                [
                    ['username_1','Suggested for you', 'https://randomuser.me/api/portraits/men/1.jpg'],
                    ['username_2','Followed by br4ke...', 'https://randomuser.me/api/portraits/men/2.jpg'],
                    ['username_3','Followed by bisw4sh...', 'https://randomuser.me/api/portraits/men/3.jpg'],
                    ['username_4','New to Instagram', 'https://randomuser.me/api/portraits/men/4.jpg'],
                    ['username_5','Suggested for you', 'https://randomuser.me/api/portraits/men/5.jpg'],
                ].map(([username, msg, img], index) => {
                    return (
                        <div className="flex justify-between items-center p-3 gap-1 cursor-pointer" key={index}>

                        <div className="profile-picture h-12 w-12 object-contain">
                            <img src={img}
                            alt="profile picture" className="object-fill rounded-full"/>
                        </div>
            
                        <div>
                        <div className="username font-semibold">{username}</div>
                        <div className="full-name text-gray-700 text-sm">{msg}</div>
                        </div>
            
                        <div className="switch pl-16 text-sm text-blue-500 font-medium">Follow</div>
                    </div>
                    )
                }

                )
            }
        </div>
        <div className="text-gray-700 text-sm mt-2 ml-4">© 2023 INSTAGRAM FROM META</div>
    </div>
  )
}

export default Right