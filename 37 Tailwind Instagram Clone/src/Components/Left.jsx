import { GoHome } from "react-icons/go";
import { AiOutlineSearch } from "react-icons/ai";
import { MdOutlineExplore } from "react-icons/md";
import { BsPersonVideo2, BsFillPlusCircleFill,BsInstagram } from "react-icons/bs";
import { FaFacebookMessenger } from "react-icons/fa6";
import { FcLike, FcBusinessman } from "react-icons/fc";
import { SiThreads } from "react-icons/si";
import { RxHamburgerMenu } from "react-icons/rx";

const Left = ({handleThemeSwitch}) => {
  return (
    <div className="h-screen w-fit pl-4 flex flex-col justify-around items md:border-r border-gray-500 max-sm:hidden">

        <div className="flex justify-center items-center max-lg:mr-1 py-2 pr-14 rounded gap-4 text-xl cursor-pointer" onClick={handleThemeSwitch}>
            <BsInstagram className="text-2xl ml-1 md:hidden"/>
            <span className=" font-dancing-script max-md:hidden text-2xl font-bold">Instagram</span>
        </div>
        
        {/* Upper Icons */}
        {[
            [GoHome, 'Home'],
            [AiOutlineSearch, 'Search'],
            [MdOutlineExplore, 'Explore'],
            [BsPersonVideo2, 'Reels'],
            [FaFacebookMessenger, 'Messages'],
            [FcLike, 'Notifications'],
            [BsFillPlusCircleFill, 'Create'],
            [FcBusinessman, 'Profile'],
        ].map(([IconComponent, title], index) =>(
            <div key={index} className="flex items-center max-lg:mr-1 py-2 pl-1 pr-14 rounded gap-4 text-xl cursor-pointer
             dark:hover:bg-zinc-800 hover:bg-gray-200">
                <IconComponent className="text-2xl ml-1"/>
                <span className="max-md:hidden">{title}</span>
            </div>
            )
        )}
        
        {/* Lower icons */}
        {[
            [SiThreads, 'Threads'],
            [RxHamburgerMenu, 'More']
        ].map(([IconComponent, title], index) =>(<div key={index} className="flex items-center max-lg:mr-1 py-2 pl-1 pr-14 rounded 
        gap-4 text-xl cursor-pointer dark:hover:bg-zinc-800 hover:bg-gray-200">
                <IconComponent className="text-2xl ml-1"/>
                <span className="max-md:hidden">{title}</span>
            </div>)
        )}
        
    </div>
)}

export default Left