import { BiDotsHorizontalRounded } from "react-icons/bi";

const Posts = () => {
  return (
    <div className="h-3/4 w-full p-16 ">

        <div className="flex justify-between cursor-pointer">
            <div className="flex gap-3 dark:text-gray-200">

                <div className="h-8 w-8 flex gap-3">
                    <img src="https://avatars.githubusercontent.com/u/59287553?v=4" alt="profile picture" 
                    className="object-fit rounded-full"/>
                </div>

                <div className="text-md font-base self-start w-fit">bisw4sh • 1d</div>

            </div>
            <BiDotsHorizontalRounded className="text-2xl"/>
        </div>
        <div className="imageHolder w-full mt-2">
            <img src="https://avatars.githubusercontent.com/u/59287553?v=4" alt="Post" 
            className="object-fit w-full rounded"/>


        </div>

    </div>
  )
}

export default Posts