import mugshots from './Data/mugshots.jsx';

const Story = () => {
    mugshots.map( ([img,username]) => {
        console.log(img, username);
    } )

  return (
    <div className='flex overflow-scroll overflow-y-hidden gap-4 py-4'>
        { mugshots.map( ([img,username], index) => (
            <div key={index} className='flex-auto'>
                <img src={img} alt="mugshots" className='rounded-full cursor-pointer'/>
                <span>{username}</span>
            </div>
        ))
        }
        
    </div>
  )
}

export default Story