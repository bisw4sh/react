import { Link, Outlet } from "react-router-dom";
import { ModeToggle } from "@/components/mode-toggle";
import { Home } from "lucide-react";

export default function Navbar() {
  return (
    <div className="min-h-screen dark:bg-neutral-950 dark:text-slate-50 p-3 ">
      <div className="w-full  flex justify-between items-center p-1 rounded-md shadow-md shadow-gray-900">
        <Link to="/">
          <Home className="cursor-pointer hover:stroke-slate-500" />
        </Link>
        <ModeToggle />
      </div>
      <Outlet />
    </div>
  );
}
