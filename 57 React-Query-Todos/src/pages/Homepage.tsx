import { useState } from "react";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import { v4 as uuidv4 } from "uuid";

interface TodoType {
  id: string;
  todo: string;
  completed: boolean;
}

export default function Homepage() {
  const [newTodo, setNewTodo] = useState<string>("");

  const queryClient = useQueryClient();
  //Get data
  const query = useQuery({
    queryKey: ["todos"],
    queryFn: async () => {
      const getTodos = await fetch("/api/get");
      if (!getTodos.ok) {
        throw new Error("Failed to fetch data");
      }
      return await getTodos.json();
    },
  });

  // Add Todo
  const addTodo = useMutation({
    mutationFn: async (newTodoData: TodoType) => {
      await fetch("/api/add", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newTodoData),
      });
    },
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ["todos"] });
    },
  });

  // Remove Todo
  const removeTodo = useMutation({
    mutationFn: async ({ id }: { id: string }) => {
      await fetch("/api/delete", {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id }),
      });
    },
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ["todos"] });
    },
  });

  // Update Todo
  const strikeTodo = useMutation({
    mutationFn: async ({ id }: { id: string }) => {
      await fetch("/api/update", {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id }),
      });
    },
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ["todos"] });
    },
  });

  if (query.isPending) return "Loading...";

  if (query.error) return "An error has occurred: " + query.error.message;

  return (
    <div className="min-h-screen p-4 flex flex-col items-center gap-4">
      <form
        className="flex w-full max-w-sm items-center space-x-2"
        onSubmit={(e: React.SyntheticEvent<HTMLFormElement, SubmitEvent>) => {
          e.preventDefault();
          addTodo.mutateAsync({
            id: uuidv4(),
            todo: newTodo,
            completed: false,
          });
          setNewTodo("");
        }}
      >
        <Input
          type="text"
          placeholder="Enter your task"
          value={newTodo}
          onChange={(e) => {
            setNewTodo(e.currentTarget.value);
          }}
        />
        <Button type="submit">Add</Button>
      </form>
      <Card className="lg:w-1/3">
        <CardHeader>
          <CardTitle>Tasks List</CardTitle>
        </CardHeader>
        <CardContent className="space-y-4">
          {query.data ? (
            query.data.map(({ id, todo, completed }: TodoType) => (
              <section className="flex justify-between items-center" key={id}>
                <article className={completed ? "line-through" : ""}>
                  {todo}
                </article>
                <main className="flex justify-center items-center gap-3">
                  <Checkbox
                    checked={completed}
                    onClick={(e: React.MouseEvent<HTMLElement>) => {
                      console.log(e.currentTarget.getAttribute("aria-checked"));
                      strikeTodo.mutateAsync({ id });
                    }}
                  />
                  <Button
                    variant="destructive"
                    onClick={() => {
                      removeTodo.mutateAsync({ id });
                    }}
                  >
                    Delete
                  </Button>
                </main>
              </section>
            ))
          ) : (
            <span>Nothing to show</span>
          )}
        </CardContent>
      </Card>
    </div>
  );
}
