import express from "express";
const app = express();
let users = [
  {
    id: "1",
    todo: "Gotta understand everysingle details about react query",
    completed: false,
  },
  {
    id: "2",
    todo: "Gotta, prep for exams now thaat they are near",
    completed: true,
  },
];

app.use(express.json());

app.get("/api/get", async (req, res) => {
  res.json(users);
});

app.post("/api/add", (req, res) => {
  users.push(req.body);
  res.send(`Task with ID ${req.body.id} is now added.`);
});

app.patch("/api/update", (req, res) => {
  const { id } = req.body;
  try {
    // Find the index of the user with the specified ID
    const index = users.findIndex((user) => user.id === id);
    
    // User with the specified ID not found
    if (index === -1) return res.status(404).json({ error: "User not found" });

    // Update the completed status by flipping the boolean
    users[index].completed = !users[index].completed;

    res.json({ message: `Task with ID ${id} is now updated.` });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.delete("/api/delete", async (req, res) => {
  try {
    const delID = req.body.id;
    users = users.filter(({ id }) => delID !== id);
  } catch (error) {
    console.log(error);
  }
  res.end(`Task with ID ${req.body.id} exists nomore.`);
});

app.listen(8080, () => console.log("server running @ http://localhost:8080"));
