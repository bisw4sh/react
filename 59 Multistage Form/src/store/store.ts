import { create } from "zustand";

interface Form1D {
  name: string;
  email: string;
  age: number;
  occupation: string;
}

interface Form23D {
  province: string;
  district: string;
  municipality: string;
  ward: number;
  tole: string;
}

interface Form4D {
  remarks: string;
}

interface Store {
  details: {
    form1: Form1D;
    form2: Form23D;
    form3: Form23D;
    form4: Form4D;
    [key: string]: Form1D | Form23D | Form4D; // Index signature
  };
  changeData: (whichform: string, someData: Form1D | Form23D | Form4D) => void;
}

export const useData = create<Store>((set) => ({
  details: {
    form1: {
      name: "",
      email: "",
      age: 0,
      occupation: "",
    },
    form2: {
      province: "",
      district: "",
      municipality: "",
      ward: 0,
      tole: "",
    },
    form3: {
      province: "",
      district: "",
      municipality: "",
      ward: 0,
      tole: "",
    },
    form4: {
      remarks: "",
    },
  },
  changeData: (whichform: string, someData: Form1D | Form23D | Form4D) =>
    set((state) => ({
      details: {
        ...state.details,
        [whichform]: { ...state.details[whichform], ...someData },
      },
    })),
}));
