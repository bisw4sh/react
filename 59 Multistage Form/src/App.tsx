import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route,
} from "react-router-dom";
import HomePage from "./pages/HomePage";
import Error from "./pages/Error";
import Navbar from "./components/Navbar";
import MainPage from "./pages/MainPage";
import Results from "./pages/Results";
import Form1 from "./pages/Forms/Form1";
import Form2 from "./pages/Forms/Form2";
import Form3 from "./pages/Forms/Form3";
import Form4 from "./pages/Forms/Form4";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<Navbar />} errorElement={<Error />}>
        <Route index element={<HomePage />} />
        <Route path="main" element={<MainPage />}>
          <Route index element={<Form1 />} />
          <Route path="form2" element={<Form2 />} />
          <Route path="form3" element={<Form3 />} />
          <Route path="form4" element={<Form4 />} />
        </Route>
        <Route path="results" element={<Results />} />
      </Route>
    </>
  )
);

export default function App() {
  return <RouterProvider router={router} fallbackElement={<Error />} />;
}
