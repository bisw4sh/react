export default function HomePage() {
  return (
    <div className="min-h-screen flex justify-center flex-col gap-4 items-center text-xl font-medium">
      <div>
        This project uses Vite, Tailwind, DaisyUI, zustand, react-hook-form
      </div>
      <div>Get started from -&gt; main</div>
    </div>
  );
}
