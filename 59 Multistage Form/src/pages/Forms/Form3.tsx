import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useData } from "../../store/store";

interface Form23D {
  province: string;
  district: string;
  municipality: string;
  ward: number;
  tole: string;
}

const Form3 = () => {
  const { details, changeData } = useData();
  const navigate = useNavigate();
  const { register, handleSubmit, setValue } = useForm<Form23D>();

  const onSubmit: SubmitHandler<Form23D> = (data) => {
    changeData("form3", data);
    console.log(details);
    navigate("../form4");
  };

  const handleSame = () => {
    const dataToFill = details.form2;
    if (dataToFill) {
      Object.keys(dataToFill).forEach((key) => {
        setValue(key as keyof Form23D, dataToFill[key as keyof Form23D]); // Set the value using setValue
      });
      console.log("Remember me button clicked!"); // Log after filling the form
    }
  };

  return (
    <main className="min-h-screen flex justify-center items-center">
      <button
        onClick={() => navigate(-1)}
        className="btn btn-primary absolute left-10 bottom-10"
      >
        Back
      </button>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full flex flex-col justify-center items-center gap-4"
      >
        <label className="input input-bordered flex items-center gap-2 w-full">
          Province
          <input
            type="text"
            className="grow bg-inherit"
            placeholder="Enter your province"
            {...register("province")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          District
          <input
            type="text"
            className="grow bg-inherit"
            placeholder="Enter your district"
            {...register("district")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Municipality
          <input
            type="string"
            className="grow bg-inherit"
            placeholder="Enter your municipality"
            {...register("municipality")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Ward
          <input
            type="number"
            className="grow bg-inherit"
            placeholder="Ward you live in"
            {...register("ward")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Tole
          <input
            type="string"
            className="grow bg-inherit"
            placeholder="Tole"
            {...register("tole")}
          />
        </label>
        <button
          type="submit"
          className="btn btn-primary absolute right-10 bottom-10"
        >
          Next
        </button>

        <label className="label cursor-pointer" onClick={handleSame}>
          <div className="label-text mr-2">Remember me </div>
          <input type="checkbox" className="checkbox" />
        </label>
      </form>
    </main>
  );
};

export default Form3;
