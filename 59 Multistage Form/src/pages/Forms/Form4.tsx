import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useData } from "../../store/store";

interface Form4D {
  remarks: string;
}

const Form4 = () => {
  const { details, changeData } = useData();
  const navigate = useNavigate();
  const { register, handleSubmit } = useForm<Form4D>();

  const onSubmit: SubmitHandler<Form4D> = (data) => {
    changeData("form4", data);
    console.log(details);
    navigate("/results");
  };

  return (
    <main className="min-h-screen flex justify-center items-center">
      <button
        onClick={() => navigate(-1)}
        className="btn btn-primary absolute left-10 bottom-10"
      >
        Back
      </button>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full flex flex-col justify-center items-center gap-4"
      >
        <label htmlFor="remarks">
          <div className="p-2">Feedback</div>
          <textarea
            placeholder="Enter Remarks about the project"
            className="textarea textarea-bordered textarea-info textarea-lg w-full max-w-xs"
            {...register("remarks")}
            id="remarks"
          ></textarea>
        </label>

        <button
          type="submit"
          className="btn btn-primary absolute right-10 bottom-10"
        >
          Next
        </button>
      </form>
    </main>
  );
};

export default Form4;
