import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useData } from "../../store/store";

interface Form1D {
  name: string;
  email: string;
  age: number;
  occupation: string;
}

const Form1 = () => {
  const { details, changeData } = useData();
  const navigate = useNavigate();
  const { register, handleSubmit } = useForm<Form1D>();

  const onSubmit: SubmitHandler<Form1D> = (data) => {
    changeData("form1", data);
    console.log(details);
    navigate("form2");
  };

  return (
    <main className="min-h-screen flex justify-center items-center">
      <button
        onClick={() => navigate(-1)}
        className="btn btn-primary absolute left-10 bottom-10"
      >
        Back
      </button>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-full flex flex-col justify-center items-center gap-4"
      >
        <label className="input input-bordered flex items-center gap-2 w-full">
          Name
          <input
            type="text"
            className="grow bg-inherit"
            placeholder="Enter your name"
            id="name"
            {...register("name")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Email
          <input
            type="text"
            className="grow bg-inherit"
            placeholder="Enter your email"
            id="email"
            {...register("email")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Age
          <input
            type="number"
            className="grow bg-inherit"
            placeholder="Enter your age"
            id="age"
            {...register("age")}
          />
        </label>

        <label className="input input-bordered flex items-center gap-2 w-full">
          Occupation
          <input
            type="text"
            className="grow bg-inherit"
            placeholder="What do you do for living ?"
            id="occupation"
            {...register("occupation")}
          />
        </label>
        <button
          type="submit"
          className="btn btn-primary absolute right-10 bottom-10"
        >
          Next
        </button>
      </form>
    </main>
  );
};

export default Form1;
