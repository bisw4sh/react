import { Link, Outlet } from "react-router-dom";

export default function Error() {
  return (
    <div className="min-h-screen bg-teal-100 flex justify-center items-center text-black text-4xl font-bold">
      <div className="space-x-4 p-4">
        <Link to="/" className="link link-info underline-offset-8">
          Home
        </Link>
        <Link to="/main" className="link link-info underline-offset-8">
          Main
        </Link>
        <Outlet />
      </div>
      <div>&lt;--- Error has occured go back</div>
    </div>
  );
}
