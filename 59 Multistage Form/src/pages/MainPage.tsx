import { Outlet, useLocation } from "react-router-dom";
import { useState, useEffect } from "react";

export default function MainPage() {
  const [step, setStep] = useState<number>(1);
  const { pathname } = useLocation();

  useEffect(() => {
    setStep(
      pathname === "/main"
        ? 1
        : pathname === "/main/form2"
        ? 2
        : pathname === "/main/form3"
        ? 3
        : pathname === "/main/form4"
        ? 4
        : 1
    );
  }, [pathname]);

  return (
    <div className="min-h-screen flex justify-center items-center">
      <ul className="steps absolute top-10 w-2/3">
        <li className={`step  ${step > 0 ? `step-primary text-primary` : ``}`}>
          Personal Details
        </li>
        <li className={`step  ${step > 1 ? `step-primary text-primary` : ``}`}>
          Home Address
        </li>
        <li className={`step  ${step > 2 ? `step-primary text-primary` : ``}`}>
          Current Address
        </li>
        <li className={`step  ${step > 3 ? `step-primary text-primary` : ``}`}>
          Feedback Form
        </li>
      </ul>

      <Outlet />
    </div>
  );
}
