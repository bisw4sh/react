import { useData } from "../store/store";
import { useEffect, useState } from "react";

const Results = () => {
  const { details, changeData } = useData();
  const [resetting, setResetting] = useState(false);

  useEffect(() => {
    if (resetting) {
      // Reset all form data to initial values
      changeData("form1", {
        name: "",
        email: "",
        age: 0,
        occupation: "",
      });
      changeData("form2", {
        province: "",
        district: "",
        municipality: "",
        ward: 0,
        tole: "",
      });
      changeData("form3", {
        province: "",
        district: "",
        municipality: "",
        ward: 0,
        tole: "",
      });
      changeData("form4", {
        remarks: "",
      });
      setResetting(false); // Resetting is completed
    }
  }, [resetting, changeData]);

  const handleReset = () => {
    setResetting(true); // Start the resetting process
  };

  return (
    <div className="min-h-screen flex justify-center items-center">
      <table className="border-collapse border border-gray-800">
        <thead>
          <tr className="bg-gray-200">
            <th className="border border-gray-800 px-4 py-2">Field</th>
            <th className="border border-gray-800 px-4 py-2">Value</th>
          </tr>
        </thead>
        <tbody>
          {/* Form 1 */}
          {Object.entries(details.form1).map(([key, value]) => (
            <tr key={`form1-${key}`}>
              <td className="border border-gray-800 px-4 py-2">{key}</td>
              <td className="border border-gray-800 px-4 py-2">{value}</td>
            </tr>
          ))}
          {/* Form 2 */}
          {Object.entries(details.form2).map(([key, value]) => (
            <tr key={`form2-${key}`}>
              <td className="border border-gray-800 px-4 py-2">{key}</td>
              <td className="border border-gray-800 px-4 py-2">{value}</td>
            </tr>
          ))}
          {/* Form 3 */}
          {Object.entries(details.form3).map(([key, value]) => (
            <tr key={`form3-${key}`}>
              <td className="border border-gray-800 px-4 py-2">{key}</td>
              <td className="border border-gray-800 px-4 py-2">{value}</td>
            </tr>
          ))}
          {/* Form 4 */}
          {Object.entries(details.form4).map(([key, value]) => (
            <tr key={`form4-${key}`}>
              <td className="border border-gray-800 px-4 py-2">{key}</td>
              <td className="border border-gray-800 px-4 py-2">{value}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <button
        onClick={handleReset}
        className="btn btn-primary absolute bottom-5 right-5"
      >
        Reset All Data
      </button>
    </div>
  );
};

export default Results;
