import ReactLogo from "./assets/react.svg"
const Navbar = () => {
  return (
    <div className="navbar">
        <ul>
            <li> <img src={ReactLogo} alt="React Logo" /> ReactFacts</li>
            <li>React Course - Project 1</li>
        </ul>
    </div>
  )
}

export default Navbar