import "./style.css"
import Navbar from "./Navbar"
import Desc from "./Desc"

function App() {
  return (
  <>
    <Navbar/>
    <Desc/>
  </>
  ) 
}

export default App