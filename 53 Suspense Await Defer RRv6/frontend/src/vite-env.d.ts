/// <reference types="vite/client" />

interface UserData {
  imgUrl: string;
  name: string;
  work: string;
  skills: string;
}
