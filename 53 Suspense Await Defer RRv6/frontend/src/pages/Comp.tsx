import Card from "../comp/Card";

export default function Comp() {
  return (
    <div className="flex min-h-screen items-center justify-center">
      <Card />
    </div>
  );
}
