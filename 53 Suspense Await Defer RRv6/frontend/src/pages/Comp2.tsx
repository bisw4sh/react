import { lazy } from "react";
const Card2 = lazy(() => import("../comp/Card2"));

export default function Comp() {
  return (
    <div className="flex min-h-screen items-center justify-center">
      <Card2 />
    </div>
  );
}
