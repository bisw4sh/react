import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route,
} from "react-router-dom";
import Homepage from "./pages/Homepage";
import Comp from "./pages/Comp";
import Comp2 from "./pages/Comp2";
import ErrorPage from "./pages/ErrorPage";
import Navbar from "./comp/Navbar";
import { loader as cardLoader } from "./comp/Card";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Navbar />} errorElement={<ErrorPage />}>
      <Route index element={<Homepage />} />
      <Route path="comp" element={<Comp />} loader={cardLoader} />
      <Route path="comp2" element={<Comp2 />} />
    </Route>,
  ),
);

export default function App() {
  return <RouterProvider router={router} />;
}
