import { Link, Outlet, useLocation } from "react-router-dom";

export default function Navbar() {
  const location = useLocation();
  return (
    <>
      <div className="bg-zinc-800 p-4">
        {location.pathname !== "/comp" ? (
          location.pathname !== "/comp2" ? (
            <div className="space-x-4">
              <Link to="/comp">
                <button className="btn">Comp</button>
              </Link>
              <Link to="/comp2">
                <button className="btn">Comp 2</button>
              </Link>
            </div>
          ) : (
            <div className="space-x-4">
              <Link to="/">
                <button className="btn">Home</button>
              </Link>
              <Link to="/comp">
                <button className="btn">Comp 2</button>
              </Link>
            </div>
          )
        ) : (
          <div className="space-x-4">
            <Link to="/">
              <button className="btn">Home</button>
            </Link>
            <Link to="/comp2">
              <button className="btn">Comp 2</button>
            </Link>
          </div>
        )}
      </div>
      <Outlet />
    </>
  );
}
