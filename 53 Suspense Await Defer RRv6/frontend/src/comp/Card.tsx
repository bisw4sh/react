import { useLoaderData, Await, defer } from "react-router-dom";
import { Suspense } from "react";
import CardSkl from "./CardSkl";

// eslint-disable-next-line react-refresh/only-export-components
export const loader = () => {
  const fetchData = async () => {
    const response = await fetch("/api/user");
    return response.json();
  };

  const respPromise = fetchData();

  return defer({ resp: respPromise });
};
// export const loader = () => {
//   const resp = fetch("/api/user").then((response) => response.json());

//   return defer({ resp });
// };

export default function Card() {
  const data = useLoaderData() as { resp: Promise<UserData> };

  return (
    <main>
      <Suspense fallback={<CardSkl />}>
        <Await
          resolve={data.resp}
          errorElement={<div>Couldn't load the data</div>}
        >
          {({ imgUrl, name, skills, work }: UserData) => (
            <div className="card w-96 bg-base-100 shadow-xl">
              <figure>
                <img src={imgUrl} alt="Person" />
              </figure>
              <div className="card-body">
                <h2 className="card-title">
                  {name}
                  <div className="badge badge-secondary">{work}</div>
                </h2>
                <p>Hey, he is him!</p>
                <div className="card-actions justify-end">
                  <div className="badge badge-outline">{skills}</div>
                </div>
              </div>
            </div>
          )}
        </Await>
      </Suspense>
    </main>
  );
}
