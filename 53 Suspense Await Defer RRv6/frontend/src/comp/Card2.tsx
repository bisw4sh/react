import { Suspense } from "react";
import { Await } from "react-router-dom";
import CardSkl from "./CardSkl";

const fetchData = async (): Promise<UserData> => {
  const response: Response = await fetch("/api/user");
  const resp: UserData = await response.json();
  return resp;
};

export default function Card2() {
  const data = fetchData();
  return (
    <Suspense fallback={<CardSkl />}>
      <Await resolve={data}>
        {({ imgUrl, name, work, skills }: UserData) => (
          <div className="card w-96 bg-base-100 shadow-xl">
            <figure>
              <img src={imgUrl} alt="Person" />
            </figure>
            <div className="card-body">
              <h2 className="card-title">
                {name}
                <div className="badge badge-secondary">{work}</div>
              </h2>
              <p>Hey, he is him!</p>
              <div className="card-actions justify-end">
                <div className="badge badge-outline">{skills}</div>
              </div>
            </div>
          </div>
        )}
      </Await>
    </Suspense>
  );
}
