### Important lesson

```js
export const loader = async () => {
  const response: Response = await fetch("/api/user");
  const resp: UserData = await response.json();
  return defer({ resp });
};
```

This doesn't show skeleton loader because the value is being resolved in the loader itself but <Await> expects a promise.
So, we return a single promise from the loader with no repeated await syntax.

```js
export const loader = () => {
  const fetchData = async () => {
    const response = await fetch("/api/user");
    return response.json();
  };

  const respPromise = fetchData();

  return defer({ resp: respPromise });
};
```
