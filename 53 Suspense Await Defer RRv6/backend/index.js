import express from "express";

const app = express();
app.use(express.json());
let count = 0;

app.get("/api/user", async (_, res) => {
  count++;
  console.log(count);
  setTimeout(() => {
    res.json({
      imgUrl:
        "https://images.pexels.com/photos/810775/pexels-photo-810775.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
      name: "Biswash Dhungana",
      work: "Software Engineer",
      skills: "Javascript, TypeScript, Go",
    });
  }, 3000);
});

app.listen(8080, () => console.log(`Running @ http://localhost:8080`));
