import { useAuth0 } from "@auth0/auth0-react";

const Profile = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();

  if (isLoading) {
    return <div>Loading ...</div>;
  }

  return (
    <div className="min-h-screen flex flex-col justify-center items-center">
      {isAuthenticated && (
      <div className="bg-zinc-800 p-5 rounded-lg flex flex-col justify-start items-start">
        <img src={user?.picture} alt={user?.name} className="rounded-md self-center"/>
        <h2>Name : {user?.name}</h2>
        <p>Email : {user?.email}</p>
        <p>Gender : {user?.gender}</p>
        <p>Phone Number : {user?.phone_number}</p>
        <p>Address : {user?.address}</p>
      </div>
      )}
    </div>
  );
};

export default Profile;
