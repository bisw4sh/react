import { useAuth0 } from "@auth0/auth0-react";

export default function Homepage() {
  const { isAuthenticated, user } = useAuth0();

  return isAuthenticated && user ? (
    <div>Welcome {user.name}</div>
  ) : (
    <>The purpose of this project is to showcase auth with Auth0</>
  );
}
