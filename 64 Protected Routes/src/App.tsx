import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import Navbar from "./components/Navbar";
import Homepage from "./pages/Homepage";
import Profile from "./pages/Profile";
import Login from "./pages/Login";
import PrivateRoutes from "./pages/PrivateRoutes/PrivateRoutes";
import Hidden from "./pages/PrivateRoutes/Hidden";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Navbar />}>
      <Route index element={<Homepage />} />
      <Route path="profile" element={<Profile />} />

      <Route element={<PrivateRoutes />}>
        <Route element={<Hidden />} path="hidden" />
        <Route path="login" element={<Login />} />
      </Route>
    </Route>
  )
);

export default function App() {
  return <RouterProvider router={router}></RouterProvider>;
}
