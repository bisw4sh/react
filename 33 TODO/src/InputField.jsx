import { useState } from 'react';
import { AiFillPushpin } from "react-icons/ai";

const InputField = ({ index, setIndex, data, setData }) => {
  const [query, setQuery] = useState('');

  const handleAdd = (e) => {
    e.preventDefault();
    const newIndex = index + 1;
    const newData = [...data, { id: newIndex, text: query }];
    localStorage.setItem('tasks', JSON.stringify(newData));
    setIndex(newIndex);
    setData(newData);
    setQuery('');
  }

  return (
    <div>
      <form action="" className="form" onSubmit={handleAdd}>
        <label htmlFor="inputField">
          <input type="text" id="inputField" placeholder={query} value={query} onChange={(e) => setQuery(e.target.value)} />
        </label>
        <button type="submit">
          Pin
          <AiFillPushpin />
        </button>
      </form>
    </div>
  )
}

export default InputField;
