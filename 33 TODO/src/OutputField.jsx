import { AiFillDelete, AiFillEdit } from "react-icons/ai";

const OutputField = ({ data, setData }) => {

  const handleDelete = (id) => {
    const newData = data.filter(single => single.id !== id);
    setData(newData);
    localStorage.setItem('tasks', JSON.stringify(data));
  }

  const handleEdit = (id, text) => {
    console.log(id,text)
    const temp = data.map( task => task.id !== id)
    setData(temp)
    localStorage.setItem('tasks', JSON.stringify(data));
    //Need a state management to fix this
    //TODO: Change query state variable in InputField and as well as the index as the id
    //Use Redux or context API 

  }

  return (
    <div className="outputfield">
      <ul>
      {data.length > 0 ? (data.map(task => (
          <li key={task.id}>
            {task.text}
            <span className="tools">
              <AiFillDelete onClick={() => handleDelete(task.id)} />
              <AiFillEdit onClick={() => handleEdit(task.id, task.text)}/>
            </span>
          </li>))) :
          (<h1>No task to be displayed</h1>)}
      </ul>
    </div>
  )
}

export default OutputField;