import { useState, useEffect } from 'react';
import InputField from "./InputField.jsx";
import OutputField from "./OutputField.jsx";

const App = () => {
  const [data, setData] = useState([]);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    // Load tasks from local storage
    const storedTasks = JSON.parse(localStorage.getItem('tasks'));
    if (storedTasks) {
      setData(storedTasks);
      setIndex(storedTasks.length); // Update the index as well
    }
  }, []);

  return (
    <>
      <h1 className="title">MY TASKS</h1>
      <InputField index={index} setIndex={setIndex} data={data} setData={setData} />
      <OutputField data={data} setData={setData} />
    </>
  );
}

export default App;
