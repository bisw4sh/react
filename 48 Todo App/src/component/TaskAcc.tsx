import { Accordion, AccordionItem, Button, Checkbox } from "@nextui-org/react";

interface TaskType {
  title: string;
  id: string;
}

type TasksProps = {
  tasks: TaskType[];
  setTasks: React.Dispatch<React.SetStateAction<TaskType[]>>;
};

export default function TaskAcc({ tasks, setTasks }: TasksProps) {
  const handleDelete = (deleteid: string) => {
    const updatedTask = tasks
      .filter(({ id }: TaskType) => id !== deleteid)
      .map(({ id, title }: TaskType) => ({ id, title }));
    setTasks(updatedTask);
    localStorage.setItem("tasks", JSON.stringify(updatedTask));
  };
  return (
    <div className="w-full flex flex-col justify-center items-center pt-4 px-4 md:px-16">
      <Accordion>
        {tasks.map(({ title, id }: TaskType) => {
          return (
            <AccordionItem
              key={id}
              aria-label={title}
              title={title}
            >
              <div className="flex max-md:flex-col justify-between items-center gap-1">
                <div>{id}</div>

                <div className="space-x-2">
                  <Button
                    color="danger"
                    variant="bordered"
                    onClick={() => handleDelete(id)}
                  >
                    Delete
                  </Button>

                  <Checkbox
                    defaultSelected={false}
                    color="success"
                    lineThrough={true}
                   
                  >
                    Disable
                  </Checkbox>
                </div>
              </div>
            </AccordionItem>
          );
        })}
      </Accordion>
    </div>
  );
}
