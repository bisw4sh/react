import { Switch } from "@nextui-org/react";
import { MoonIcon } from "./MoonIcon";
import { SunIcon } from "./SunIcon";

export default function Switcher({
  css,
  handleTheme,
}: {
  css: string;
  handleTheme: () => void;
}) {
  return (
    <Switch
      // defaultSelected
      size="lg"
      color="secondary"
      thumbIcon={({ isSelected, className }) =>
        isSelected ? (
          <SunIcon className={className} />
        ) : (
          <MoonIcon className={className} />
        )
      }
      className={css}
      onClick={handleTheme}
    ></Switch>
  );
}
