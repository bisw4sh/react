import "./About.css"

const About = () => {
  return (
    <div className="about-sect">
        <h2>About</h2>
        <p>I am a software engineering student in final year with particular interest in system programming,
             web assembly, block chain & web3
        </p>
    </div>
  )
}

export default About