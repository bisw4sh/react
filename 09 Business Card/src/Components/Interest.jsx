import "./Interest.css"

const Interest = () => {
  return (
    <div className="interest-sect">
        <h2>Interests</h2>
        <p>Geo Politics, History, European Football, PUBG & PUBGM esports, EDMs & Anime</p>
    </div>
  )
}

export default Interest