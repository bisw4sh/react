import DP from "./../assets/profile.png"
import "./Card.css"
import Middle from "./Middle"
import Footer from "./Footer"
import About from "./About"
import Interest from "./Interest"

const Card = () => {
  return (
    <div className="Card">
        <img src={DP} alt="Profile Picture" className="imgCard"/>
        <Middle />
        <About />
        <Interest />
        <Footer />
    </div>
  )
}

export default Card