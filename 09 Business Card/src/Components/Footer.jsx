import "./Footer.css"

const Footer = () => {
  return (
    <div className="footer">

        <i className="fa-brands fa-square-x-twitter"></i>
        <i className="fa-brands fa-square-facebook"></i>
        <i className="fa-brands fa-square-instagram"></i>
        <i className="fa-brands fa-square-github"></i>

        </div>
  )
}

export default Footer