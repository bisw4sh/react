import "./Middle.css"

const Middle = () => {
  return (
    <div className="Middle">
        <h2>Biswash Dhungana</h2>
        <h4>Software Engineer</h4>
        <p>www.biswashdhungana.com.np</p>

        <div className="icons">

            <div className="email">
                <i className="fa-solid fa-envelope"></i>
                <span>Email</span>
            </div>

            <div className="linkedin">
                <i className="fa-brands fa-linkedin"></i>
                <span>LinkedIn</span>
            </div>
            
        </div>

    </div>
  )
}

export default Middle