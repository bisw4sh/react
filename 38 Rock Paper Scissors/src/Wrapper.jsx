import { useState, useEffect } from 'react'
import { FaHandRock, FaHandPaper, FaHandScissors } from "react-icons/fa"
import { AiOutlineCloseCircle } from "react-icons/ai"
import { BiReset } from "react-icons/bi";

const Wrapper = () => {
  const [userPick, setUserPick] = useState(null)
  const [machinePick, setMachinePick] = useState(null)
  const [result, setResult] = useState('Start')
  const [count, setCount] = useState(0)
  const [userScore, setUserScore] = useState(Number(localStorage.getItem('user')) || 0)
  const [machineScore, setMachineScore] = useState(Number(localStorage.getItem('machine')) || 0)

  const handleReset = () => {
    localStorage.removeItem('user')
    localStorage.removeItem('machine')
    setUserScore(0)
    setMachineScore(0)
  }

  useEffect(() => {
    const handleResult = () => {

      if (userPick === null) {
          setResult('Start')
          return
        }
        else if( userPick === machinePick ){
          setResult('draw')
        }
        else if (
            (userPick === 'Rock' && machinePick === 'Scissors') ||
            (userPick === 'Scissors' && machinePick === 'Paper') ||
            (userPick === 'Paper' && machinePick === 'Rock')) 
            {
            setResult('win')
            localStorage.setItem('user', userScore + 1)
            setUserScore(userScore + 1)
          }
      else if(
        (machinePick === 'Rock' && userPick === 'Scissors') ||
        (machinePick === 'Scissors' && userPick === 'Paper') ||
        (machinePick === 'Paper' && userPick === 'Rock')){
        setResult('loss')
        localStorage.setItem('machine', machineScore + 1)
        setMachineScore(machineScore + 1)
      } 
    }

    const machinePicked = () => {
      const Options = ['Rock', 'Paper', 'Scissors']
      if (userPick != null) {
        setMachinePick(Options[Math.floor(Options.length * Math.random())])
      }
    }

    machinePicked()
    handleResult()
  }, [count]) 

  const handlePicks = (pick) => {
    setCount(count + 1)
    setUserPick(pick)
  }

  return (
  <div className="h-1/3 w-1/3 min-h-fit min-w-fit p-3 flex flex-col dark:bg-zinc-950 bg-zinc-200 rounded-lg">

    <div className="w-full flex justify-center items-center text-2xl font-semibold capitalize dark:text-teal-300">
      {result}
    </div>

    <div className="flex justify-around items-center text-xl font-semibold">
      <span className='text-blue-400 flex flex-col justify-center items-center'>
        <span>{userScore || <AiOutlineCloseCircle />}</span>
        <span>User:</span>
        <span>{userPick  || <AiOutlineCloseCircle />} </span>
      </span>

    <span>
      <BiReset className="hover:fill-cyan-950 active:fill-teal-800 text-5xl" onClick={ handleReset }/>
    </span>

      <span className='text-rose-400 flex flex-col justify-center items-center text-xl font-semibold'>
        <span>{machineScore || <AiOutlineCloseCircle />}</span>
        <span>Computer:</span>
        <span>{machinePick || <AiOutlineCloseCircle />}</span>
      </span>
    </div>

    <div className="h-full flex justify-around items-center">
        {[
          [FaHandRock, 'Rock'],
          [FaHandPaper, 'Paper'],
          [FaHandScissors, 'Scissors']
        ].map( ([Icon,pick], index) => (
          <div key={index} className="text-2xl flex flex-col justify-center items-center cursor-pointer" onClick={() => handlePicks(pick)}>
                <Icon  className="hover:fill-cyan-950 active:fill-teal-800 text-4xl " value={pick}/>
                <div className="hover:text-cyan-950 " value={pick}>{pick}</div>
            </div>))}
    </div>
  </div>
  )
}

export default Wrapper