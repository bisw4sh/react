import Card from './Card.jsx'
import Form from './Form.jsx'
import Banner from './Banner.jsx'
import TAB from './TAB.jsx'
import Solid from './Solid.jsx'
import Titled from './Titled.jsx'

const App = () => {

  return (
    <div className="min-h-screen flex items-center justify-center gap-4">
      <div>
      <Banner />
      <TAB />
      <Solid/>
      <Titled/>
      </div>
      <Card />
      <Form />
    </div>
  )
}

export default App