import { useState, useEffect } from "react"
import { MdDarkMode, MdSunny } from "react-icons/md"
import MainContent from "./MainContent.jsx"

function App() {
  const [theme, setTheme] = useState(null);

  useEffect(() => {
    if(localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)){
      setTheme('dark')
    }
    else {
      setTheme('light')
    }
  }, [])

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark")
    } else {
      document.documentElement.classList.remove("dark")
    }
  }, [theme])

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem('theme', `${theme === "dark" ? "light" : "dark"}`)
  }

  return (
    <div className="h-screen relative dark:bg-black dark:text-white flex flex-col justify-around items-center ">

      <button onClick={handleThemeSwitch} className="absolute right-8 top-8 text-3xl">
          {theme === 'dark' ? <MdDarkMode className="fill-blue-400"/> : <MdSunny className="fill-yellow-500"/>}
      </button>

      <MainContent className='flex flex-col justify-around items-center'/>

    </div>
  )
}

export default App