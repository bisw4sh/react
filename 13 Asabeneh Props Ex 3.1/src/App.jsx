import "./App.css"
const HEX ="0123456789abcdef"

const colorcode = () =>{
  let colorStr = ''
  for(let i = 0; i < 6; i++){
    colorStr += HEX[ Math.floor(Math.random() * HEX.length) ] 
  }
  return `#${colorStr}`
}

const Rect = () => {
  const colorCode = colorcode();
  return(
    <div className="colorRect" style={{backgroundColor:colorCode}}><h3>{colorCode}</h3></div>
  )
}

function App() {
  return (
    <div className="wrapper">
     <Rect />
     <Rect />
     <Rect />
     <Rect />
     <Rect />
    </div>
  )
}

export default App