import { useState, useEffect } from 'react';
import Navbar from './Navbar.jsx';
import { BsArrowUpCircleFill } from "react-icons/bs";

const App = () => {
  const [scrollTop, setScrollTop] = useState(0);

  const toTop = () => window.scrollTo({top: 0, left: 0, behavior: 'smooth'})
  const handleScroll = () => setScrollTop(window.scrollY);

  useEffect(() => {

    handleScroll()
    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll)
  }, []);

  return (
    <div className="h-screen bg-slate-800" onScroll={handleScroll}>
      <Navbar />

      {scrollTop > 200 ? (<button className='p-1 fixed bottom-10 right-10 text-5xl' onClick={toTop}>
        <BsArrowUpCircleFill />
       {/* {Math.floor(scrollTop)} */}
       </button>): (<div>Loadubg</div>)}

    </div>
  );
};

export default App;