const products = [
  {
    id: 'recZkNf2kwmdBcqd0',
    name: 'accent chair',
    image:
      'https://media.istockphoto.com/id/1464525764/photo/birthday-party-background-with-border-of-balloons.webp?s=2048x2048&w=is&k=20&c=NMrGBMTTlQs6uWdCMHj5G-P_KFzX1A_HDooAJw2Gn9U=',
  },
  {
    id: 'recEHmzvupvT8ZONH',
    name: 'albany sectional',

    image:
      'https://media.istockphoto.com/id/1460755337/photo/white-color-theme-modern-style-office-with-exposed-concrete-floor-and-a-lot-of-plant-3d.jpg?s=1024x1024&w=is&k=20&c=-G_7gtguYmk3uuYxxfBfCTbL0wulw0izJnUs6gW6iwE=',
  },
  {
    id: 'rec5NBwZ5zCD9nfF0',
    name: 'albany table',

    image:
      'https://as2.ftcdn.net/v2/jpg/05/32/23/63/1000_F_532236382_GYXU7WpBSnmeqE4CtoZrLDOJQo9arDVq.jpg',
  },
  {
    id: 'recd1jIVIEChmiwhe',
    name: 'armchair',

    image:
      'https://images.unsplash.com/photo-1553095066-5014bc7b7f2d?auto=format&fit=crop&q=80&w=2071&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  },
  {
    id: 'recoM2MyHJGHLVi5l',
    name: 'bar stool',
    image:
      'https://scontent.fbhr1-1.fna.fbcdn.net/v/t1.6435-9/75650459_442068489837186_1975546207241502720_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=c2f564&_nc_ohc=wKRXJHHECwkAX_7CY4t&_nc_ht=scontent.fbhr1-1.fna&oh=00_AfDiDFEzo_X0bfNxc36IvGFxsKJ4ONp8B58XdqWW85pamQ&oe=655D9F80',
  },
];

export default products;