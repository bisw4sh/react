import { useState, useEffect } from 'react';
import { MdDarkMode, MdSunny, MdKeyboardArrowRight } from "react-icons/md";

const Terminal = ({dark, setDark}) => {
    const [query, setQuery] = useState('')
    const [template, setTemplate] = useState('')
    const successTemplate = "{name: Biswash Dhungana, skills : HTML, CSS, JS, Tailwind, NodeJS, MongoDB, PostgreSQL}"
    const failedTemplate = "Enter valid command"

    const [theme, setTheme] = useState(null);

    useEffect(() => {
      if(window.matchMedia('(prefers-color-scheme: dark)').matches){
        setTheme('dark');
      }
      else {
        setTheme('light');
      }
    }, [])
  
    useEffect(() => {
      if (theme === "dark") {
        document.documentElement.classList.add("dark");
      } else {
        document.documentElement.classList.remove("dark");
      }
    }, [theme]);
  
    const handleThemeSwitch = () => {
      setTheme(theme === "dark" ? "light" : "dark");
      setDark(!dark)
    };

    const handleQuery = (e) => {
        setQuery(e.target.value)
        console.log(query)
        if(e.key === 'Enter'){
            if( query === 'help'){
                console.log('Good')
                setTemplate(successTemplate)
            }
            else{
                console.log("Enter valid commands")
                setTemplate(failedTemplate)
            }
            setQuery('')
          }
    }

  return (
    <div className="h-3/5 w-3/5">
        <div className="dark:bg-slate-600 bg-slate-400 rounded-t-lg py-2 px-4 flex justify-between items-center gap-4">
            <div className="flex gap-4">
                <div className="h-4 w-4 bg-red-500 rounded-full cursor-pointer"></div>
                <div className="h-4 w-4 bg-yellow-500 rounded-full cursor-pointer"></div>
                <div className="h-4 w-4 bg-green-500 rounded-full cursor-pointer"></div>
            </div>

            <div className="self-center">Web Terminal</div>

            {/* Sign/ svg should be opposite of the current theme, signalling the event on click */}
            {dark ? (<MdSunny onClick={ handleThemeSwitch} className="text-2xl active:fill-slate-500 cursor-pointer fill-yellow-300"/>) : (
            <MdDarkMode onClick={ handleThemeSwitch} className="text-2xl active:fill-slate-500 cursor-pointer fill-blue-200"/>) }
        </div>
        
        {/* Caret Width issue */}
        <div className="h-full w-full rounded-b-lg dark:bg-zinc-900 bg-zinc-400 dark:text-slate-400 text-slate-50 p-2 caret-pink-500 stroke-2" style={{caretWidth: "100px" }}>
            <span className="dark:text-pink-700 text-rose-600 pr-4">bisw4sh@b4 webTerminal ~/</span>
            Type &apos;help&apos; to get started ! <br />
            <span className="dark:text-pink-700 text-rose-600 pr-4">$
            </span>
            <input type="text" className="dark:bg-zinc-900 bg-zinc-400 border-none focus:outline-none w-5/6 cursor-text" autoFocus 
            onKeyUp={handleQuery}/>

    {template ? (<div className='flex items-start '><span className='dark:text-pink-700 text-rose-600 -ml-1'>
    <MdKeyboardArrowRight className='mt-1'/>
    </span>{template}</div>) : (<div></div>)}

        </div>

    </div>
  )
}

export default Terminal