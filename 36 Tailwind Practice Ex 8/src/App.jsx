import { useState } from 'react';
import Terminal from './Terminal.jsx'

export default function App() {
  const [dark, setDark ] = useState(true) 

  return (
    <div className="h-screen flex justify-center items-center bg-slate-50 dark:bg-slate-800  font-mono">
     <Terminal dark={dark} setDark={setDark} />
    </div>
  )
}