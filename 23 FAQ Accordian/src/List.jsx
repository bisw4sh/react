import { useState } from 'react';
import Single from './Single.jsx';

const List = ({ data }) => {
  const [activeItems, setActiveItems] = useState({});

  const toggleItem = (itemId) => {
    setActiveItems((prevActiveItems) => ({
      ...prevActiveItems,
      [itemId]: !prevActiveItems[itemId],
    }))
  }

  return ( <> 
    {data.map(datum => (
        <Single
          key={datum.id}
          question={datum.question}
          answer={datum.answer}
          isActive={activeItems[datum.id] || false}
          toggleItem={() => toggleItem(datum.id)} />))}
           </>
  )
}


export default List;