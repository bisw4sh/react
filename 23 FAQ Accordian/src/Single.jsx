import React from 'react';
import { FaPlus, FaMinus } from 'react-icons/fa';

const Single = ({ question, answer, isActive, toggleItem }) => {
  return (
    <div className="single-item">
      <div className="question">
        <h3>{question}</h3>
        <button onClick={toggleItem}>
          {isActive ? <FaMinus /> : <FaPlus />}
        </button>
      </div>
      {isActive && <div className="answer">{answer}</div>}
    </div>
  );
};

export default Single;
