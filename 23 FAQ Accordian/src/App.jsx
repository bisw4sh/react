import List from "./List.jsx"

function App() {

const data =    [
  {
    "id": 1,
    "question" : "Name?",
    "answer" : "Biswash Dhungana",
    "active" : false
  },
  {
    "id": 2,
    "question" : "Interest?",
    "answer" : "Depends on my YouTube feed",
    "active" : false
  },
  {
    "id": 3,
    "question" : "Profession?",
    "answer" : "Hopefully software engineering",
    "active" : false
  }
]

  return (
    <div className="container">
      <h1>Frequently Asked Questions</h1>
      <List data={data}/>
    </div>
  )
}

export default App