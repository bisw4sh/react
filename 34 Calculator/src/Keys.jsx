const Keys = ({display, setDisplay}) => {

    const handleClick = (e) => {
        console.log(e.target.value)
        setDisplay(display + e.target.value) 
    }

    const displayResult = () => {
        setDisplay(eval(display))
        setDisplay('')
    }

  return (
    <div className="keypads">
            <button className="num ac" value="ac" onClick={handleClick}>AC</button>
            <button className="num del" value="del" onClick={handleClick}>DEL</button>
            <button className="num op num-div" value="÷" onClick={handleClick}>÷</button>
            <button className="num digit num-1" value="1" onClick={handleClick}>1</button>
            <button className="num digit num-2" value="2" onClick={handleClick}>2</button>
            <button className="num digit num-3" value="3" onClick={handleClick}>3</button>
            <button className="num op num-mul" value="*" onClick={handleClick}>*</button>
            <button className="num digit num-4" value="4" onClick={handleClick}>4</button>
            <button className="num digit num-5" value="5" onClick={handleClick}>5</button>
            <button className="num digit num-6" value="6" onClick={handleClick}>6</button>
            <button className="num op num-add" value="+" onClick={handleClick}>+</button>
            <button className="num digit num-7" value="7" onClick={handleClick}>7</button>
            <button className="num digit num-8" value="8" onClick={handleClick}>8</button>
            <button className="num digit num-9" value="9" onClick={handleClick}>9</button>
            <button className="num op num-sub" value="-" onClick={handleClick}>-</button>
            <button className="num digit num-dot" value="." onClick={handleClick}>.</button>
            <button className="num digit num-0" value="0" onClick={handleClick}>0</button>
            <button className="num num-eq" value="=" onClick={displayResult}>=</button>
    </div>
  )
}

export default Keys