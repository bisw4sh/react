const InputField = ({display}) => {

  return (
    <div className="display-area">
        {display}
    </div>
  )
}

export default InputField