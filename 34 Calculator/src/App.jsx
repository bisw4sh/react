import { useState } from 'react'
import InputField from "./InputField"
import Keys from './Keys.JSX'

const App = () => {
  const [display, setDisplay] = useState(0)

  return (
    < div className="calculator">
      <InputField display={display} />
      <Keys display={display} setDisplay={setDisplay}/>
    </div>
  )
}

export default App