import { useState } from 'react';
import { SlTrash, SlPencil } from 'react-icons/sl';

const InputField = ({ data, setData, Id, setId }) => {
  const [temp, setTemp] = useState({ Id: null, text: "" });

  const handleChange = (e) => {
    setTemp({ ...temp, text: e.target.value });
  }

  const handleBlur = (e) => {
    e.preventDefault();
    setId(Id + 1);
    const newNote = { Id: Id, text: temp.text };
    setData([...data, newNote]);
    setTemp({ Id: null, text: "" }); // Clear the text when blurring
  }

  const handleDelete = (id) => {
    // Filter out the note with the specified Id and update the data state
    const updatedData = data.filter((datum) => datum.Id !== id);
    setData(updatedData);
  }

  return (
    <div className="store">
      <label htmlFor="noteInput">
        <textarea
          name="noteInput"
          id="noteInput"
          cols="44"
          rows="15"
          placeholder="START TAKING NOTES"
          draggable="false"
          value={temp.text} // Bind the textarea value to temp.text
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </label>

      {data.length > 0 ? (
        data.map((datum) => (
          <label htmlFor="noteOutput" key={datum.Id}>
            <textarea
              name="noteInput"
              cols="44"
              rows="15"
              placeholder={datum.text}
              draggable="false"
              disabled
            />
            <span className='tools'>
              <SlTrash onClick={() => handleDelete(datum.Id)} />
              <SlPencil />
            </span>
          </label>
        ))
      ) : (
        <div>No content to display</div>
      )}
    </div>
  );
}

export default InputField;