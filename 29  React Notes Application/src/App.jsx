import { useState } from 'react';
import InputField  from "./InputField.jsx";
const App = () => {

  const [data, setData] = useState([])
  const [Id, setId] = useState(0)

  return (
    <>
    <h1 className="title">React Notes Application</h1>
    <div className="Container">
      <InputField data={data} setData={setData} Id={Id} setId={setId}/>
    </div>
    </>
  )
}

export default App