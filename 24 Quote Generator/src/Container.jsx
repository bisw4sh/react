const Container = ({text, author, setDataIndex, dataArr, dataIndex}) => {
    const handleClick = ()=>{
        setDataIndex(dataIndex === dataArr.length -1 ? 0 : dataIndex + 1)
    }
  return (
    <div className="container">
        <div className="quoteC">Quote : {text}</div>
        <div className="quoteA">Author: {author}</div>
        <button onClick={handleClick}>Next Quote</button>
        </div>
  )
}

export default Container