import { useEffect, useState } from "react";
import Container from "./Container";

const App = () => {
  const [dataArr, setDataArr] = useState([])
  const [dataIndex, setDataIndex] = useState(0)

  useEffect(() => {
    (async () => {
      try {
        const response = await fetch('https://type.fit/api/quotes');
        if (response.ok) {
          const data = await response.json();
          setDataArr(data);
        } else {
          console.error('Failed to fetch data');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    })();
  }, []);

  return (
    <>
      {dataArr.length > 0 ? (
        <Container
          text={dataArr[dataIndex].text}
          author={dataArr[dataIndex].author}
          setDataIndex={setDataIndex}
          dataArr={dataArr}
          dataIndex={dataIndex}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  )
}

export default App;
