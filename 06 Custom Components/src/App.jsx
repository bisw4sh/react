import ReactLogo from "./assets/react.svg"
import "./style.css"

const List = () => 
    <ul className="list">
      <li>Home</li>
      <li>Products</li>
      <li>Pricing</li>
      <li>About</li>
      <li>Services</li>
    </ul>


function App() {
  return (
    <>
    <nav>
        <header>
        <img src={ReactLogo} alt="React Logo" className="logo"/>
        <List />
        </header>
    </nav>

    <div className="desc"><h1>This is segement is to describe about the page so...</h1></div>

    <ol>
      <li>Nodejs</li>
      <li>Nestjs</li>
      <li>Nextjs</li>
      <li>Astro</li>
    </ol>

    <footer>&copy; 2023 dhungana development. All rights reserved.</footer>
    </>
  ) 
}

export default App