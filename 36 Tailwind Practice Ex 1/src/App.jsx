const App = () => {
  // const markdown = ` For years parents have espoused the health benefits of eating garlic bread with cheese to their
  // children, with the food earning such an iconic status in our culture that kids will often dress
  // up as warm, cheesy loaf for Halloween.`

  const handleClick = (e) => {
    console.log(`"${e.target.innerText}" was clicked`)
  }

  return (
    <div className="h-screen w-screen flex flex-col items-center">
    <h1 className="text-3xl text-green-400 font-bold  hover:text-pink-500 hover:underline" onClick={handleClick}>
      Hello world!
    </h1>

    <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" className="accent-green-500"/>
    <label htmlFor="vehicle1"> I have a bike</label><br />

    <input type="checkbox" id="vehicle2" name="vehicle2" value="Car" className="accent-pink-500"/>
    <label htmlFor="vehicle2"> I have a car</label><br />

    <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat" className="accent-zinc-500"/>
    <label htmlFor="vehicle3"> I have a boat</label><br />

    <div className="flex flex-col justify-center items-center">
      <img src="https://images.unsplash.com/photo-1501196354995-cbb51c65aaea?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=facearea&facepad=4&w=256&h=256&q=80" />
      <div>
        <strong className="block">Andrew Alfred</strong>
        <span className="block">Technical advisor</span>
      </div>
    </div>

    <div className="table bg-teal-600 p-6 rounded">
  <div className="table-header-group ...">
    <div className="table-row">
      <div className="table-cell text-left ...">Song</div>
      <div className="table-cell text-left ...">Artist</div>
      <div className="table-cell text-left ...">Year</div>
    </div>
  </div>
  <div className="table-row-group">
    <div className="table-row">
      <div className="table-cell ...">The Sliding Mr. Bones (Next Stop, Pottersville)</div>
      <div className="table-cell ...">Malcolm Lockyer</div>
      <div className="table-cell ...">1961</div>
    </div>
    <div className="table-row">
      <div className="table-cell ...">Witchy Woman</div>
      <div className="table-cell ...">The Eagles</div>
      <div className="table-cell ...">1972</div>
    </div>
    <div className="table-row">
      <div className="table-cell ...">Shining Star</div>
      <div className="table-cell ...">Earth, Wind, and Fire</div>
      <div className="table-cell ...">1975</div>
    </div>
  </div>
</div>

<div className="flex bg-zinc-400 px-8 ...">
  <div className="hidden ...">01</div>
  <div className="pr-4 pb-4 hover:p-8 bg-fuchsia-600 text-sky-700">02</div>
  <div>03</div>
</div>

{/* <img className="h-80 float-right ..." src="https://images.pexels.com/photos/1287145/pexels-photo-1287145.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
<p>Maybe we can live without libraries, people like you and me. ...</p> */}

<div className="h-48 mb-4">
  <div className="h-full bg-slate-400 p-1 rounded mt-4  ">
    Oh madam, ye kar kaya rahi ho
  </div>
</div>

<div className="h-screen bg-slate-400 p-1 rounded mt-3">
  !-- ... -- Ye kuch toh hai
</div>


<div className="isolate hover:isolation-auto w-2/6 bg-green-400 p-6 m-6 rounded">
Tailwind lets you conditionally apply utility classes in different states using variant modifiers. For example, use hover:isolation-auto to only apply the isolation-auto utility on hover.
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-cover h-48 w-96 rounded" />
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-contain h-48 w-96 rounded" />
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-fill h-48 w-96 rounded" />
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-scale-down h-48 w-96 rounded" />
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-none h-48 w-96 rounded" />
</div>

<div className="bg-indigo-300 rounded m-6">
  <img src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="object-contain hover:object-scale-down h-48 w-96 rounded" />
</div>

<div className="p-5 grid grid-cols-3">
<img className="object-cover object-left-top bg-yellow-400 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-contain object-top bg-yellow-800 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-fill object-right-top bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-contain object-left bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-none object-center bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-none object-right bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-none object-left-bottom bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-none object-bottom bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
<img className="object-none object-right-bottom bg-yellow-300 w-24 h-24 ..." src="https://images.unsplash.com/photo-1697446274337-1abda37787cf?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"/>
</div>

<div className="static bg-teal-500 p-6 rounded">
  <p>Static parent</p>
  <div className="absolute bottom-0 bg-rose-600 p-6 rounded left-80">
    <p>Absolute child</p>
  </div>
</div>
 
<div className="flex gap-3 py-4">
<div dir="ltr">
  <div className="relative h-32 w-32 bg-slate-400 rounded">
    <div className="absolute h-14 w-14 top-0 start-0 bg-slate-700">Pehela Block</div>
  </div>
</div>

<div dir="rtl">
  <div className="relative h-32 w-32 bg-zinc-400 rounded text-center ">
    <div className="absolute h-24 w-24 top-0 start-0 bg-zinc-700 rounded-full flex items-center justify-center"> Dusra Block</div>
  </div>
</div>
</div>


<div className="bg-teal-500 flex flex-row w-full p-4">
  <div className=" bg-gradient-to-r from-cyan-500 to-blue-500 basis-1/4 hover:basis-full">01</div>
  <div className=" bg-emerald-500 basis-1/4 hover:basis-auto">02</div>
  <div className="bg-slate-300 basis-1/2">03</div>
</div>


<div className=" w-full h-36 grid grid-cols-3 bg-lime-500 justify-stretch items-stretch">
  <div className=" bg-slate-500 self-start">1</div>
  <div className=" bg-neutral-500 self-end">2</div>
  <div className=" bg-zinc-500 self-center	">3</div>
</div>

{/* List */}

<ul className="list-disc">
<li>5 cups chopped Porcini mushrooms</li>
  <li>  1/2 cup of olive oil</li>
  <li>3lb of celery</li>
</ul>


<ul className="list-disc">
  <li>Now this is a story all about how, my life got flipped-turned upside down</li>
  <li>  1/2 cup of olive oil</li>
  <li>3lb of celery</li>
</ul>

<ol className="list-decimal">
  <li>Now this is a story all about how, my life got flipped-turned upside down</li>
  <li>  1/2 cup of olive oil</li>
  <li>3lb of celery</li>
</ol>

<ul className="list-none">
  <li>Now this is a story all about how, my life got flipped-turned upside down</li>
  <li>  1/2 cup of olive oil</li>
  <li>3lb of celery</li>
</ul>

<p className="text-justify  hover:text-center w-1/3">So I started to walk into the water... So I started to walk into the water. I wont lie to you boys, I was terrified. But I pressed on, and as I made my way past the breakers a strange calm came over me. I don know if it was divine intervention or the kinship of all living things but I tell you Jerry at that moment, I was a marine biologist.</p>

<div className="w-64">
  <p className="w-full indent-7	">
    I’m Derek, an astro-engineer based in Tattooine. I like to build X-Wings at
    <a className="underline decoration-sky-500  decoration-4 uppercase">my Company, Inc</a>.
    Outside of work, I like to <a className="underline decoration-pink-500  decoration-4 underline-offset-8 lowercase">watch
    pod-racing</a> and have <a className="underline decoration-indigo-500  decoration-4 underline-offset-8 capitalize text-ellipsis">light-saber</a> fights.
  </p>Name
</div>

<div className="bg-scroll bg-clip-border p-32 bg-center bg-cover" style={{ backgroundImage: 'url(https://images.unsplash.com/photo-1698310876902-1138bc8ec442?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)' }}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro sunt provident obcaecati officia nam dolore. Nostrum deserunt laboriosam vero mollitia cumque neque quos eligendi officia ratione obcaecati, reprehenderit quae eveniet ex modi excepturi repudiandae quam dolorum dignissimos? Voluptates et odio amet sunt consectetur minima dolore harum. Nihil, quos? Maiores voluptatem id eaque nam cupiditate corrupti ex sed dignissimos ad, doloremque incidunt aliquam architecto, velit dolore dolor ipsam porro nihil deserunt odit rem! Reiciendis saepe autem ratione ipsum mollitia dolor aspernatur expedita aliquam fugit, nam sit, ipsa repellat quam cumque tenetur eius, unde repellendus voluptatum illum quos. Porro molestias ad, modi est aut sequi vel error perspiciatis animi iure optio nisi autem illo quibusdam nostrum? Debitis natus eum pariatur earum cupiditate sit eaque ut cum similique? Obcaecati blanditiis voluptatem tenetur illo id qui velit, amet porro excepturi quo sint ducimus ipsam libero? Placeat veritatis dolores assumenda accusantium amet consectetur tenetur maiores veniam explicabo iste eligendi, repellat atque voluptatem magni exercitationem earum blanditiis ipsam sapiente enim cum culpa aspernatur? Beatae porro veniam est minima, cumque, quidem in magnam vero possimus sit, ut eveniet sunt dignissimos veritatis temporibus! Quibusdam, deleniti totam dolores, dolor, minima accusamus hic optio cupiditate ratione reprehenderit expedita omnis eos autem voluptas. Officia in maiores sunt, perspiciatis iste magni id porro! Quasi harum ea minus quibusdam eum distinctio tempora doloribus cumque culpa. Voluptatibus, dolorem enim autem atque nostrum fugiat ex sint mollitia consequuntur aut earum velit beatae doloremque adipisci voluptatum accusamus suscipit perferendis illum voluptatem, quasi doloribus? Dolores velit praesentium eligendi a animi, eius exercitationem vero tempore, error impedit molestiae! Ex quod qui sint aliquid voluptas earum dolor vero ab ratione facilis molestias in sunt, velit aspernatur amet fugit nihil eligendi ut error vel adipisci natus repellendus ipsum. Fugit consectetur culpa temporibus? Nesciunt nulla incidunt assumenda ducimus rem laudantium et maiores, aut consequuntur libero illum fugit iusto dolor itaque at ipsa repellat. Accusamus blanditiis harum omnis, reiciendis officia sunt eum et voluptatem earum ex enim unde! Id, pariatur inventore odio quo perspiciatis libero! Error mollitia nam nulla alias, ex eius soluta! Magni illo porro quos ipsum debitis! Corporis, eligendi, facere esse vel accusantium odio at temporibus architecto fugiat reprehenderit soluta eius reiciendis ipsum tempore consequuntur perspiciatis vero. Atque commodi architecto nulla voluptatibus ducimus doloremque? Fugiat, atque iure. Cumque alias corporis cupiditate asperiores illum similique nemo eligendi ipsum vitae libero. Asperiores similique repellendus nulla ratione atque. Nisi alias ipsum animi atque obcaecati ex, quisquam natus harum officia asperiores doloribus, corrupti dicta excepturi sint. Optio, tenetur? Facilis provident ratione perferendis architecto veritatis veniam molestiae distinctio soluta odio, sint quasi dolores nisi hic, sunt enim facere. Magni eos asperiores itaque nemo impedit totam expedita veniam molestias nihil voluptatem nobis, accusamus cumque laborum in veritatis doloremque rem aut at explicabo id! Ipsum repellat exercitationem adipisci non alias esse sequi vel laudantium ex officiis nemo eos, numquam at quasi obcaecati maiores fuga expedita aperiam sed tempora. Quia, saepe? Assumenda placeat voluptate fugit obcaecati. Quis dolor iure ipsum. Cumque itaque fugit id omnis at nesciunt natus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam hic asperiores repellat esse odio fuga reiciendis ratione, deleniti harum culpa quas amet modi id ab corrupti cum corporis quae ex! Eveniet facere iusto officia. Accusantium excepturi quia facere obcaecati, nulla ipsam cum quis quibusdam nisi repellendus voluptatem laboriosam tempore vitae rem voluptate minus corporis nostrum quaerat voluptatibus ea. Inventore maiores vel ipsa quibusdam eius laboriosam cumque veritatis nihil. Itaque, aliquam facere! Assumenda mollitia, possimus aspernatur fugiat itaque, inventore a, numquam nemo saepe recusandae dolorem! Vero explicabo ea, magni dicta minima quidem adipisci consequatur id velit suscipit voluptatum numquam facere delectus autem molestias consequuntur quis possimus quo. Incidunt consectetur totam quia sapiente itaque facere! Doloremque veniam iste repudiandae corporis tenetur, illo consequatur quasi harum, voluptatibus distinctio consectetur fuga recusandae provident exercitationem molestias vero eligendi modi laudantium minus voluptatem illum necessitatibus eum? Dolores atque, laborum dolore voluptas accusamus aliquam iste ipsum, vero velit ut perferendis, magni sed vel id sequi amet ad totam corporis nihil nostrum architecto a provident. Exercitationem consectetur maiores recusandae sed minus fugit dolorum in deserunt, quidem hic quos perspiciatis eos. Molestias soluta iste optio dignissimos magnam itaque accusantium dicta praesentium obcaecati eaque? Iste ipsam nisi impedit suscipit, inventore rem consequuntur doloribus odit excepturi, atque aliquid, quas dicta. Praesentium, adipisci voluptatem. Iusto, praesentium ab. Repudiandae rerum quaerat alias impedit doloribus dolor nulla maxime dolores libero error, atque autem deserunt officiis minima sequi porro iure fuga mollitia eos at neque voluptas voluptatum obcaecati? Facere quas alias aspernatur, sed temporibus maxime ea repellendus. Consequatur repudiandae necessitatibus consectetur ipsa. Cumque, minima ratione? Aspernatur, nihil porro! Incidunt suscipit autem itaque numquam doloribus in esse minima, sunt at deleniti tempore repellat nobis adipisci placeat eum impedit eveniet reiciendis fuga aut ipsum ipsam natus earum voluptatibus! Consectetur doloribus, quam, aliquam consequatur recusandae voluptate rem pariatur tenetur quo aperiam voluptates repellat! Perferendis corrupti facilis eligendi rerum corporis odit, laboriosam commodi quam temporibus dolore labore dicta adipisci, eaque modi cumque libero doloribus aspernatur in accusamus nostrum aperiam. Cupiditate tenetur architecto, veritatis placeat quaerat doloremque ipsum facilis quia est iure minus possimus illum reiciendis laboriosam illo perferendis, magni ipsa sunt dolor sint! Iusto a deserunt molestiae dolore optio omnis exercitationem, similique eaque ad minus, obcaecati laboriosam ratione velit quae hic expedita odit rerum non atque! Consequuntur similique tempora minus aut officiis sunt ratione molestias fuga sapiente nulla quaerat id ullam aliquid vero at architecto, quam ipsum qui veritatis debitis? Doloremque natus qui placeat! Amet quis illum ducimus ullam nesciunt a minus voluptate non rem quia fugiat quod laborum esse quo sit, cumque veniam nostrum deserunt! Aliquam quae, minima dolores quaerat minus voluptate harum quo assumenda eveniet, doloribus temporibus quisquam, odit adipisci nam? Molestias cumque, accusamus deserunt doloribus adipisci perspiciatis ullam consequatur iusto consectetur obcaecati, ad culpa ipsum, eum saepe dicta? Eligendi nobis doloremque iusto velit non amet voluptates dolores fugit voluptatum doloribus earum dolore ex corrupti distinctio ut aliquid, autem incidunt! Inventore laboriosam asperiores eveniet molestiae nihil totam ea quaerat culpa ipsum, saepe corrupti laborum. Dolorem perferendis blanditiis, repudiandae inventore maxime, nihil facilis nisi minima alias libero quod totam mollitia voluptatum molestias. Magni rerum officiis, doloremque illo ea voluptatum? Dolore perspiciatis nam iure consectetur doloremque vitae explicabo enim! Beatae molestias repudiandae commodi. Alias velit voluptates repellendus? Magni illo cumque quisquam ea et eius veniam distinctio molestias laboriosam corporis inventore aliquam vero autem hic nam ipsum culpa consectetur placeat, iste eos dolore. Ducimus fugit tempore facere earum ipsum ipsa accusantium, nulla eum inventore temporibus excepturi sed repudiandae soluta eligendi, minus iure. Velit voluptates, similique deserunt id nam, blanditiis magnam incidunt, rerum harum autem ipsum repudiandae assumenda hic ratione itaque ipsa aspernatur reprehenderit ut ex temporibus quaerat laboriosam eveniet. Fugit, nihil! Praesentium quaerat voluptatibus distinctio provident nostrum impedit error earum officiis in ex corrupti quos assumenda, tempora ducimus repudiandae vitae eius sequi dolorem delectus minima, laborum voluptatum obcaecati sapiente eum. Ut suscipit et a, amet magni reprehenderit repellat fugiat est eligendi fugit exercitationem. Quibusdam, suscipit soluta atque, ex officia optio minima reiciendis itaque distinctio illum nam accusamus totam in nesciunt pariatur aliquam labore eius porro veritatis blanditiis ipsam at libero. Nemo alias laboriosam eum cum odio et? Saepe magni quisquam, laudantium dolores qui aperiam fugit reiciendis doloremque, in inventore error. Praesentium nihil quisquam veritatis expedita aut ipsam harum illum reprehenderit sint, eos vitae quidem nisi voluptates quia sit cupiditate consequuntur earum rem autem repellat velit! Officiis impedit velit consectetur temporibus eos beatae recusandae, sunt maxime enim voluptate id. Quidem quas quae architecto deleniti error optio veniam quaerat alias. Ea tenetur cum dolor aspernatur, blanditiis eius nisi eveniet labore molestiae reprehenderit culpa rerum consequuntur neque inventore eos fuga voluptatem obcaecati deserunt quia tempora. Nulla provident adipisci quo saepe eius, repellat enim mollitia sequi est nisi incidunt libero minus rerum cumque, nam odio? Consequatur explicabo laboriosam itaque atque incidunt quas dolores non, nisi ipsam, alias aperiam at blanditiis, animi fugit enim distinctio accusamus. Ea neque enim, obcaecati esse autem nesciunt velit at, a iusto delectus quaerat consectetur porro architecto vitae dolorem inventore eligendi quasi, doloremque beatae blanditiis cupiditate ab. Doloremque suscipit officia vero, nihil explicabo quis enim aliquam? Sequi, animi fuga repellat accusamus nihil officia aliquam doloremque voluptatum iure itaque tempora consectetur eligendi, culpa incidunt, dolorum accusantium error saepe! Architecto dolor omnis ad corporis accusamus repudiandae ex saepe totam voluptate placeat necessitatibus at repellendus est nihil, laboriosam sapiente. Consectetur corrupti voluptatibus ad excepturi quam doloribus quis debitis fuga deserunt! Quam, labore officia nostrum at expedita aliquid nisi quo veniam ad. Debitis magni saepe exercitationem, corrupti nesciunt possimus iure facere soluta incidunt maiores, iste officia eius, consectetur quia! Assumenda vitae quasi eveniet, eum esse fugit adipisci enim distinctio inventore. Accusamus dolor commodi autem, veniam quo nisi voluptatem aperiam consequatur nesciunt totam at facilis tempore dignissimos. Sunt, necessitatibus accusantium cum saepe animi obcaecati mollitia facere nulla laudantium? Inventore magni sequi quos maxime ex doloribus hic neque atque veritatis, nobis veniam expedita at reprehenderit recusandae quo cumque repellendus nesciunt deleniti labore nihil nostrum. Quibusdam ullam consectetur rerum est incidunt veniam earum cum dolore minima eius!</div>

<div className="bg-cover" style={{ backgroundImage: 'url(https://images.unsplash.com/photo-1698310876902-1138bc8ec442?auto=format&fit=crop&q=80&w=1887&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)' }}>
  Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui, reiciendis mollitia corrupti maiores id, accusamus aperiam, rem doloribus quis laboriosam quam delectus ut autem. Fugiat ipsa incidunt aperiam, ut ea corporis temporibus voluptatum? At magni temporibus similique. Ipsum nisi numquam sunt voluptatibus harum sint sed ducimus inventore! Sequi, vero mollitia.
</div>

<div className="h-14 bg-gradient-to-r from-cyan-500 to-blue-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet autem natus maiores totam dolore facere molestiae aspernatur recusandae atque quas inventore perferendis ullam dolor, voluptas dignissimos eaque. Eaque sunt ea libero impedit nobis eos minima consectetur fugit dolorem qui, totam dolore magni, odio dolores? Recusandae voluptatum veniam eligendi, nihil dolorum laborum veritatis asperiores saepe magni illo numquam totam. Libero, facere. Dicta, veniam officiis voluptatem adipisci atque illo eum. Ullam, quos officiis assumenda laboriosam sit debitis reprehenderit laborum quaerat quam voluptas consectetur voluptatibus sequi natus saepe accusamus sapiente odio totam fugiat earum. Amet provident aperiam repellendus expedita dolores enim earum tempore.</div>
<div className="h-14 bg-gradient-to-r from-sky-500 to-indigo-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet autem natus maiores totam dolore facere molestiae aspernatur recusandae atque quas inventore perferendis ullam dolor, voluptas dignissimos eaque. Eaque sunt ea libero impedit nobis eos minima consectetur fugit dolorem qui, totam dolore magni, odio dolores? Recusandae voluptatum veniam eligendi, nihil dolorum laborum veritatis asperiores saepe magni illo numquam totam. Libero, facere. Dicta, veniam officiis voluptatem adipisci atque illo eum. Ullam, quos officiis assumenda laboriosam sit debitis reprehenderit laborum quaerat quam voluptas consectetur voluptatibus sequi natus saepe accusamus sapiente odio totam fugiat earum. Amet provident aperiam repellendus expedita dolores enim earum tempore.</div>
<div className="h-14 bg-gradient-to-r from-violet-500 to-fuchsia-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet autem natus maiores totam dolore facere molestiae aspernatur recusandae atque quas inventore perferendis ullam dolor, voluptas dignissimos eaque. Eaque sunt ea libero impedit nobis eos minima consectetur fugit dolorem qui, totam dolore magni, odio dolores? Recusandae voluptatum veniam eligendi, nihil dolorum laborum veritatis asperiores saepe magni illo numquam totam. Libero, facere. Dicta, veniam officiis voluptatem adipisci atque illo eum. Ullam, quos officiis assumenda laboriosam sit debitis reprehenderit laborum quaerat quam voluptas consectetur voluptatibus sequi natus saepe accusamus sapiente odio totam fugiat earum. Amet provident aperiam repellendus expedita dolores enim earum tempore.</div>
<div className="h-14 bg-gradient-to-r from-purple-500 to-pink-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet autem natus maiores totam dolore facere molestiae aspernatur recusandae atque quas inventore perferendis ullam dolor, voluptas dignissimos eaque. Eaque sunt ea libero impedit nobis eos minima consectetur fugit dolorem qui, totam dolore magni, odio dolores? Recusandae voluptatum veniam eligendi, nihil dolorum laborum veritatis asperiores saepe magni illo numquam totam. Libero, facere. Dicta, veniam officiis voluptatem adipisci atque illo eum. Ullam, quos officiis assumenda laboriosam sit debitis reprehenderit laborum quaerat quam voluptas consectetur voluptatibus sequi natus saepe accusamus sapiente odio totam fugiat earum. Amet provident aperiam repellendus expedita dolores enim earum tempore.</div>

<button className="bg-indigo-500 px-3 py-2 mt-4 mb-4 rounded">
  Save changes
</button>

<div className="flex gap-4 p-4">
<button className="outline outline-offset-2 outline-1 outline-lime-300 rounded-lg ...">Button A</button>
<button className="outline outline-offset-2 outline-2 outline-lime-400 rounded-xl ...">Button B</button>
<button className="outline outline-offset-2 outline-4  outline-lime-500 rounded-s-sm rounded-xl ...">Button C</button>
</div>

<div className="w-3/4 gap-24 justify-between flex">
<div className="shadow-md shadow-sky-600	 ...">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam sapiente fugit ipsam delectus dicta ullam commodi earum atque hic blanditiis.</div>
<div className="shadow-lg shadow-indigo-600	 ...">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam sapiente fugit ipsam delectus dicta ullam commodi earum atque hic blanditiis.</div>
<div className="shadow-xl ...">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam sapiente fugit ipsam delectus dicta ullam commodi earum atque hic blanditiis.</div>
<div className="shadow-2xl ...">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam sapiente fugit ipsam delectus dicta ullam commodi earum atque hic blanditiis.</div>
</div>

<div className="shadow-inner ..."></div>

<div className="opacity-50 bg-teal-400 hover:opacity-100 w-1/2">
  Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus quam eaque consequuntur ut dolorum debitis deleniti! Voluptates tempora voluptas minus, vitae quidem rem natus illo velit sint tenetur ea at, deleniti facilis reiciendis porro amet ut reprehenderit voluptate nisi dolore.
</div>

{/* <article className="prose lg:prose-xl">
  {{ markdown }}
</article> */}

You can actually customize padding on a select element now:
<select className="px-4 py-3 rounded-full">
 Identifier
</select>

Or change a checkbox color using text color utilities
<input type="checkbox" className="rounded text-pink-500" />

    </div>
  )
}

export default App