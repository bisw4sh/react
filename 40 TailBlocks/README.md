# React + Vite + pnpm
```bash
pnpm install
pnpm run dev || pnpm dev
```
__Tailwind Configuration__
```bash
pnpm install -D tailwindcss postcss autoprefixer
pnpm dlx tailwindcss init -p
```

### tailwind.config.js

```sh
/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

### index.css
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

# Have dark mode in Tailwind CSS

### tailwind.config.js

```sh
darkMode: "class",
```
inside of **export default**

### App.jsx

```js
  const [theme, setTheme] = useState(null);

  useEffect(() => {
    if(localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)){
      setTheme('dark')
    }
    else {
      setTheme('light')
    }
  }, [])

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark")
    } else {
      document.documentElement.classList.remove("dark")
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem('theme', `${theme === "dark" ? "light" : "dark"}`)
  }
```

- add **handleThemeSwitch** function as the event handler to toggle modes.