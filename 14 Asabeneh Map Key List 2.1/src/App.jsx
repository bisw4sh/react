import "./App.css"

const checkPrime = (index) => {

  if(index === 0 || index === 1 ) 
    return false

  for(let i = 2; i < index; i++){

    if(index % i === 0)
      return false
    
  }
  return true
}

const Square = ({ index }) => {
  const isPrime = checkPrime(index)
  const returnJSX = isPrime ? <div className="square prime"><h2>{index}</h2></div> : <div className="square"><h2>{index}</h2></div>
  return (returnJSX)
}

const Box = () => {
  const squares = [];
  for (let i = 0; i < 32; i++) {
    squares.push(<Square key={i} index={i} />)
  }
  return squares;
}

const App = () => {
  return (
    <div className="Content">
      <h1>30 Days of React</h1>
      <h3>Number Generator</h3>
      <div className="box">
        <Box />
      </div>
    </div>
  )
}

export default App