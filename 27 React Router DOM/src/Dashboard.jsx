import { Link,Outlet } from 'react-router-dom'

const Dashboard = () => {
  return (
    <div>
      <h1>This is Dashboard</h1>
      <Link to = '/'>Home Page</Link>
      <Link to = '/dashboard/nested'>Dashboard/Nested</Link>
      <Outlet />
    </div>
  )
}

export default Dashboard