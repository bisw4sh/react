import { Link } from 'react-router-dom'

const Nested = () => {
  return (
    <div>
      <h1>This is Nested Page</h1>
      <br />
      <Link to='/'>Homepage</Link>
    </div>
  )
}

export default Nested