import { useState } from "react";
import { useNavigate, Outlet, Link } from "react-router-dom";
import ThemeToggle from "./ThemeToggle";
import useDebounce from "../hooks/useDebounce";

export default function Navbar() {
  const [mode, setMode] = useState<"light" | "dark">("light");
  const navigate = useNavigate();

  const handleNavigation = (path: string) => {
    navigate(path);
  };

  const debouncedNavigate = useDebounce(handleNavigation, 5000);

  return (
    <div data-theme={mode} className="space-x-4 p-4">
      <ThemeToggle mode={mode} setMode={setMode} />
      <button onClick={() => debouncedNavigate("/")} className="link link-info">
        Home
      </button>
      <Link to="/main" className="link link-info">
        Main
      </Link>
      <Outlet />
    </div>
  );
}
