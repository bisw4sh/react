import { Outlet, useSearchParams } from "react-router-dom";
import { twMerge } from "tailwind-merge";
import FileOptions from "./FileOptions";

const Options = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const level = searchParams.get("level");

  return (
    <>
      <main className="flex flex-col items-center gap-3 py-3">
        <div role="tablist" className="tabs tabs-lifted w-[99%]">
          <button
            role="tab"
            onClick={() => setSearchParams({ level: "easy" })}
            className={twMerge(
              "tab",
              level === "easy"
                ? "tab-active text-primary [--tab-bg:cyan]"
                : "tabs-lifted",
            )}
          >
            Easy
          </button>
          <button
            role="tab"
            className={twMerge(
              "tab",
              level === "intermediate"
                ? "tab-active text-primary [--tab-bg:cyan]"
                : "tabs-lifted",
            )}
            onClick={() => setSearchParams({ level: "intermediate" })}
          >
            Intermediate
          </button>
          <button
            role="tab"
            className={twMerge(
              "tab",
              level === "expert"
                ? "tab-active text-primary [--tab-bg:cyan]"
                : "tabs-lifted",
            )}
            onClick={() => setSearchParams({ level: "expert" })}
          >
            Expert
          </button>
        </div>

        {level ? <FileOptions /> : null}
      </main>
      <Outlet />
    </>
  );
};

export default Options;
