import { useLocation, useNavigate } from "react-router-dom";
import useDebounce from "../hooks/useDebounce";

const FileOptions = () => {
  const { search } = useLocation();
  const navigate = useNavigate();

  const handleNavigation = (path: string) => {
    navigate(path);
  };
  const debouncedNavigate = useDebounce(handleNavigation, 5000);

  return (
    <div className="flex w-full items-center justify-between">
      <button
        onClick={() => debouncedNavigate(`generative-ai${search}`)}
        className="link link-info"
      >
        Generative AI
      </button>
      <button
        onClick={() => debouncedNavigate(`web-development${search}`)}
        className="link link-info"
      >
        Web Development
      </button>
      <button
        onClick={() => debouncedNavigate(`ffmpeg${search}`)}
        className="link link-info"
      >
        FFmpeg
      </button>
      <button
        onClick={() => debouncedNavigate(`global-warming${search}`)}
        className="link link-info"
      >
        Global Warming
      </button>
    </div>
  );
};

export default FileOptions;
