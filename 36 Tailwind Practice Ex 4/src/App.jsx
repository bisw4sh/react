const App = () =>{

  return (
  <div className="h-screen flex justify-center items-center bg-indigo-950">

    <div className="Container h-fit w-2/5 max-sm:w-full max-md:w-4/5 flex flex-col gap-4 bg-blue-950 text-gray-50 p-6 rounded-lg">

      <h1 className="text-2xl font-semibold">Sign in to our platform</h1>

      <label htmlFor="email">
        Your email
      </label>
      <input type="email" id="email" placeholder="name@company.com" className="px-1 py-2 rounded-md bg-slate-600" />

      <label htmlFor="password">
        Your password
      </label>
      <input type="password" id="password" placeholder="**********" className="px-1 py-2 rounded-md bg-slate-600"/>

      <div>
 
      <div className="flex justify-between items-center my-3">
        <div>
            <input type="checkbox" id="remember" className="accent-blue-500 px-4"/>
            <label htmlFor="remember" className="px-2">
              Remember me
            </label>
        </div>

            <span className="text-blue-600">Lost Password?</span>
      </div>

      <button className="bg-blue-700 w-full py-3 rounded-lg">Login to your account</button>

      <div className="mt-4">
        Not registered?
            <span className="cursor-pointer text-blue-600">Create account</span>
      </div>
        
      </div>


    </div>

  </div>
  )
}

export default App