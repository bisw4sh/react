import { GrChannel } from "react-icons/gr";
import videos from './videoData.jsx'

const VideoComp = () => {

  return (
    <>
    {
        videos.map(({img, title, channel, views, uploaded}, index) => {

            return (
            <div className='single-video bg-zinc-300 dark:bg-zinc-900 rounded-lg max-w-96 min-w-64' key={index}>
                <img src={img} alt="Video Image" className='rounded-lg w-full' />
    
                <div className='p-3 flex justify-start'>
                <GrChannel className="text-2xl dark:fill-white"/>
                    <div className="right-info ml-3">
                        <div className='title text-lg max-md:text-sm font-semibold max-xl:text-base dark:text-zinc-100'>
                        {title}
                        </div>
                        <div className='channel-name text-md max-lg:text-xs max-xl:text-sm font-normal dark:text-zinc-100'>
                        {channel}
                        </div>
                        <div className='views-time text-md max-xl:text-sm font-normal dark:text-zinc-100'>
                        {views} views &#8226; {uploaded} ago
                        </div>
                    </div>
                </div>
            </div>
            )

        })
    }
    </>
  )
}

export default VideoComp