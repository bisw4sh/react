import { useState } from 'react';
import { GiHamburgerMenu } from "react-icons/gi"
import { GrYoutube } from "react-icons/gr";
import { BsSearch } from "react-icons/bs";
import { FaMicrophone } from "react-icons/fa";
import { RiVideoAddLine } from "react-icons/ri";
import { VscBell } from "react-icons/vsc";
import { FcBusinessman } from "react-icons/fc";

const Menubar = ({handleThemeSwitch, setCollapse, collapse}) => {
    const [search, setSearch] = useState(false)

  return (
    <div className="flex justify-between">
        <div className="left-side flex items-center gap-5">
            <GiHamburgerMenu className="dark:fill-white text-2xl cursor-pointer active:fill-fuchsia-600"
            onClick={() => setCollapse(!collapse)}/>
            <span className="flex items-center">
                <GrYoutube className="fill-red-600 text-3xl text-zinc-50 active:fill-fuchsia-600"/>
                <span className="dark:text-zinc-50 font-semibold leading-9 px-1 text-xl font-poppins cursor-pointer">YouTube</span>
                <sup className="dark:text-zinc-50">NP</sup>
            </span>
        </div>

        <div className="central relative flex justify-center items-center">
        {search ? (<button className="absolute -left-4 h-full py-2 px-3 rounded-r-full">
                <BsSearch className="fill-gray-200 text-xl ml-4 active:fill-fuchsia-600" />
            </button>) : <div></div>}

            <input type="text" placeholder="Search" className="w-96 h-full dark:bg-gray-950 py-1 px-5 rounded-l-full 
            focus:border-none focus:dark:outline-violet-300 outline-1 dark:text-zinc-50 border-2 dark:border-slate-800 flex items-center focus:pl-11"
            onFocus={() => setSearch(true)}
            onBlur={() => setSearch(false)} />

            <button className="dark:bg-gray-950 bg-gray-300 h-full py-2 px-2 rounded-r-full">
                <BsSearch className="fill-gray-200 text-2xl ml-2 active:fill-fuchsia-600" />
            </button>
            <button className="dark:bg-gray-900 bg-gray-300 p-2 rounded-full ml-4">
                <FaMicrophone className="fill-gray-200 text-2xl active:fill-fuchsia-600"/>
            </button>
        </div>

        <div className="right flex justify-center items-center gap-6">
            <button>
                <RiVideoAddLine className="dark:fill-gray-200 text-2xl active:fill-fuchsia-600"/>
            </button>
            <button>
                <VscBell className="dark:fill-gray-200 text-2xl active:fill-fuchsia-600"/>
            </button>
            <button>
                <FcBusinessman className="dark:fill-gray-200 text-2xl active:fill-fuchsia-600" onClick={handleThemeSwitch}/>
            </button>
        </div>
    </div>
  )
}

export default Menubar