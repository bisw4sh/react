import topics from './topicData.jsx'
import VideoComp from './VideoComp.jsx';

const Central = () => {

  return (
    <div className="min-h-screen max-h-fit w-fit text-justify p-3  basis-full overflow-x-clip ">
        <div className='h-8 flex'>
            {topics.map ((topic, index) => <span key={index} className='px-2 m-1 rounded-sm dark:bg-zinc-500 bg-zinc-200 font-semibold dark:text-white cursor-pointer'>{topic}</span>)}
        </div>

        <div className='rounded-lg mt-4 ml-2 grid grid-cols-4  max-lg:grid-cols-3 max-md:grid-cols-2 max-sm:grid-cols-1 gap-5'>
            <VideoComp />
        </div>
    </div>
  )
}

export default Central