import { MdPlaylistPlay } from "react-icons/md";

const ShowMore = () => {
  return (
    <>
        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 1</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 2</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 3</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 4</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 5</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 6</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 7</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 8</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 9</span>
        </div>
        
        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdPlaylistPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">PlayList 10</span>
        </div>
    </>
  )
}

export default ShowMore