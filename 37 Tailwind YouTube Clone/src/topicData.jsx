const data = [
    'All',
    'Gaming',
    'Music',
    'Mixes',
    'Sports',
    'Live',
    'Manga',
    'Musical',
    'Sketch',
    'React',
    'Film',
    'History',
    'Python',
    'Love',
    'Podcast',
    'Smartphones',
    'Game',
    'PUBG',
    'Recently',
    'Watched',
    'New'
]

export default data;