import { useState, useEffect } from 'react'
import Menubar from './Menubar.jsx'
import SideBarC from './SideBarC.jsx'
import SideBar from './SideBar.jsx'
import Central from './Central.jsx'

export default function App() {

  const [theme, setTheme] = useState(null);
  const [collapse, setCollapse] = useState(true)

  useEffect(() => {
    if(localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)){
      setTheme('dark')
    }
    else {
      setTheme('light')
    }
  }, [])

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark")
    } else {
      document.documentElement.classList.remove("dark")
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem('theme', `${theme === "dark" ? "light" : "dark"}`)
  }

  return (
    <div className="h-fit px-6 py-3 dark:bg-black select-none" >
      <Menubar handleThemeSwitch={handleThemeSwitch} collapse={collapse} setCollapse={setCollapse}/>
      <div className='flex flex-row justify-start items-stretch'>
      {collapse ? <SideBarC /> : <SideBar /> }
      <Central />
      </div>
    </div>
  )
}