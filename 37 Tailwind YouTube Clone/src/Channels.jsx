import { FcAddressBook } from "react-icons/fc";

const Channels = () => {
  return (
    <>
        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 5</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 6</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 7</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 8</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 9</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Channel 10</span>
        </div>
    </>
  )
}

export default Channels