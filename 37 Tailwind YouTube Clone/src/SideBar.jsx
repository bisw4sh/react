import { useState } from 'react';
import { VscHome, VscHistory } from "react-icons/vsc";
import { BsCollectionPlay, BsChevronRight, BsFire, BsMusicNote } from "react-icons/bs";
import { AiOutlinePlaySquare, AiOutlineDown, AiOutlineUp } from "react-icons/ai";
import { MdOutlineWatchLater } from "react-icons/md";
import { HiMiniScissors } from "react-icons/hi2";
import { BiLike, BiHelpCircle, BiUserCircle } from "react-icons/bi";
import { FcAddressBook } from "react-icons/fc";
import { SiYoutubegaming, SiYoutubestudio, SiYoutubemusic } from "react-icons/si";
import { GiTrophyCup } from "react-icons/gi";
import { GrYoutube } from "react-icons/gr";
import { TbBrandYoutubeKids } from "react-icons/tb";
import { FiSettings, FiFlag } from "react-icons/fi";
import { RiFeedbackLine } from "react-icons/ri";
import ShowMore from "./ShowMore.jsx";
import Channels from './Channels.jsx';


const SideBar = () => {
    const [more, setMore] = useState(false)
    const [channels, setChannels] = useState(false)

  return (
    <div className="w-fit pr-2">

        <div className="flex gap-3 items-center py-2 cursor-pointer  hover:bg-zinc-300 rounded px-1">
            <VscHome className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Home</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <BsCollectionPlay className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50">Subscriptions</span>
        </div>

        <hr className='my-2'/>

        <div className="flex gap-2 items-center font-bold dark:text-zinc-50">
            You
            <span>
                <BsChevronRight className="text-lg dark:fill-white cursor-pointer hover:bg-zinc-300 rounded px-1"/>
            </span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1 ">
            <BiUserCircle className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Your Channel</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <VscHistory className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">History</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <AiOutlinePlaySquare className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Your Videos</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <MdOutlineWatchLater className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Watch Later</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <HiMiniScissors className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Your Clips</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <BiLike className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50">Liked videos</span>
        </div>
        {more ? <ShowMore className="dark:fill-white"/> : <div></div>}
        
        {/* The show more/less string template only works if clicked on the text but doesnot when clicked on the svg/icon */}
        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1" onClick={() => setMore(!more)}>
            {more ? 
            (<AiOutlineUp className="text-2xl dark:fill-white "/>): 
            (<AiOutlineDown className="text-2xl dark:fill-white "/>)}
            <span className="text-base dark:text-zinc-50">Show { more ? 'Less' : 'More'}</span>
        </div>

        <hr className='my-2'/>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1" >
            <span className="text-base font-bold dark:text-zinc-50" >Subscriptions</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Channel 1</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Channel 2</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Channel 3</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FcAddressBook className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Channel 4</span>
        </div>

        {channels ? <Channels/> : <div></div>}
        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1" onClick={() => setChannels(!channels)}>
            {channels ? 
            (<AiOutlineUp className="text-2xl dark:fill-white "/>):
            (<AiOutlineDown className="text-2xl dark:fill-white "/>)}  
            <span className="text-base dark:text-zinc-50" onClick={ () => setChannels(!channels)}>Show {channels === true ? 'Less': 'More'}</span>
        </div>


        <hr className='my-2'/>

        <div className="flex gap-3 items-center" >
            <span className="text-base font-bold dark:text-zinc-50" >Explore</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <BsFire className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Trending</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <BsMusicNote className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Music</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <SiYoutubegaming className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Gaming</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <GiTrophyCup className="text-2xl dark:fill-white"/>
            <span className="text-base dark:text-zinc-50" >Sports</span>
        </div>

        <hr className='my-2'/>

        <div className="flex gap-3 items-center" >
            <span className="text-base font-bold dark:text-zinc-50" >More from YouTube</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <GrYoutube className="text-2xl fill-red-600"/>
            <span className="text-base dark:text-zinc-50" >YouTube Premium</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <SiYoutubestudio className="text-2xl fill-red-600"/>
            <span className="text-base dark:text-zinc-50" >YouTube Studio</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <SiYoutubemusic className="text-2xl fill-red-600"/>
            <span className="text-base dark:text-zinc-50" >YouTube Music</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <TbBrandYoutubeKids className="text-2xl fill-red-600"/>
            <span className="text-base dark:text-zinc-50" >YouTube Kids</span>
        </div>

        <hr className='my-2'/>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FiSettings className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Settings</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <FiFlag className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Report History</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <BiHelpCircle className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Help</span>
        </div>

        <div className="flex gap-3 items-center py-2 cursor-pointer hover:bg-zinc-300 rounded px-1">
            <RiFeedbackLine className="text-2xl dark:fill-white "/>
            <span className="text-base dark:text-zinc-50" >Send Feedback</span>
        </div>

        <div className='text-gray-500 dark:text-zinc-500'>&copy; 2023 Google LLC</div>
    </div>
  )
}

export default SideBar