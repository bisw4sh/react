import { VscHome } from "react-icons/vsc";
import { BsCollectionPlay } from "react-icons/bs";
import { AiOutlinePlaySquare } from "react-icons/ai";

const SideBarC = () => {
  return (
    <div className=" -ml-5 h-full w-fit flex flex-col items-center gap-10 text-xs mt-8">

      <div className="flex flex-col justify-center items-center cursor-pointer">
        <VscHome className="text-2xl dark:fill-white"/>
        <span className="dark:text-white">Home</span>
      </div>

      <div className="flex flex-col justify-center items-center cursor-pointer">
        <BsCollectionPlay className="text-2xl dark:fill-white"/>
        <span className="dark:text-white">Subscriptions</span>
      </div>

      <div className="flex flex-col justify-center items-center cursor-pointer">
        <AiOutlinePlaySquare className="text-2xl dark:fill-white"/>
        <span className="dark:text-white">You</span>
      </div>

    </div>
  )
}

export default SideBarC