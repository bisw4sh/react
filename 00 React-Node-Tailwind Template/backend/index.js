import express from "express";
const app = express();

app.use(express.json());
app.get("/api", async (req, res) => {
  res.send("It is working");
});

app.listen(8080, console.log("server running @ http://localhost:8080"));
