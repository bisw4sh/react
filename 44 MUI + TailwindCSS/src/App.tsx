import Button from "@mui/material/Button";
import MovieSelect from "./components/MovieSelect.tsx";
import CountrySelect from "./components/CountrySelect";
import DialogBox from "./components/DialogBox";
import { useState, useEffect } from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Skeleton from './components/Skeleton.tsx'
import Accordion from './components/Accordion.tsx'
import Card from './components/Card.tsx'
import BottomNavigation from './components/BottomNavigation.tsx'
import Drawer from './components/Drawer.tsx'
import SpeedDial from './components/SpeedDial.tsx'
import Stepper from './components/Stepper.tsx'

export default function App() {
  const darkTheme = createTheme({
    palette: {
      mode: "dark",
    },
  });

  const lightTheme = createTheme({
    palette: {
      mode: "light",
    },
  });

  const [theme, setTheme] = useState<"light" | "dark" | null>(null);

  useEffect(() => {
    if (
      localStorage.theme === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }, []);

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem("theme", `${theme === "dark" ? "light" : "dark"}`);
  };

  return (
    <ThemeProvider theme={theme === "dark" ? darkTheme : lightTheme}>
      <CssBaseline />
      <div className="py-5 flex flex-col py-5 justify-center items-center gap-4 dark:bg-zinc-950 dark:text-zinc-50">
        <h1 className="text-3xl font-bold underline ">Hello world!</h1>
        <Button variant="contained" onClick={handleThemeSwitch}>
          Toggle Mode
        </Button>
        <h1 className="">AutoComplete</h1>
        <MovieSelect />
        <CountrySelect />
        <DialogBox />
        <div>
          <Skeleton />
        </div>
        <div className="w-1/3">
          <Accordion />
        </div>
        <Card />
        <BottomNavigation />
        <Drawer />
        <SpeedDial />
        <Stepper />
      </div>
    </ThemeProvider>
  );
}
