import express from "express";

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Its working");
});

app.post("/api/signin", (req, res) => {
  console.log(req.body)
  res.end()
})

app.listen(8080, () => console.log(`Running @ http://localhost:8080`));
