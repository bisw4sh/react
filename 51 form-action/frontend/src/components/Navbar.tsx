import { Outlet, NavLink } from "react-router-dom";
// import { userContext, AuthContextType } from "../context/AuthContext";
// import { useContext } from "react";

export default function Navbar() {
  //   const { user, setUser } = useContext(userContext) as AuthContextType;

  return (
    <div className="px-8 py-4">
      <nav className="w-full flex justify-between">
        <div className="flex gap-4">
          <NavLink
            to="/"
            className="text-teal-400 hover:text-teal-600 hover:scale-105"
          >
            Home
          </NavLink>
          <NavLink
            to="/signin"
            className="text-teal-400 hover:text-teal-600 hover:scale-105"
          >
            Sign In
          </NavLink>
        </div>
      </nav>
      <Outlet />
    </div>
  );
}
