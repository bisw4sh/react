import { Form, redirect, useActionData } from "react-router-dom";

export const action = async ({ request }: { request: Request }) => {
  const formData = await request.formData();
  const formObject = Object.fromEntries(formData);

  try {
    const response = await fetch("/api/signin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formObject),
    });
    console.log(response);
  } catch (error) {
    console.log(error);
  }
  console.log(formObject);

  return redirect("/");
};

export default function SignIn() {
  const actionData = useActionData();
  console.log(actionData);

  return (
    <div className="min-h-screen flex flex-col justify-center items-center ">
      <Form
        // action="/signin"
        method="POST"
        className="p-4 rounded-lg h-64 w-1/3 bg-zinc-700 flex flex-col justify-between items-center"
      >
        <h1 className="text-2xl font-semibold">Sign In Form</h1>
        <input
          type="text"
          name="email"
          className="input w-full max-w-xs"
          placeholder="Email"
        />
        {/* <span>{actionData?.email}</span> */}

        <input
          type="password"
          name="password"
          className="input w-full max-w-xs"
          placeholder="Password"
        />

        <button type="submit" className="btn">
          Log in
        </button>
      </Form>
    </div>
  );
}
