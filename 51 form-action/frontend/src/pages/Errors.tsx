import { Link } from "react-router-dom";

export default function Errors() {
  return (
    <div className="min-h-screen flex justify-center items-center flex-col gap-4">
      <span>There has been error</span>
      <button className="btn btn-active">
        <Link to="/">Go back to Home</Link>
      </button>
    </div>
  );
}
