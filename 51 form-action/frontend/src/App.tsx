import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route,
} from "react-router-dom";
import Navbar from "./components/Navbar";
import Homepage from "./pages/Homepage";
import SignIn, { action as signInAction } from "./pages/SignIn";
import Errors from "./pages/Errors";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Navbar />} errorElement={<Errors />}>
      <Route index element={<Homepage />} />
      <Route path="signin" element={<SignIn />} action={signInAction} />
    </Route>
  )
);

export default function App() {
  return <RouterProvider router={router} />;
}
