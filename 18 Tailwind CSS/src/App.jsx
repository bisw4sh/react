export default function App() {
  return (
    <div className="flex flex-col gap-y-2 items-center justify-center h-screen" style={{ background: 'linear-gradient(to right, #12c2e9, #c471ed, #f64f59)', overflow: 'hidden' }}>
      <h1 className="text-3xl font-bold" style={{ background: '-webkit-linear-gradient(45deg, #9233ff, #343331, #2e00c7)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent'}}>
        Hello world!
      </h1>
      <h2 className="px-8 text-5xl hover:decoration-sky-500 text-center">Hello sir</h2>

      <button className="bg-sky-500 hover:bg-sky-700 px-3 py-2 rounded-xl">
        Save changes
      </button>

    </div>
  );
}