const ImageContainer = ({src}) => {

  return (
    <>
    <img src={src} alt="Image" className='currentImage'/>
    </>
  )
}

export default ImageContainer