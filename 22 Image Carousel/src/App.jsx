import { useState } from 'react'
import ImageContainer from './components/ImageContainer.jsx'
import btn from './assets/next.png'

const App = () => {

  const imageName = {
    'src' : [
      'https://images.unsplash.com/photo-1465572089651-8fde36c892dd?auto=format&fit=crop&q=80&w=1931&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1552872673-9b7b99711ebb?auto=format&fit=crop&q=80&w=2070&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1496753480864-3e588e0269b3?auto=format&fit=crop&q=80&w=1868&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1475924156734-496f6cac6ec1?auto=format&fit=crop&q=80&w=2070&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1619551964399-dad708b59b8b?auto=format&fit=crop&q=80&w=2070&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
      'https://images.unsplash.com/photo-1542068829-1115f7259450?auto=format&fit=crop&q=80&w=2070&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D']
  }

  const Button = ({className, onClick}) => <img src={btn} className={className} alt='next' onClick={onClick}/>

  const [currentIndex, setcurrentIndex] = useState(0)

  const handlePrev = () => setcurrentIndex( currentIndex === 0 ? imageName.src.length -1 : currentIndex - 1 )
  const handleNext = () => setcurrentIndex( currentIndex === imageName.src.length - 1 ? 0 : currentIndex + 1 )

  return (
    <>
    <Button className="prev btn" onClick={handlePrev}/>
      <ImageContainer  src={imageName.src[currentIndex]}/>
    <Button className="next btn" onClick={handleNext}/>

    </>
  )
}

export default App