import express from "express";

const app = express();
app.use(express.json());

app.get("/", (req, res) => {
  res.json({
    user: "Biswash",
    id: "br4ke",
    age: 24,
  });
});

app.get("/contact", (req, res) => {
  res.json({
    facebook: "Biswash Dhungana",
    instagram: "bisw4sh",
    linkedin: "biswashdhungana",
    github: "bisw4sh",
  });
});

app.listen(8080, console.log(`Server running @ http://localhost:8080`));
