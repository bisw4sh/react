import { useLoaderData } from "react-router-dom";
import getData from "../api/get_data";

export const loader = () => getData();

export default function HomePage() {
  const data = useLoaderData();
  console.log(data);

  return <div>HomePage</div>;
}
