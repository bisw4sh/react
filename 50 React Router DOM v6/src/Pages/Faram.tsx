import { Form, redirect, useLoaderData } from "react-router-dom";

export const loader = async () => {
  return "This is loader";
};

export const action = async ({ request } : {request : Request}) => {
  const data = await request.formData();
  const email = data.get("email");
  const password = data.get("password");
  console.log(email, password);
  return redirect("/")
};

export default function Faram() {
  const loaderData = useLoaderData()
  console.log(loaderData)
  return (
    <div className="min-h-screen bg-zinc-300 flex justify-center items-center flex-col ">
      <Form
        method="post"
        className="bg-zinc-600 flex justify-center items-center flex-col gap-3 p-8 rounded-xl"
        replace
      >
        <h1 className="text-2xl font-semibold">Form</h1>
        <input
          type="email"
          name="email"
          placeholder="Email"
          className="p-2 rounded-lg"
        />
        <input
          type="password"
          name="password"
          placeholder="Password"
          className="p-2 rounded-lg"
        />
        <button type="submit" className="bg-teal-400 px-3 py-1 rounded-md">
          Log In
        </button>
      </Form>
    </div>
  );
}
