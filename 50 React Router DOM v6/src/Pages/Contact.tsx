import { useLoaderData } from "react-router-dom";
import contactData from "../api/get_contact";

export const loader = () => contactData();

export default function Contact() {
  const data = useLoaderData();
  console.log(data);
  return <div>Contact</div>;
}
