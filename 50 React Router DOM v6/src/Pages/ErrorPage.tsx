import { useLoaderData } from "react-router-dom";
import sendError from "../api/sendError";

export const loader = () => sendError();

export default function ErrorPage() {
  const data = useLoaderData();
  console.log(data);
  return <div>This won't be displayed as the error element is put at the loader i.e sendError()</div>;
}
