import { Outlet, Navigate } from "react-router-dom";

export default function AuthRequired(): JSX.Element {
  const isLoggedIn: boolean = true;

  if (!isLoggedIn) {
    return <Navigate to="login" />;
  }
  return <Outlet />;
}
