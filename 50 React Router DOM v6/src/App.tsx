import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";

import HomePage, { loader as homepageLoader } from "./Pages/HomePage";
import Contact, { loader as contactLoader } from "./Pages/Contact";
import ErrorPage, { loader as errorLoader } from "./Pages/ErrorPage";
import Error from "./Pages/Error";
import NotFound from "./Pages/NotFound";
import LogIn from "./Pages/Protected/LogIn";
import AuthRequired from "./Pages/Protected/AuthRequired";
import Faram, {
  action as faramAction,
  loader as faramLoader,
} from "./Pages/Faram";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<HomePage />} loader={homepageLoader} />
      <Route path="contact" element={<Contact />} loader={contactLoader} />
      <Route
        path="error"
        element={<ErrorPage />}
        errorElement={<Error />}
        loader={errorLoader}
      />

      <Route element={<AuthRequired />}>
        <Route path="protected" element={<h1>This is secret information</h1>} />
      </Route>
      <Route path="login" element={<LogIn />} />

      <Route
        path="faram"
        element={<Faram />}
        loader={faramLoader}
        action={faramAction}
      />
      <Route path="*" element={<NotFound />} />
    </>
  )
);

export default function App() {
  return <RouterProvider router={router} />;
}
