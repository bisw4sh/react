export default async function sendError() {
  const data = await fetch("nonsense");
  if (!data.ok) {
    throw {
      message: "This is nonsense error",
      statusText: data.statusText,
      status: data.status,
    };
  }
  const resp = await data.json();
  return resp;
}
