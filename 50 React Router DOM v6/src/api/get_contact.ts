export default async function get_data() {
  const resp = await fetch("/api/contact");
  const data = resp.json();
  return data;
}
