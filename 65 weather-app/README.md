> To get started 

```js
pnpm install
pnpm dev
```

That will initiate the dev server and most likely throw the [development url](http://localhost:5173/)

Production Live [@](https://main--weather-nps-biswashdhungana.netlify.app/)
#### Things I did to get this working
- Designed the UI with placeholders
- Imported the required static and dynamic svgs
- The svg icons changes according to the specified weather code
- If nothing is specified in the input field, it fetches the current longitude and latitude from GeoAPI, which is required for weatherAPI.
- If location is specified, a fetch call is made to GeoAPI and longitude and latitude are acquired and passed to the fetch call of weatherAPI.
- WeatherAPI call is made to get max temperature, min temperature, wind speed, precipitation, rain probabilility of 5 days from today.
- F and C changes the temperatures units.
#### Note
API keys in env file may or maynot be available, so make your own.
#### Quirks noticed
- WeatherAPI misbehaved for some data regarding the far days temperature.



