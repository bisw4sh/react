import Icon0 from "/Icons/0.svg";
import Icon1 from "/Icons/1.svg";
import Icon2 from "/Icons/2.svg";
import Icon3 from "/Icons/3.svg";
import Icon4 from "/Icons/4.svg";
import Icon5 from "/Icons/5.svg";
import Icon6 from "/Icons/6.svg";
import Icon7 from "/Icons/7.svg";
import Icon8 from "/Icons/8.svg";
import Icon9 from "/Icons/9.svg";
import Icon10 from "/Icons/10.svg";
import Icon11 from "/Icons/11.svg";
import Favicon from "/Icons/favicon.svg";
import { weather_icon_map } from "./utility/weather_icon_map";

const ForecastCard = ({
  min,
  max,
  code,
  day,
}: {
  min?: number;
  max?: number;
  code: number;
  day?: string;
}) => {
  const code_str: string = !code ? "" : code!.toString();
  const weatherIcon = weather_icon_map[code_str];
  const display_string =
    weatherIcon !== undefined ? weatherIcon.toString() : "";

  const Selected_Icon =
    display_string === "0.svg"
      ? Icon0
      : display_string === "1.svg"
      ? Icon1
      : display_string === "2.svg"
      ? Icon2
      : display_string === "3.svg"
      ? Icon3
      : display_string === "4.svg"
      ? Icon4
      : display_string === "5.svg"
      ? Icon5
      : display_string === "6.svg"
      ? Icon6
      : display_string === "7.svg"
      ? Icon7
      : display_string === "8.svg"
      ? Icon8
      : display_string === "9.svg"
      ? Icon9
      : display_string === "10.svg"
      ? Icon10
      : display_string === "11.svg"
      ? Icon11
      : Favicon;

  return (
    <main className="w-full flex flex-col items-center justify-start">
      <img src={Selected_Icon} alt="Icon" className="h-12 w-12" />
      <div className="flex flex-col items-center">
        <aside>
          <span>
            {Math.trunc(max ?? 0)} <sup>o</sup>
          </span>
          /
          <span>
            {Math.trunc(min ?? 0)} <sup>o</sup>
          </span>
        </aside>
        <article className="uppercase">{day}</article>
      </div>
    </main>
  );
};

export default ForecastCard;
