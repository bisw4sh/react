type Temperature = {
  temp: number;
  min: number;
  max: number;
};

const Mid = ({ temp, min, max }: Temperature) => {
  return (
    <aside className="flex flex-col justify-between items-center gap-3">
      <div className="text-black text-6xl">{Math.trunc(temp)}</div>
      <div className="text-xs">
        <span className="px-1">
          {Math.trunc(min)}
          <sup>o</sup>
        </span>
        /
        <span className="px-1">
          {Math.trunc(max)}
          <sup>o</sup>
        </span>
      </div>
    </aside>
  );
};

export default Mid;
