import { FiWind } from "react-icons/fi";
import { BsFillUmbrellaFill } from "react-icons/bs";
import { FiDroplet } from "react-icons/fi";

type WaterFall = {
  wind: number;
  precipitation: number;
  rain: number;
};

const Right = ({ wind, precipitation, rain }: WaterFall) => {
  return (
    <section>
      <div className="flex gap-3 items-center">
        <FiWind />
        <span className="no-wrap">
          <span className="text-lg">{Math.trunc(wind)}</span>
          <span className="pl-1">mph</span>
        </span>
      </div>

      <div className="flex gap-3 items-center">
        <BsFillUmbrellaFill />
        <span className="no-wrap">
          <span className="text-lg">{Math.trunc(precipitation)}</span>
          <span className="pl-1">%</span>
        </span>
      </div>

      <div className="flex gap-3 items-center">
        <FiDroplet />
        <span className="no-wrap">
          <span className="text-lg">{Math.trunc(rain)}</span>{" "}
          <span className="pl-1">%</span>
        </span>
      </div>
    </section>
  );
};

export default Right;
