import Left from "./InfoComp/Left";
import Mid from "./InfoComp/Mid";
import Right from "./InfoComp/Right";

type WeatherData = {
  weather_code?: number;
  max_temp?: number;
  min_temp?: number;
  rain?: number;
  precipitation?: number;
  wind?: number;
};

const InfoSect = ({
  weather_code,
  max_temp,
  min_temp,
  wind,
  precipitation,
  rain,
}: WeatherData) => {
  const avg_temp =
    max_temp !== undefined && min_temp !== undefined
      ? Math.round((max_temp + min_temp) / 2)
      : undefined;
  return (
    <main className="w-full flex justify-between items-center gap-8 py-10 px-5">
      <Left code={weather_code ?? 0} />
      <Mid temp={avg_temp ?? 0} min={min_temp ?? 0} max={max_temp ?? 0} />
      <Right
        wind={wind ?? 0}
        precipitation={precipitation ?? 0}
        rain={rain ?? 0}
      />
    </main>
  );
};

export default InfoSect;
