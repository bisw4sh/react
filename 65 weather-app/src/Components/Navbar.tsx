const Navbar = () => {
  return (
    <section className="w-full bg-[#4D4D4D] p-3 rounded-t-lg">
      <main className=" flex gap-2">
        <div className="w-[0.5rem] h-[0.5rem] rounded-full bg-[#9A9999] " />
        <div className="w-[0.5rem] h-[0.5rem] rounded-full bg-[#9A9999] " />
        <div className="w-[0.5rem] h-[0.5rem] rounded-full bg-[#9A9999] " />
      </main>
    </section>
  );
};

export default Navbar;
