type TempType = "f" | "c";

const Temp = ({
  tempUnit,
  setTempUnit,
}: {
  tempUnit: TempType;
  setTempUnit: (tempUnit: TempType) => void;
}) => {
  return (
    <div className="flex items-center pb-4">
      <button
        className={`border-r-[1px] border-[#0000008d] px-3 ${
          tempUnit === "f" ? "text-black" : null
        }`}
        onClick={() => setTempUnit("f")}
      >
        <sup>o</sup>F
      </button>
      <button
        className={`px-3 ${tempUnit === "c" ? "text-black" : null}`}
        onClick={() => setTempUnit("c")}
      >
        <sup>o</sup>C
      </button>
    </div>
  );
};

export default Temp;
