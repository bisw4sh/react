type WeatherCode = {
  [key: string]: string;
};

export const weather_icon_map: WeatherCode = {
  "0": "0.svg",
  "1": "1.svg",
  "2": "1.svg",
  "3": "1.svg",
  "45": "2.svg",
  "48": "2.svg",
  "51": "3.svg",
  "56": "4.svg",
  "57": "4.svg",
  "61": "5.svg",
  "63": "5.svg",
  "65": "5.svg",
  "66": "6.svg",
  "67": "6.svg",
  "71": "7.svg",
  "73": "7.svg",
  "75": "7.svg",
  "77": "8.svg",
  "80": "9.svg",
  "81": "9.svg",
  "82": "9.svg",
  "85": "10.svg",
  "86": "10.svg",
  "95*": "11.svg",
  "96": "11.svg",
  "99*": "11.svg",
};
