type WeatherCode = {
  [key: string]: string;
};

export const weather_code: WeatherCode = {
  "0": "Clear sky",
  "1": "mainly clear, partly cloudy, and overcast",
  "2": "mainly clear, partly cloudy, and overcast",
  "3": "mainly clear, partly cloudy, and overcast",
  "45": "fog and depositing rime fog",
  "48": "fog and depositing rime fog",
  "51": "drizzle: Light, moderate, and dense intensity",
  "56": "freezing Drizzle: Light and dense intensity",
  "57": "freezing Drizzle: Light and dense intensity",
  "61": "rain: Slight, moderate and heavy intensity",
  "63": "rain: Slight, moderate and heavy intensity",
  "65": "rain: Slight, moderate and heavy intensity",
  "66": "freezing Rain: Light and heavy intensity",
  "67": "freezing Rain: Light and heavy intensity",
  "71": "snow fall: Slight, moderate, and heavy intensity",
  "73": "snow fall: Slight, moderate, and heavy intensity",
  "75": "snow fall: Slight, moderate, and heavy intensity",
  "77": "snow grains",
  "80": "rain showers: Slight, moderate, and violent",
  "81": "rain showers: Slight, moderate, and violent",
  "82": "rain showers: Slight, moderate, and violent",
  "85": "snow showers slight and heavy",
  "86": "snow showers slight and heavy",
  "95*": "thunderstorm: Slight or moderate",
  "96": "thunderstorm with slight and heavy hail",
  "99*": "thunderstorm with slight and heavy hail",
};
