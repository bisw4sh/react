import { useState, ChangeEvent } from "react";
import { weather_code } from "./utility/weather_code";

const InputField = ({
  code,
  location,
  setLocation,
  setCustomGeo,
}: {
  code?: number;
  location: string;
  setLocation: (location: string) => void;
  setCustomGeo: (location: string) => void;
}) => {
  const [width, setWidth] = useState<number>(1);

  const code_str: string = code?.toString() || "";
  const display_string =
    weather_code[code_str] !== undefined ? weather_code[code_str] : "";

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setLocation(value);
    setWidth(value.length + 1);
    const query = value.split(" ").join("%");
    console.log("Query ",query);
    setCustomGeo(
      `https://api.geoapify.com/v1/geocode/search?name=${query}&format=json&apiKey=${
        import.meta.env.VITE_GEOAPIFY_API_KEY
      }`
    );
  };
//https://api.geoapify.com/v1/ipinfo?&apiKey=a3f367e316af4d3a8d1a67f382c9aaa9

  return (
    <div className="pt-[6rem] px-2 text-lg">
      Right now in
      <input
        type="text"
        id="weather-input"
        onChange={handleChange}
        value={location}
        className="mx-2 px-2 focus:outline-none w-auto text-black font-bold focus:border-b"
        size={width}
      />
      , it's {display_string}.
    </div>
  );
};

export default InputField;
