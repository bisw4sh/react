import ForecastCard from "./ForecastCard";

type WeatherData = {
  daily: {
    time: Date[];
    weatherCode: Float32Array;
    temperature2mMax: Float32Array;
    temperature2mMin: Float32Array;
    rainSum: Float32Array;
    precipitationProbabilityMax: Float32Array;
    windSpeed10mMax: Float32Array;
  };
};

const Forcast = ({ weather_data }: { weather_data?: WeatherData }) => {
  const day_names = ["SUN", "MON", "TUES", "WED", "THURS", "FRI", "SAT"];

  const now = new Date();
  const today = now.getDay();

  return (
    <section className="w-full flex justify-between items-center pt-5 pb-12 ">
      <ForecastCard
        min={weather_data?.daily.temperature2mMin[1]}
        max={weather_data?.daily.temperature2mMax[1]}
        code={weather_data?.daily.weatherCode[1] ?? 0}
        day={"TOM"}
      />
      <ForecastCard
        min={weather_data?.daily.temperature2mMin[2]}
        max={weather_data?.daily.temperature2mMax[2]}
        code={weather_data?.daily.weatherCode[2] ?? 0}
        day={day_names[(today + 2) % 7]}
      />
      <ForecastCard
        min={weather_data?.daily.temperature2mMin[3]}
        max={weather_data?.daily.temperature2mMax[3]}
        code={weather_data?.daily.weatherCode[3] ?? 0}
        day={day_names[(today + 3) % 7]}
      />
      <ForecastCard
        min={weather_data?.daily.temperature2mMin[4]}
        max={weather_data?.daily.temperature2mMax[4]}
        code={weather_data?.daily.weatherCode[4] ?? 0}
        day={day_names[(today + 4) % 7]}
      />
    </section>
  );
};

export default Forcast;
