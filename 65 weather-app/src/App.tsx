import Navbar from "./Components/Navbar";
import InputField from "./Components/InputField";
import InfoSect from "./Components/InfoSect";
import Forecast from "./Components/Forecast";
import Temp from "./Components/Temp";
import { fetchWeatherApi } from "openmeteo";
import { useState, useEffect } from "react";

type WeatherData = {
  daily: {
    time: Date[];
    weatherCode: Float32Array;
    temperature2mMax: Float32Array;
    temperature2mMin: Float32Array;
    rainSum: Float32Array;
    precipitationProbabilityMax: Float32Array;
    windSpeed10mMax: Float32Array;
  };
};
type TempType = "f" | "c";

export default function App() {
  const [wData, setWData] = useState<WeatherData | undefined>(undefined);
  const [tempUnit, setTempUnit] = useState<TempType>("c");
  const [location, setLocation] = useState<string>("");
  const [customGeo, setCustomGeo] = useState<string>(
    `https://api.geoapify.com/v1/geocode/search?name=pokhara&format=json&apiKey=${
      import.meta.env.VITE_GEOAPIFY_API_KEY
    }`
  );

  useEffect(() => {
    (async () => {
      const GeoToFetchURL =
        location.length === 0
          ? `https://api.geoapify.com/v1/ipinfo?&apiKey=${
              import.meta.env.VITE_GEOAPIFY_API_KEY
            }`
          : customGeo;

      console.log("GeoToFetchURL", GeoToFetchURL);
      const geo_data = await fetch(GeoToFetchURL);

      if (!geo_data.ok) throw new Error("Geo data couldn't be fetched");
      const geo_data_obj = await geo_data.json();

      const [latitude, longitude] = [
        location.length === 0
          ? geo_data_obj.location.latitude
          : geo_data_obj?.results[0]?.lat,
        location.length === 0
          ? geo_data_obj.location.longitude
          : geo_data_obj?.results[0]?.lon,
      ];

      console.log("geodata : ", geo_data_obj);
      console.log(latitude, longitude);

      const Initialparams = {
        latitude: latitude,
        longitude: longitude,
        daily: [
          "weather_code",
          "temperature_2m_max",
          "temperature_2m_min",
          "rain_sum",
          "precipitation_probability_max",
          "wind_speed_10m_max",
        ],
        wind_speed_unit: "mph",
        timeformat: "unixtime",
        timezone: "auto",
        forecast_days: 5,
      };

      const params =
        tempUnit === "f"
          ? { ...Initialparams, temperature_unit: "fahrenheit" }
          : { ...Initialparams };
      const url = import.meta.env.VITE_WEATHER_API_URL;
      const responses = await fetchWeatherApi(url, params);

      // Helper function to form time ranges
      const range = (start: number, stop: number, step: number) =>
        Array.from(
          { length: (stop - start) / step },
          (_, i) => start + i * step
        );

      // Process first location. Add a for-loop for multiple locations or weather models
      const response = responses[0];

      // Attributes for timezone and location
      const utcOffsetSeconds = response.utcOffsetSeconds();

      const daily = response.daily()!;

      // Note: The order of weather variables in the URL query and the indices below need to match!
      const weatherData = {
        daily: {
          time: range(
            Number(daily.time()),
            Number(daily.timeEnd()),
            daily.interval()
          ).map((t) => new Date((t + utcOffsetSeconds) * 1000)),
          weatherCode: daily.variables(0)!.valuesArray()!,
          temperature2mMax: daily.variables(0)!.valuesArray()!,
          temperature2mMin: daily.variables(1)!.valuesArray()!,
          rainSum: daily.variables(2)!.valuesArray()!,
          precipitationProbabilityMax: daily.variables(3)!.valuesArray()!,
          windSpeed10mMax: daily.variables(4)!.valuesArray()!,
        },
      };

      setWData(weatherData);
    })();
  }, [customGeo, location.length, tempUnit]);

  return (
    <>
      <main className="min-h-screen w-full p-3 bg-[#4DBFD9] flex justify-center items-center text-gray-400">
        <div className="h-full w-full md:h-2/3 md:w-2/3 lg:h-1/2 lg:w-1/2 bg-[#FAFAFA] rounded-md flex flex-col justify-start items-center rounded-t-lg">
          <Navbar />
          <InputField
            code={wData?.daily.weatherCode[0]}
            location={location}
            setLocation={setLocation}
            setCustomGeo={setCustomGeo}
          />
          <InfoSect
            weather_code={wData?.daily.weatherCode[0]}
            max_temp={wData?.daily.temperature2mMax[0]}
            min_temp={wData?.daily.temperature2mMin[0]}
            wind={wData?.daily.windSpeed10mMax[0]}
            precipitation={wData?.daily.precipitationProbabilityMax[0]}
            rain={wData?.daily.rainSum[0]}
          />
          <Forecast weather_data={wData} />
          <Temp tempUnit={tempUnit} setTempUnit={setTempUnit} />
        </div>
      </main>
    </>
  );
}
