import { useQuery, keepPreviousData } from "@tanstack/react-query";
import { useState } from "react";
import fetchPage from "../utility/fetchPage";

const MainPage = () => {
  const [pgNum, setPgNum] = useState<number>(0);

  const handlePrevClick = () => {
    setPgNum((prev) => (prev > 0 ? prev - 1 : 0));
    if (!isFetching) fetchPage(pgNum);
  };

  const handleNextClick = () => {
    setPgNum((prev) => (prev < 99 ? prev + 1 : 99));
    if (!isFetching) fetchPage(pgNum);
  };

  const { status, error, data, isFetching } = useQuery({
    queryKey: ["infos", pgNum],
    queryFn: () => fetchPage(pgNum),
    placeholderData: keepPreviousData,
  });

  return status === "pending" ? (
    <div>Loading...</div>
  ) : status === "error" ? (
    <div>{error?.message}</div>
  ) : (
    <div className="min-h-screen w-full flex flex-col justify-center items-center gap-2">
      <div className="overflow-x-auto">
        <table className="table">
          {/* head */}
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Gender</th>
              <th>IP Address</th>
            </tr>
          </thead>
          <tbody>
            {/* Table Body */}
            {data?.map((details) => (
              <tr key={details.id}>
                <td>{details.id}</td>
                <td>{details.first_name}</td>
                <td>{details.last_name}</td>
                <td>{details.email}</td>
                <td>{details.gender}</td>
                <td>{details.ip_address}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Pagination */}
      <div className="join grid grid-cols-3">
        <button className="join-item btn btn-outline" onClick={handlePrevClick}>
          Previous page
        </button>
        <button className="join-item btn btn-disabled">Page {pgNum + 1}</button>
        <button className="join-item btn btn-outline" onClick={handleNextClick}>
          Next
        </button>
      </div>
    </div>
  );
};

export default MainPage;
