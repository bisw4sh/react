export default function HomePage() {
  return (
    <div className="min-h-screen flex justify-center items-center text-xl font-medium">
      Go to the Main from Navbar to see the work
    </div>
  );
}
