interface MOCK_DATA {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  ip_address: string;
}

const fetchPage = async (pgNum: number): Promise<MOCK_DATA[]> => {
  const res = await fetch("/api", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ currentPage: pgNum }),
  });

  const collectedData: MOCK_DATA[] = await res.json();
  return collectedData;
};

export default fetchPage;
