import { useState, useEffect } from 'react';
import UserComp from './userComp.jsx'

const App = () => {
  const [userDetails, setUserDetails] = useState({});
  const [username, setUsername] = useState('bisw4sh');

  const fetchData = async () => {
    try {
      const userData = await fetch(`https://api.github.com/users/${username}`);
      const userJSON = await userData.json();
      setUserDetails(userJSON);
      console.log(userJSON);
    } catch (error) {
      console.error(error);
    }
  }
  useEffect(() => {
    fetchData();
  }, [username]);

  const handleChange = (e) => {
    setUsername(e.target.value);
  };

  return (
    <div className='wrapper'>
      <form>
        {/* <input type="text" className='username' onChange={handleChange} value={username} /> */}

        <div className="form__group field">
          <input type="input" className="username form__field" placeholder="Name" name="name" id='name' required onChange={handleChange} value={username}/>
          <label htmlFor="name" className="form__label">USERNAME</label>
        </div>
      </form>

      {Object.keys(userDetails).length > 0 ? (
        <UserComp userDetails={userDetails} />
      ) : (
        <div>Loading</div>
      )}
    </div>
  );
};

export default App;
