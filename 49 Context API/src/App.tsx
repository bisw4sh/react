import { useContext, useState, FormEvent } from "react";
import { ThemeContext, ThemeContextType } from "./context/ThemeContext";
import { userContext, AuthContextType } from "./context/AuthContext";

export default function App() {
  const { theme, setTheme } = useContext(ThemeContext) as ThemeContextType;
  const { user, setUser } = useContext(userContext) as AuthContextType;

  const [bglight, bgdark] = ["bg-slate-900", "bg-slate-400"];
  const [userID, setUserID] = useState<string>("");

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setUser(userID);
    setUserID("");
  };

  return (
    <div
      className={`h-screen flex flex-col gap-2  justify-center items-center 
      ${theme === "light" ? bgdark : bglight}
      ${theme === "light" ? "text-dark" : "text-white"}
      `}
    >
      {user} <br />
      <span className="uppercase">{theme}</span>
      <button
        className={`bg-teal-500 px-4 py-2 rounded-full h-3 w-3`}
        onClick={() => setTheme(theme === "light" ? "dark" : "light")}
      ></button>
      <form action="" onSubmit={handleSubmit} className="flex flex-col gap-2 ">
        <input
          type="text"
          placeholder="Enter userID "
          value={userID}
          onChange={(e) => {
            setUserID(e.target.value);
          }}
          className="text-rose-500 p-1 rounded-md"
        />
        <button type="submit" className={`bg-teal-500 px-4 py-2 rounded-md`}>
          Submit{" "}
        </button>
      </form>
    </div>
  );
}
