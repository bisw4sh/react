import { useEffect, useState } from "react";
import MainContainer from "./comp/MainContainer.jsx";
import Record from "./comp/Record.jsx";

const App = () => {
  const [rData, setRData] = useState([]); // Initialize rData as an empty array
  const [count, setCount] = useState(0);

  useEffect(() => {
    const savedItem = localStorage.getItem("records");
    const parsedItem = JSON.parse(savedItem);
    if (parsedItem) {
      setRData(parsedItem); // Update rData with the data from local storage
    }
  }, []);

  return (
    <>
      <MainContainer rData={rData} setRData={setRData} count={count} setCount={setCount} />
      <Record rData={rData} />
    </>
  );
}

export default App;