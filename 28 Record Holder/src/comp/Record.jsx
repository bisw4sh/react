const Record = ({ rData }) => {
  return (
    <div className="record">
      {rData.map((datum) => (
        <ul key={datum.id}>
          <li>{datum.name}</li>
          <li>{datum.email}</li>
        </ul>
      ))}
    </div>
  );
}

export default Record;