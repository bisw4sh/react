import { useState } from "react";

const MainContainer = ({ rData, setRData, count, setCount }) => {
  const [temp, setTemp] = useState({ name: "", email: "" });

  const handleClick = (e) => {
    e.preventDefault();
    setCount(count + 1);
    const newRecord = { ...temp, id: count };
    setRData([...rData, newRecord]); // Update rData correctly
    setTemp({ name: "", email: "", id: count }); // Reset temp after adding to rData

    // Save the updated rData to local storage
    localStorage.setItem("records", JSON.stringify([...rData, newRecord]));
  }

  return (
    <div className="MainContainer">
      <form action="" onSubmit={handleClick}>
        <label htmlFor="name">Name:
          <input type="text" id="name" name="name" value={temp.name} onChange={e => setTemp({ ...temp, name: e.target.value })} required />
        </label>

        <label htmlFor="email">Email:
          <input type="email" id="email" name="email" value={temp.email} onChange={e => setTemp({ ...temp, email: e.target.value })} required />
        </label>

        <button type="submit">Add Person</button>
      </form>
    </div>
  );
}

export default MainContainer;