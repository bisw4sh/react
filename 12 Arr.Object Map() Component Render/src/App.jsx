import React from 'react';

// Sample array filled with objects
const data = [
  { id: 1, name: 'John', age: 30 },
  { id: 2, name: 'Alice', age: 25 },
  { id: 3, name: 'Bob', age: 35 },
];

// Create a functional component that receives data as props
const DataList = ({ dataList }) => {
  return (
    <div>
      <h2>Data List:</h2>
      <ul>
        {dataList.map((item) => (
          <li key={item.id}>
            {`${item.name} is ${item.age} years old.`}
          </li>
        ))}
      </ul>
    </div>
  );
};

// Main App component
function App() {
  return (
    <div className="App">
      <DataList dataList={data} />
    </div>
  );
}

export default App;