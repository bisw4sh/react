import { useForm, SubmitHandler } from "react-hook-form";
import { animals } from "../Utility/data";
import { Input, Button, Select, SelectItem } from "@nextui-org/react";

interface Inputs {
  name: string;
  city: string;
  skills: {
    html: boolean;
    css: boolean;
    js: boolean;
    react: boolean;
    node: boolean;
    next: boolean;
  };
  animals: string;
}

const Form3 = () => {
  const {
    register,
    handleSubmit,
    // control,
    reset,
    // watch,
    // formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    const respData = await fetch("/api/form3", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    if (!respData.ok) {
      throw new Error("Network response was not ok");
    }
``
    console.log(respData);
    console.log(await respData.json());
  };

  return (
    <div className="min-h-screen w-full bg-black p-3 flex justify-center items-center text-zinc-900">
      <form
        method="POST"
        onSubmit={handleSubmit(onSubmit)}
        className="w-1/2 flex flex-col justify-start items-center gap-5 bg-slate-200 p-12 rounded-2xl"
      >
        <h1>Form 3</h1>
        <Input
          type="text"
          variant="underlined"
          label="Name"
          placeholder="Enter your name"
          {...register("name", { required: true })}
          defaultValue=""
        />

        {/* input button, rounded - need to have a single value returned that is selected */}
        <main className="flex flex-col gap-3">
          <label htmlFor="kathmandu">
            <input
              type="radio"
              // name="city"
              value="kathmandu"
              {...register("city")}
              id="kathmandu"
            />
            <span> Kathmandu</span>
          </label>

          <label htmlFor="bharatpur">
            <input
              type="radio"
              // name="city"
              value="bharatpur"
              id="bharatpur"
              {...register("city")}
            />
            <span> Bharatpur</span>
          </label>

          <label htmlFor="pokhara">
            <input
              type="radio"
              // name="city"
              value="pokhara"
              id="pokhara"
              {...register("city")}
            />
            <span> Pokhara</span>
          </label>

          <label>
            <input
              type="radio"
              // name="city"
              value="dharan"
              id="dharan"
              {...register("city")}
            />
            <span> Dharan</span>
          </label>

          <label htmlFor="biratnagar">
            <input
              type="radio"
              // name="city"
              value="biratnagar"
              id="biratnagar"
              {...register("city")}
            />
            <span> Biratnagar</span>
          </label>

          <label htmlFor="mahendranagar">
            <input
              type="radio"
              // name="city"
              value="mahendranagar"
              id="mahendranagar"
              {...register("city")}
            />
            <span> Mahendranagar</span>
          </label>
        </main>

        {/* Checkbox - select multiple options - return an array of selected fields */}
        <div className="flex gap-4">
          <label htmlFor="html">
            <input
              type="checkbox"
              value="html"
              id="html"
              {...register("skills.html")}
            />
            HTML
          </label>

          <label htmlFor="css">
            <input
              type="checkbox"
              value="css"
              id="css"
              {...register("skills.css")}
            />
            CSS
          </label>

          <label htmlFor="js">
            <input
              type="checkbox"
              value="js"
              id="js"
              {...register("skills.js")}
            />
            JS
          </label>

          <label htmlFor="react">
            <input
              type="checkbox"
              value="react"
              id="react"
              {...register("skills.react")}
            />
            React
          </label>

          <label htmlFor="node">
            <input
              type="checkbox"
              value="node"
              id="node"
              {...register("skills.node")}
            />
            Node
          </label>

          <label htmlFor="next">
            <input
              type="checkbox"
              value="next"
              id="next"
              {...register("skills.next")}
            />
            Next
          </label>
        </div>

        {/* Selecting options */}
        <Select
          items={animals}
          label="Favorite Animal"
          placeholder="Select an animal"
          className="max-w-xs"
          {...register("animals")}
        >
          {(animal) => (
            <SelectItem key={animal.value} value={animal.value}>
              {animal.label}
            </SelectItem>
          )}
        </Select>
        <Button onPress={() => reset({
          name: "name hanna name",
          city: "pokhara"
        })}>Reset</Button>
        {/* submission button */}
        <Button type="submit" variant="shadow" color="secondary" spinner="o">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default Form3;
