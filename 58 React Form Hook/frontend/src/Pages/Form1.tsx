import { Input } from "@nextui-org/react";
import { Button } from "@nextui-org/react";
import { useForm, SubmitHandler } from "react-hook-form";

interface Inputs {
  name: string;
  password: string;
  age: number;
  email: string;
  phone: string;
  hobbies: string;
}

interface ResponsePayload {
  message: string;
  data: Inputs;
}

const Form1 = () => {
  const {
    register,
    handleSubmit,
    // watch,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = async (dataClient) => {
    try {
      const res = await fetch("/api/form1", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataClient),
      });

      if (!res.ok) {
        throw new Error("Network response was not ok");
      }

      const resPayload: ResponsePayload = await res.json();
      const { message, data } = resPayload;

      console.log("message:", message);
      console.log("------------------------------------");
      console.log("Data:", data);
    } catch (error) {
      console.error("Error occurred:", error);
    }
  };

  return (
    <div className="min-h-screen w-full bg-black p-3 flex justify-center items-center text-zinc-50">
      <form
        method="POST"
        onSubmit={handleSubmit(onSubmit)}
        className="w-1/2 flex flex-col justify-start items-center gap-3 bg-slate-900 p-12 rounded-2xl"
      >
        <h1>Form 1</h1>
        <Input
          type="text"
          variant="underlined"
          label="Name"
          placeholder="Enter your name"
          {...register("name", { required: true })}
          defaultValue=""
        />

        {errors.name && <span>This name field is required</span>}
        <Input
          type="password"
          variant="underlined"
          label="Password"
          placeholder="Enter your password"
          color="secondary"
          {...register("password", { required: true })}
          defaultValue=""
        />

        <Input
          type="number"
          variant="underlined"
          label="Age"
          placeholder="Enter your age"
          {...register("age")}
          className="text-rose-500"
          defaultValue=""
        />

        <Input
          type="email"
          variant="underlined"
          label="Email"
          placeholder="Enter your Email"
          {...register("email")}
          defaultValue=""
        />

        <Input
          type="number"
          variant="bordered"
          label="Phone Number"
          placeholder="Enter your phone number"
          color="danger"
          isClearable={true}
          {...register("phone", { minLength: 9, min: 18 })}
          defaultValue=""
        />

        <Input
          type="hobbies"
          variant="flat"
          label="Hobbies"
          placeholder="Enter your hobbies"
          labelPlacement="inside"
          color="success"
          {...register("hobbies")}
          defaultValue=""
        />

        <Button type="submit" variant="shadow" color="secondary" spinner="o">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default Form1;
