import { useForm, SubmitHandler, Controller } from "react-hook-form";
import {
  Button,
  Input,
  RadioGroup,
  Radio,
  Checkbox,
  Select,
  SelectItem,
} from "@nextui-org/react";
import { animals } from "../Utility/data";

interface Inputs {
  name: string;
  city: string;
  skills: {
    html: boolean;
    css: boolean;
    js: boolean;
    react: boolean;
    node: boolean;
    next: boolean;
  };
  animals: string;
}

const Form1 = () => {
  const { register, handleSubmit, control } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      const respData = await fetch("/api/form2", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      console.log(await respData.json());
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="min-h-screen w-full bg-black p-3 flex justify-center items-center text-zinc-50">
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="w-1/2 flex flex-col justify-start items-center gap-3 bg-slate-200 p-12 rounded-2xl"
      >
        <h1>Form 2</h1>
        <Input
          type="text"
          variant="underlined"
          label="Name"
          placeholder="Enter your name"
          {...register("name", { required: true })}
          defaultValue=""
        />

        <Controller
          control={control}
          name="city"
          defaultValue=""
          render={({ field }) => (
            <RadioGroup
              label="Select your city"
              orientation="horizontal"
              onChange={(value) => field.onChange(value)}
              value={field.value}
              defaultValue="nepal"
            >
              <Radio value="kathmandu" color="danger">
                Kathmandu
              </Radio>
              <Radio value="bharatpur" color="default">
                Bharatpur
              </Radio>
              <Radio value="pokhara" color="warning">
                Pokhara
              </Radio>
              <Radio value="dharan" color="primary">
                Dharan
              </Radio>
              <Radio value="biratnagar" color="secondary">
                Biratnagar
              </Radio>
              <Radio value="mahendranagar" color="success">
                Mahendranagar
              </Radio>
            </RadioGroup>
          )}
        />

        <h1 className="text-lg text-zinc-400 pt-4 self-start">
          Select your skills
        </h1>

        <Controller
          control={control}
          name="skills"
          defaultValue={{
            html: false,
            css: false,
            js: false,
            react: false,
            node: false,
            next: false,
          }}
          render={({ field }) => (
            <div className="flex gap-4">
              <Checkbox
                color="default"
                value="html"
                checked={field.value.html}
                onChange={(e) =>
                  field.onChange({ ...field.value, html: e.target.checked })
                }
              >
                html
              </Checkbox>
              <Checkbox
                color="danger"
                value="css"
                checked={field.value.css}
                onChange={(e) =>
                  field.onChange({ ...field.value, css: e.target.checked })
                }
              >
                css
              </Checkbox>
              <Checkbox
                color="warning"
                value="js"
                checked={field.value.js}
                onChange={(e) =>
                  field.onChange({ ...field.value, js: e.target.checked })
                }
              >
                js
              </Checkbox>
              <Checkbox
                color="secondary"
                value="react"
                checked={field.value.react}
                onChange={(e) =>
                  field.onChange({ ...field.value, react: e.target.checked })
                }
              >
                react
              </Checkbox>
              <Checkbox
                color="success"
                value="node"
                checked={field.value.node}
                onChange={(e) =>
                  field.onChange({ ...field.value, node: e.target.checked })
                }
              >
                node
              </Checkbox>
              <Checkbox
                color="default"
                value="next"
                checked={field.value.next}
                onChange={(e) =>
                  field.onChange({ ...field.value, next: e.target.checked })
                }
              >
                next
              </Checkbox>
            </div>
          )}
        />

        <Select
          items={animals}
          label="Favorite Animal"
          placeholder="Select an animal"
          className="max-w-xs"
          {...register("animals")}
        >
          {(animal) => (
            <SelectItem key={animal.value} value={animal.value}>
              {animal.label}
            </SelectItem>
          )}
        </Select>

        <Button type="submit" variant="shadow" color="secondary" spinner="o">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default Form1;
