import { Outlet, NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="w-full">
      <div className="w-full bg-black text-white p-3">
        <div>React Form Hook</div>
        <div className="flex items-center gap-3 decoration-wavy	underline-offset-[5px]">
          <NavLink to="/" className="hover:underline">
            Form 1
          </NavLink>
          <NavLink to="/form2" className="hover:underline hover:text-rose-500">
            Form 2
          </NavLink>
          <NavLink to="/form3" className="hover:underline">
            Form 3
          </NavLink>
        </div>
      </div>
      <Outlet />
    </div>
  );
};

export default Navbar;
