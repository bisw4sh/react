import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import Navbar from "./Components/Navbar";
import Form1 from "./Pages/Form1";
import Form2 from "./Pages/Form2";
import Form3 from "./Pages/Form3";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Navbar />}>
      <Route index element={<Form1 />} />
      <Route path="form2" element={<Form2 />} />
      <Route path="form3" element={<Form3 />} />
    </Route>
  )
);

export default function App() {
  return <RouterProvider router={router} />;
}
