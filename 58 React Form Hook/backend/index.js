import express from "express";

// Create Express App
const app = express();
const PORT = 3000;

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Example Route
app.get("/", (req, res) => {
  res.send("Express server is now running");
});

app.post("/api/form1", (req, res) => {
  const { ...data } = req.body;
  console.log(data);
  res.json({ message: "success", data: { ...data } });
});

app.post("/api/form1", (req, res) => {
  const { ...data } = req.body;
  console.log(data);
  res.json({ message: "success", data: { ...data } });
});

app.post("/api/form2", (req, res) => {
  const { ...data } = req.body;
  console.log(data);
  res.json({ message: "success", data: { ...data } });
});

// Start Server
app.listen(PORT, () => {
  console.log("Server is running on http://localhost:" + PORT);
});
