import { useQuery } from "@tanstack/react-query";
import { Button } from "@/components/ui/button";
import { HardDriveDownload } from "lucide-react";
import { toast } from "sonner";
import { useState } from "react";
import AlertComp from "../comp/AlertComp";
import { SkeletonDemo } from "../comp/SkeletonDemo";

export default function Homepage() {
  const [count, setCount] = useState<number>(0);

  const { isPending, isError, data, error } = useQuery({
    queryKey: ["single-data"],
    queryFn: async () => {
      const fetchedData = await fetch(
        "https://jsonplaceholder.typicode.com/todos/1"
      );
      const data = await fetchedData.json();
      const { completed, id, title, userId } = data;
      return { completed, id, title, userId };
    },
  });

  const handleClicks = async () => {
    setCount((prevCount) => prevCount + 1);
    toast(`This is the ${count + 1} click`);
    console.log({ isPending, isError, data, error });
    toast(`${data?.completed}
    ${data?.id}
    ${data?.title}
    ${data?.userId}
    `);
  };

  const handleRefetch = async () => {};

  return (
    <div className="p-1 flex flex-col justify-center items-center gap-4">
      <Button
        variant="ghost"
        onClick={handleClicks}
        className="flex justify-center item-center gap-3"
      >
        Single Post
        <HardDriveDownload />
      </Button>
      <Button
        variant="ghost"
        onClick={handleRefetch}
        className="flex justify-center item-center gap-3"
      >
        Refetch
        <HardDriveDownload />
      </Button>
      <article className="w-1/3 space-y-3">
        <AlertComp header={data?.userId} desc={data?.title} />
        <AlertComp />
      </article>
      <SkeletonDemo />
    </div>
  );
}
