import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { Input } from "@/components/ui/input";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { Skeleton } from "@/components/ui/skeleton";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";

type UsersType = {
  id: string;
  user: string;
  password: string;
};

export default function Formpage() {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const queryClient = useQueryClient();
  const query = useQuery({
    queryKey: ["users-data"],
    queryFn: async () => {
      const response = await fetch("/api/getUsers");
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }
      return response.json();
    },
  });

  const mutation = useMutation({
    mutationFn: async (newData: UsersType) => {
      const mutResp = await fetch("/api/signin", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newData),
      });
      console.log("mutResp");
      console.log(mutResp);
    },
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ["users-data"] });
    },
  });

  const deletion = useMutation({
    mutationFn: async (id: string) => {
      const mutResp = await fetch("/api/deletion", {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json", // Change Content-Type to "application/json"
        },
        body: JSON.stringify({ id }), // Convert id to JSON and send it in the body
      });

      console.log("mutResp");
      console.log(mutResp);
    },
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ["users-data"] });
    },
  });

  return (
    <div className="mt-8 flex flex-col justify-center items-center space-y-4">
      <Card className="lg:w-1/3 min-h-1/2">
        <CardHeader>
          <CardTitle className="text-center">Sign In Form</CardTitle>
        </CardHeader>
        <CardContent>
          <form
            className="space-y-4 flex flex-col justify-center items-center"
            method="POST"
            onSubmit={(e) => {
              e.preventDefault();
              mutation.mutate({ user: user, password: password, id: uuidv4() });
              setUser("");
              setPassword("");
            }}
          >
            <Input
              name="user"
              type="text"
              placeholder="user"
              value={user}
              onChange={(e) => setUser(e.target.value)}
            />
            <Input
              name="password"
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button variant="outline" type="submit">
              Submit
            </Button>
          </form>
        </CardContent>
        <CardFooter className="text-center">
          <p className="w-full text-sm text-center">Fill both</p>
        </CardFooter>
      </Card>

      <Card className="lg:w-1/3 min-h-1/2">
        <CardHeader>
          <CardTitle className="text-center">Details</CardTitle>
        </CardHeader>
        <CardContent className="space-y-3">
          {query.data ? (
            query.data.map(({ user, password, id }: UsersType) => (
              <div
                className="flex flex-col border border-slate-700 p-2 rounded-md"
                key={id}
              >
                <span className="flex justify-between items-center gap-3">
                  User:
                  {query.isPending ? (
                    <Skeleton className="h-4 w-[250px]" />
                  ) : (
                    <span>{user}</span>
                  )}
                </span>
                <span className="flex justify-between items-center gap-3">
                  Password:
                  {query.isPending ? (
                    <Skeleton className="h-4 w-[250px]" />
                  ) : (
                    <span>{password}</span>
                  )}
                </span>
                <Button
                  variant="destructive"
                  className="mt-2"
                  onClick={() => {
                    console.log(id);
                    deletion.mutate(id);
                  }}
                >
                  Delete
                </Button>
              </div>
            ))
          ) : (
            <p>Loading...</p>
          )}
        </CardContent>
      </Card>
    </div>
  );
}
