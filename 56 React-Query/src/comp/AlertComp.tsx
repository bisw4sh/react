import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";

export default function AlertComp({
  header,
  desc,
}: {
  header?: string;
  desc?: string;
}) {
  return (
    <>
      <Alert>
        <AlertTitle>{header || "It was empty"}</AlertTitle>
        <AlertDescription>{desc || "It was empty"}</AlertDescription>
      </Alert>
    </>
  );
}
