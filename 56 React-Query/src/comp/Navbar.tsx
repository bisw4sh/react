import { Link, Outlet, NavLink } from "react-router-dom";
import { ModeToggle } from "@/components/mode-toggle";
import { Home } from "lucide-react";
import { Toaster } from "@/components/ui/sonner";

export default function Navbar() {
  return (
    <div className="min-h-screen dark:bg-neutral-950 dark:text-slate-50 p-3 ">
      <div className="w-full  flex justify-between items-center p-1 rounded-md shadow-md shadow-gray-900">
        <Link to="/">
          <Home className="cursor-pointer hover:stroke-slate-500" />
        </Link>
        <nav className="text-lg font-semibold">
          <NavLink
            to="formpage"
            className="hover:underline decoration-blue-600 decoration-wavy	decoration-2	underline-offset-8"
            style={({ isActive }) => ({
              display: isActive ? "none" : "block",
            })}
          >
            FormPage
          </NavLink>
        </nav>
        <ModeToggle />
      </div>
      <Toaster />
      <Outlet />
    </div>
  );
}
