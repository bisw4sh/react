import express from "express";
const app = express();
let users = [
  {
    id: "1",
    user: "Biswash",
    password: "dhungana-123",
  },
  {
    id: "2",
    user: "Saroj",
    password: "123-dhungana",
  },
];

app.use(express.json());
app.post("/api/signin", (req, res) => {
  users.push(req.body);
  console.log(users);
  res.json(users);
});

app.get("/api/getUsers", async (req, res) => {
  res.json(users);
});

app.delete("/api/deletion", async (req, res) => {
  console.log(req.body);
  const delID = req.body.id;
  users = users.filter(({ id }) => delID !== id);
  console.log(users);
  res.json(users);
});

app.listen(8080, () => console.log("server running @ http://localhost:8080"));
