import Navbar from './Navbar.jsx'

const App = () => {

  return (
  <div className="h-screen bg-slate-800">
    <Navbar/>
  </div>
  )
}

export default App