import { MdOutlineHome } from "react-icons/md";
import { IoColorWandSharp } from "react-icons/io5";
import { SiGooglescholar, SiNounproject } from "react-icons/si";
import { LiaBloggerB } from "react-icons/lia";
import { BiSolidPhoneIncoming } from "react-icons/bi";

const Hamburg = () => {
  return (
    <ul className="sm:hidden absolute right-0 top-16 bg-sky-950 h-full w-max flex flex-col justify-start items-start gap-8 pr-16 pl-8 font-mono text-2xl font-bold text-gray-400">
            
    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <MdOutlineHome className="text-xl"/>
        Home
    </li>

    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <SiGooglescholar className="text-lg"/>
        Career
    </li>

    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <IoColorWandSharp className="text-lg"/>
        Skills
    </li>

    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <SiNounproject className="text-lg"/>
        Projects
    </li>

    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <LiaBloggerB className="text-lg"/>
        Blogs
    </li>

    <li className="flex gap-4 items-center active:underline decoration-gray-400 decoration-2 underline-offset-8">
        <BiSolidPhoneIncoming className="text-lg"/>
        Contact
    </li>

</ul>
  )
}

export default Hamburg