import { useState } from 'react';
import { MdOutlineHome } from "react-icons/md";
import { IoColorWandSharp } from "react-icons/io5";
import { SiGooglescholar, SiNounproject } from "react-icons/si";
import { LiaBloggerB } from "react-icons/lia";
import { BiSolidPhoneIncoming } from "react-icons/bi";
import { GiHamburgerMenu } from "react-icons/gi";
import { RxCross2 } from "react-icons/rx";
import Hamburg from './Hamburg.jsx'

const Navbar = () => {
    const [toggle, setToggle] = useState(false)

  return (
    <>
    <div className="w-full fixed bg-sky-950 flex justify-between items-center px-8 py-4 font-mono text-gray-400">

        <ul>
            <li>
                <a href="#" className=" font-bold text-2xl">Biswash</a>
            </li>
        </ul>

        <ul className="w-full flex justify-end gap-8 font-bold">

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <MdOutlineHome className="text-xl max-sm:hidden"/>
                <a href="#home" className="max-md:hidden">Home</a>
            </li>

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <SiGooglescholar className="text-lg max-sm:hidden"/>
                <a href="#career" className="max-md:hidden">Career</a>
            </li>

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <IoColorWandSharp className="text-lg max-sm:hidden"/>
                <a href="#skills" className="max-md:hidden">Skills</a>
            </li>

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <SiNounproject className="text-lg max-sm:hidden"/>
                <a href="#projects" className="max-md:hidden">Projects</a>
            </li>

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <LiaBloggerB className="text-lg max-sm:hidden"/>
                <a href="#blogs" className="max-md:hidden">Blogs</a>
            </li>

            <li className="flex gap-1 items-center hover:underline decoration-gray-400 decoration-2 underline-offset-8">
                <BiSolidPhoneIncoming className="text-lg max-sm:hidden"/>
                <a href="#contact" className="max-md:hidden">Contact</a>
            </li>
        </ul>

        <div>
            {toggle ? <RxCross2 className="active:fill-sky-950 font-bold text-2xl sm:hidden" onClick={() => setToggle(!toggle)}/> : <GiHamburgerMenu  className="active:fill-sky-950 font-bold text-2xl sm:hidden" onClick={() => setToggle(!toggle)}/>}
        </div>

    </div>

        {/* Hamburg menu logic for small devices */}
        {toggle?(<Hamburg />): (<div>Loading</div>)}


        <div id="home" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Home</div>
        <div id="career" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Career</div>
        <div id="skills" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Skills</div>
        <div id="projects" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Projects</div>
        <div id="blogs" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Blogs</div>
        <div id="contact" className='h-screen flex justify-center items-center text-4xl font-bold bg-sky-900 text-gray-400'>Contact</div>

        </>
  )
}

export default Navbar