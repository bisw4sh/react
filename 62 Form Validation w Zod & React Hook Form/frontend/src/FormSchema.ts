import { z } from "zod";

export const FormSchema = z
  .object({
    email: z
      .string()
      .email({ message: "Enter a valid mail" })
      .trim()
      .toLowerCase(),
    username: z
      .string()
      .trim()
      .toLowerCase()
      .min(4, { message: "Username must be 4 or more characters long" }),
    password: z
      .string()
      .min(8, { message: "Make the password length min of 8" }),
    confirmPassword: z
      .string()
      .min(8),
  })
  .refine((data) => data.confirmPassword === data.password, {
    path: ["confirmPassword"],
    message: "Password don't match",
  });

export type ToFormSchema = z.infer<typeof FormSchema>;
