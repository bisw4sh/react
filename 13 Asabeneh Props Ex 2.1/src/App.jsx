import htmlImg from './assets/HTML.png'
import cssImg from './assets/CSS.png'
import jsImg from './assets/JS.png'
import reactImg from './assets/REACT.png'
import "./App.css"

const CompWrapper = ({imgSrc}) => {
  return (
    <img src={imgSrc} alt="Image of Langs." className='imageHolder'/>
  )
}

function App() {
  return (
    <>
    <h2>Front End Technologies</h2>
    <div className="Container">
   <CompWrapper imgSrc={htmlImg}/>
   <CompWrapper imgSrc={cssImg}/>
   <CompWrapper imgSrc={jsImg}/>
   <CompWrapper imgSrc={reactImg}/>
    </div>
    </>
  )
}

export default App