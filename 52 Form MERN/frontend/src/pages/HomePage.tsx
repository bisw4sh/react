import { useState } from "react";
import FormComp from "../components/FormComp";
import ThemeToggle from "../components/ThemeToggle";

export default function HomePage() {
  const [mode, setMode] = useState<"light" | "dark">("light");
  return (
    <div data-theme={mode}>
      <ThemeToggle mode={mode} setMode={setMode} />
      <div className="min-h-screen flex justify-center items-center">
        <FormComp  />
      </div>
    </div>
  );
}
