import { useRouteError } from 'react-router-dom'

export default function Error() {
  const err = useRouteError()
  console.log(err?.error?.message)
  return (
    <div className="min-h-screen bg-teal-100 flex justify-center items-center text-black text-4xl font-bold">
      Error {err?.error?.message}
    </div>
  );
}
