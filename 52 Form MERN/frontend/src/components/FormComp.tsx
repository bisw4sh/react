import { Form, useActionData, redirect } from "react-router-dom";

export const action = async ({ request }: { request: Request }) => {
  const data = await request.formData();
  const dataObj = Object.fromEntries(data);

  const res = await fetch(`/api/sub`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(dataObj),
  });

  console.log(dataObj);
  if (!res.ok) throw res;
  return redirect("/");
};

export default function FormComp(): JSX.Element {
  const actionData = useActionData();
  console.log(actionData);
  return (
    <>
      <Form
        className="flex flex-col justify-center items-center gap-2"
        method="POST"
      >
        <h1 className="text-2xl font-bold text-teal-500">Form</h1>
        <input
          type="text"
          placeholder="Full Name"
          name="name"
          className="input input-bordered w-full max-w-xs"
        />

        <input
          type="email"
          placeholder="Email"
          name="email"
          className="input input-bordered w-full max-w-xs"
        />

        <input
          type="password"
          placeholder="Password"
          name="password"
          className="input input-bordered w-full max-w-xs"
        />

        <button type="submit" className="btn btn-active btn-neutral">
          Submit
        </button>
      </Form>
    </>
  );
}
