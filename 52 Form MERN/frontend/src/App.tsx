import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route,
} from "react-router-dom";
import HomePage from "./pages/HomePage";
import Error from "./pages/Error";
import { action as formAction} from './components/FormComp'

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/">
        <Route
          index
          element={<HomePage />}
          errorElement={<Error />}
          action = {formAction}
        />
      </Route>
    </>
  )
);

export default function App() {
  return <RouterProvider router={router} fallbackElement={<Error />}/>;
}
