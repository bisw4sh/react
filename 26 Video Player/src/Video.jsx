import sample from './assets/video.mp4'
const Video = () => {
  return (
    <>
      <video width="960" height="540" controls className="videoContainer">
      <source src={sample} type="video/mp4" className="source"/>
      Your browser does not support the video tag.
      </video>
    </>
  )
}

export default Video