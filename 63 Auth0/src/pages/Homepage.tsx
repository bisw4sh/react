import { useAuth0 } from "@auth0/auth0-react";

export default function Homepage() {
  const { isAuthenticated, user: authUser } = useAuth0();

  return isAuthenticated && authUser ? (
    <div>Welcome {authUser.name}</div>
  ) : (
    <>The purpose of this project is to showcase auth with Auth0</>
  );
}
