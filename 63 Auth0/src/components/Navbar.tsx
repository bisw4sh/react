import { Outlet, NavLink } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";

export default function Navbar() {
  const { isAuthenticated, user } = useAuth0();

  return (
    <div className="px-8 py-4">
      <nav className="w-full flex justify-between">
        <div className="flex gap-4">
          <NavLink
            to="/"
            className="text-teal-400 hover:text-teal-600 hover:scale-105"
          >
            Home
          </NavLink>
          <NavLink
            to="/profile"
            className="text-teal-400 hover:text-teal-600 hover:scale-105"
          >
            Profile
          </NavLink>
        </div>
        <div className="flex justify-between items-center gap-3">
          <button className="btn capitalize">
            {user?.name?.slice(0, 10) ?? "click ->"}
          </button>
          {isAuthenticated ? <LogoutButton /> : <LoginButton />}
        </div>
      </nav>
      <Outlet />
    </div>
  );
}

const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();

  return (
    <button className="btn" onClick={() => loginWithRedirect()}>
      Log In
    </button>
  );
};

const LogoutButton = () => {
  const { logout } = useAuth0();

  return (
    <button
      className="btn"
      onClick={() =>
        logout({ logoutParams: { returnTo: window.location.origin } })
      }
    >
      Log Out
    </button>
  );
};
