import { useCount } from "../utility/store";

export default function HomePage() {
  const count = useCount((state) => state.count);
  return (
    <>
      <div className="min-h-screen flex justify-center items-center gap-4">
        <div className="bg-zinc-900 p-8 rounded-full flex flex-col justify-center items-center gap-4 text-white">
          {count}
        </div>
      </div>
    </>
  );
}
