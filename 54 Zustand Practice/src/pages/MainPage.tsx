import { useCount } from "../utility/store";

export default function MainPage() {
  const count = useCount((state) => state.count);
  const increase = useCount((state) => state.increase);
  const decrease = useCount((state) => state.decrease);
  const clear = useCount((state) => state.clear);
  return (
    <>
      <div className="min-h-screen flex justify-center items-center gap-4">
        <button className="btn btn-success" onClick={increase}>
          Add
        </button>
        <div className="bg-zinc-900 p-8 rounded-full flex flex-col justify-center items-center gap-4 text-white">
          <span>{count}</span>
          <button className="btn btn-warning rounded-full" onClick={clear}>
            Clear
          </button>
        </div>
        <button className="btn btn-error" onClick={decrease}>
          Substract
        </button>
      </div>
    </>
  );
}
