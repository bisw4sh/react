import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";

type Store = {
  count: number;
  increase: () => void;
  decrease: () => void;
  clear: () => void;
};

export const useCount = create<Store>()(
  persist(
    (set) => ({
      count: 0,
      increase: () => set((state) => ({ count: state.count + 1 })),
      decrease: () => set((state) => ({ count: state.count - 1 })),
      clear: () => set({ count: 0 }),
    }),
    {
      name: "countering",
      storage: createJSONStorage(() => localStorage),
    }
  )
);
