import GitHubCalendar from "react-github-calendar";

function App() {
  return (
    <>
      <GitHubCalendar
        username="bisw4sh"
        blockSize={15}
        blockMargin={5}
        color="#c084f5"
        fontSize={16}
        colorScheme="dark"
      />
    </>
  );
}

export default App;
