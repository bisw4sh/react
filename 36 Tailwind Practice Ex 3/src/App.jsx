const App = () =>{

  return (
  <div className="h-screen">

    {/* First Screen */}
    <div className="bg-green-600 opacity-75 h-2/5 flex flex-col justify-center items-center gap-7">
      <h1 className="text-5xl font-bold text-stone-100	">Tailwind CSS</h1>
      <h2 className="text-3xl font-semibold	text-blue-700 z-10	">Utility-first CSS framework that makes writing CSS a joy</h2>
      <button className="bg-red-800 px-10 py-3 rounded-md text-slate-50 underline underline-offset-2	hover:bg-red-500">BUY BOOK</button>
    </div>

    {/* Second Screen */}

    <div className="h-3/5 bg-pink-300 flex flex-col justify-center items-center gap-16">

      <h1 className="text-3xl font-bold">Avatar</h1>

      <div className="flex flex-col gap-6 sm:flex-row sm:items-center">

            <div>
              <img className="rounded h-32" src="https://images.unsplash.com/photo-1556740738-b6a63e27c4df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=448&q=80" alt="" />
            </div>
            <div>
              <h1 className="uppercase text-blue-700 font-bold font-sans">Marketing</h1>
              <span className="font-semibold text-xl hover:underline cursor-pointer">Finding customers for your new business</span>
              <article className="font-light	text-md">Getting a new business off the ground is a lot of hard work. Here are five ideas you can use to find your first customers.</article>
            </div>
      </div>

    </div>

    {/* Third Screen */}
    <div className="bg-yellow-200 h-2/6 flex flex-col justify-center items-center gap-8">
      <h1 className="text-3xl font-bold text-gray-700">Responsive Screens</h1>
      <div className="bg-yellow-400 h-32 w-32 sm:bg-red-500 md:bg-pink-50 lg:bg-lime-400 xl:bg-zinc-700"></div>
    </div>

  </div>
  )
}

export default App
