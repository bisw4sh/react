import Dark from './Dark.jsx'
import White from './White.jsx'
import Blue from './Blue.jsx'
const App = () => {

  return (
    <div className='bg-cyan-900'>
    <Dark/>
    <White/>
    <Blue/>
    </div>
  )
}

export default App