import { AiFillAppstore, AiFillHome, AiOutlineSearch, AiFillSetting, AiFillMessage, AiOutlineUser } from "react-icons/ai";
import { BsFillFileBarGraphFill, BsFiletypeDoc, BsFillCartFill } from "react-icons/bs";

const White = () => {
  return (
    <div className="h-screen dark-sidebar flex flex-col w-max bg-gray-100 text-gray-500 font-sans text-xl px-5 py-3 font-semibold justify-start gap-6 relative">

        <div className="the-app flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <AiFillAppstore />
            <span className="max-sm:invisible">The App</span>
        </div>

        <hr className="opacity-50"/>

        <div className="dashboard flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <AiFillHome />
            <span className="max-sm:invisible">Dashboard</span>
        </div>

        <div className="search flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <AiOutlineSearch />
            <span className="max-sm:invisible">Search</span>
        </div>

        <div className="insights flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <BsFillFileBarGraphFill />
            <span className="max-sm:invisible">Insights</span>
        </div>

        <div className="docs flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <BsFiletypeDoc />
            <span className="max-sm:invisible">Docs</span>
        </div>

        <hr className="opacity-25"/>

        <div className="products flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <BsFillCartFill />
            <span className="max-sm:invisible">Products</span>
        </div>

        <div className="settings flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <AiFillSetting />
            <span className="max-sm:invisible">Settings</span>
        </div>

        <div className="messages flex items-center hover:bg-slate-400 p-3 gap-2 rounded-md ">
            <AiFillMessage />
            <span className="max-sm:invisible">Messages</span>
        </div>


        <div className="box-border account flex items-center hover:bg-slate-400 p-3 pr-3 gap-2 rounded-md ">
            <AiOutlineUser />
            <span className="max-sm:invisible">Account</span>
        </div>

        <div className="absolute left-0 bottom-0 bg-indigo-700 opacity-10 h-20 w-full">

        </div>
        
    </div>
  )
}

export default White