import { Button } from "@nextui-org/button";
import { ThemeSwitcher } from "./components/ThemeSwitcher";
import ButtonRipple from "./components/ButtonRipple";
import Accordion from "./components/Accordion";
import AutoComplete from "./components/AutoComplete";
import DropDown from "./components/DropDown";
import DropDownVarient from "./components/DropDownVarient";
import Images from "./components/Images";
import Inputs from "./components/Inputs";
import InputPassword from "./components/InputPassword";
import Modal from "./components/Modal";
import PaginationLoop from "./components/PaginationLoop";
import PopOver from "./components/PopOver";
import Skeleton from "./components/Skeleton";
import Snippet from "./components/Snippets";
import Tabs from "./components/Tabs";

export default function App() {
  return (
    <div className="py-4 flex justify-center items-center flex-col gap-4">
      <h1 className="text-3xl font-bold underline">Hello world!</h1>
      <Button>Press me</Button>
      <div className="bg-primary-500 text-primary-50 rounded-small px-unit-2 py-unit-1">
        This is a primary color box
      </div>
      <div className="flex gap-4">
        <Button color="secondary" variant="solid">
          Solid
        </Button>
        <Button color="secondary" variant="ghost">
          Ghost
        </Button>
        <div className="flex flex-wrap gap-4 items-center">
          <Button color="primary" variant="solid">
            Solid
          </Button>
          <Button color="primary" variant="faded">
            Faded
          </Button>
          <Button color="primary" variant="bordered">
            Bordered
          </Button>
          <Button color="primary" variant="light">
            Light
          </Button>
          <Button color="primary" variant="flat">
            Flat
          </Button>
          <Button color="primary" variant="ghost">
            Ghost
          </Button>
          <Button color="primary" variant="shadow">
            Shadow
          </Button>
        </div>
      </div>
      <ThemeSwitcher />
      <ButtonRipple />
      <div className="w-1/3">
        <Accordion />
      </div>
      <div className="flex justify-center items-center">
        <AutoComplete />
      </div>
      <DropDown />
      <DropDownVarient />
      <Images />
      <Inputs />
      <InputPassword />
      <Modal />
      <PaginationLoop />
      <PopOver />
      <Skeleton />
      <Snippet />
      <Tabs />
    </div>
  );
}
