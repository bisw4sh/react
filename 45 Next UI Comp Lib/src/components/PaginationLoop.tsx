import { Pagination } from "@nextui-org/react";

export default function App() {
  return (
    <Pagination loop showControls color="primary" total={5} initialPage={1} />
  );
}
