import { Image } from "@nextui-org/react";

export default function App() {
  return (
    <div className="w-full flex flex-wrap gap-8 ">
      <Image
        width={300}
        alt="NextUI hero Image"
        src="https://nextui-docs-v2.vercel.app/images/hero-card-complete.jpeg"
      />
      <Image
        isBlurred
        width={240}
        src="https://nextui-docs-v2.vercel.app/images/album-cover.png"
        alt="NextUI Album Cover"
        className="m-5"
      />
      <Image
        isZoomed
        width={240}
        alt="NextUI Fruit Image with Zoom"
        src="https://nextui-docs-v2.vercel.app/images/fruit-1.jpeg"
      />
      <Image
        width={300}
        height={200}
        alt="NextUI hero Image with delay"
        src="https://app.requestly.io/delay/5000/https://nextui-docs-v2.vercel.app/images/hero-card-complete.jpeg"
      />
      <Image
        width={300}
        height={200}
        src="https://app.requestly.io/delay/1000/https://nextui-docs-v2.vercel.app/images/fruit-4.jpeg"
        fallbackSrc="https://via.placeholder.com/300x200"
        alt="NextUI Image with fallback"
      />
    </div>
  );
}
