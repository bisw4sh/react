import JOKES from "./JOKES"

const JokeComp = (props) => {
  return (
    <div>
      <div>Question: {props.question}</div>
      <div>Answer: {props.answer}</div>
    </div>
  )
}

function App() {
  const jokesElements = JOKES().map((joke, index) => (
    <JokeComp
      key={index}
      question={joke.question}
      answer={joke.answer}
    />
  ))

  return <div>{jokesElements}</div>
}

export default App