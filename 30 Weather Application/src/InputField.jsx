import {useState} from 'react'

const InputField = ({setBaseUrl }) => {
const [query, setQuery] = useState('')
const handleChange = (e) => setQuery(e.target.value)

const handleSubmit = (e) => {
    e.preventDefault()
    setBaseUrl(`http://api.weatherapi.com/v1/current.json?key=4577afa28ecc4fca90a73442232410&q=${query}`)
    setQuery('')
}    

  return (
    <div className="InputField">
        <form onSubmit={handleSubmit}>
            <input type="text"  placeholder="...Place, Region, Country" value={query} onChange={handleChange}/>
            <button type="submit">Search</button>
        </form>
    </div>
  )
}

export default InputField