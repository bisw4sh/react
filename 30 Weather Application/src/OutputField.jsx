const OutputField = ({ fetchedData }) => {
    return (
      <div className="OutputField">
        {fetchedData && fetchedData.location ? (<>
          <h3>{fetchedData.location.name}</h3>
          Coordinated = ({fetchedData.location.lat}, {fetchedData.location.lon})
          <br />
          Time = {fetchedData.location.localtime}
          <br />
          Temperature = {fetchedData.current.temp_c} <sup>o</sup>C
        </>
        ) : (
          <h2>Loading</h2>
        )}
      </div>
    );
  }
  
export default OutputField;  