import { useState , useEffect} from 'react';
import InputField from "./InputField.jsx";
import OutputField from "./OutputField.jsx";

const App = () => {
  const [baseUrl, setBaseUrl] = useState("http://api.weatherapi.com/v1/current.json?key=4577afa28ecc4fca90a73442232410&q=pokhara")
  const [fetchedData, setFetchedData] = useState({})

useEffect(() =>{
  (async() => {
    try {
      const data = await fetch(baseUrl)
      const jsonData = await data.json()
      setFetchedData(jsonData)
    } 
    catch (error) {
      console.log("This is from here")
      console.log(error.message)
    }
  })()
  }, [baseUrl])
  
  return (
    <>
      <InputField setBaseUrl={setBaseUrl} />
      <OutputField fetchedData={fetchedData}/>
    </>
  )
}

export default App