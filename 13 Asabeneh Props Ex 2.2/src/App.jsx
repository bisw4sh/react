import "./App.css"

const Input = ({typeInp, Holder}) => {
  return (
    <input type={typeInp} placeholder={Holder} />
  )
}

const Button = ({Holder}) => {
  return(
    <button>{Holder}</button>
  )
}

function App() {
  return (
    <>
      <h2>SUBSCRIBE</h2>
      <h3>Sign up with your email to receive news and updates.</h3>
      <div className="Container">
        <Input type="text" Holder="First name" />
        <Input type="text" Holder="Last name" />
        <Input type="email" Holder="Email" />
      </div>
      <Button Holder="Subscribe"/>
    </>
  )
}

export default App