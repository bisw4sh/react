import { useState, useEffect } from "react";

function App() {
  const [theme, setTheme] = useState(null);

  useEffect(() => {
    if(localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)){
      setTheme('dark')
    }
    else {
      setTheme('light')
    }
  }, [])

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark")
    } else {
      document.documentElement.classList.remove("dark")
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem('theme', `${theme === "dark" ? "light" : "dark"}`)
  }

  return (
    <div className="h-screen bg-white dark:bg-black flex justify-center items-center">
      <button className="bg-rose-500 p-3 rounded-xl text-xl font-bold text-zinc-100" onClick={handleThemeSwitch}>
        Dark Mode
      </button>
    </div>
  );
}

export default App;