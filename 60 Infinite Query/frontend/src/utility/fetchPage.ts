interface MOCK_DATA {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  ip_address: string;
}

const fetchPage = async ({
  pageParam,
}: {
  pageParam: number;
}): Promise<{
  items: MOCK_DATA[];
  currentPage: number;
  nextPage: number | null;
}> => {
  const res = await fetch("/api", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ currentPage: pageParam }),
  });

  const collectedData = await res.json();
  return {
    items: collectedData,
    currentPage: pageParam,
    nextPage: pageParam + 10 < 1000 ? pageParam + 10 : null,
  };
};

export default fetchPage;
