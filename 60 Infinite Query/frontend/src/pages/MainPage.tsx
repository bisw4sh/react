import { useInfiniteQuery } from "@tanstack/react-query";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import fetchPage from "../utility/fetchPage";

const MainPage = () => {
  const { fetchNextPage, isFetchingNextPage, data, error, status } =
    useInfiniteQuery({
      queryKey: ["infos"],
      queryFn: fetchPage,
      initialPageParam: 0,
      getNextPageParam: (lastPage) => lastPage.nextPage,
      maxPages: 1000,
    });

  const { ref, inView } = useInView();

  useEffect(() => {
    if (inView && !isFetchingNextPage) {
      fetchNextPage();
    }
  }, [fetchNextPage, inView, isFetchingNextPage]);

  return status === "pending" ? (
    <div>Loading...</div>
  ) : status === "error" ? (
    <div>{error.message}</div>
  ) : (
    <div className="flex flex-col gap-2">
      <div className="overflow-x-auto">
        <table className="table">
          {/* head */}
          <thead>
            <tr>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Gender</th>
              <th>IP Address</th>
            </tr>
          </thead>
          <tbody>
            {/* Table Body */}
            {data?.pages?.map((page, pageIndex) =>
              page?.items?.map((item, idx) => (
                <tr key={idx + pageIndex}>
                  <td>{item.id}</td>
                  <td>{item.first_name}</td>
                  <td>{item.last_name}</td>
                  <td>{item.email}</td>
                  <td>{item.gender}</td>
                  <td>{item.ip_address}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
      <div ref={ref}>{isFetchingNextPage && "Loading..."}</div>
    </div>
  );
};

export default MainPage;
