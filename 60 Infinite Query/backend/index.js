import express from "express";
import { MOCK_DATA } from "./public/MOCK_DATA.js";
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const retData = (idx) =>
  MOCK_DATA.filter((item) => item.id > idx && item.id <= idx + 10);

app.post("/api", async (req, res) => {
  const data = retData(parseInt(req.body.currentPage));
  res.json(data);
});

app.listen(8080, console.log("server running @ http://localhost:8080"));
