import { useState, useEffect } from "react";
import { MdDarkMode, MdSunny } from "react-icons/md";

function ThemeSwitcher() {
  const [theme, setTheme] = useState<"dark" | "light" | null>(null);

  useEffect(() => {
    if (
      localStorage.theme === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }, []);

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
    localStorage.setItem("theme", `${theme === "dark" ? "light" : "dark"}`);
  };

  return (
    <button onClick={handleThemeSwitch} className="text-3xl">
      {theme === "dark" ? (
        <MdDarkMode className="fill-blue-400" />
      ) : (
        <MdSunny className="fill-yellow-500" />
      )}
    </button>
  );
}

export default ThemeSwitcher;
