import { Input, Button } from "@nextui-org/react";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import TaskAcc from "./component/TaskAcc";

interface TaskType {
  title: string;
  id: string;
}

export default function App() {
  const [tasks, setTasks] = useState<TaskType[]>(
    JSON.parse(localStorage.getItem("tasks") || "[]")
  );

  const [task, setTask] = useState<string>("");

  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    setTask(e.currentTarget.value);
  };

  const handleSubmit = (
    e: React.SyntheticEvent<HTMLFormElement, SubmitEvent>
  ) => {
    e.preventDefault();
    const newTask: TaskType = { title: task, id: uuidv4() };
    const updatedTasks = [...tasks, newTask];
    setTasks(updatedTasks);
    setTask("");
    localStorage.setItem("tasks", JSON.stringify(updatedTasks));
  };



  return (
    <div
      className={`relative px-4 md:px-16 py-4`}
    >
      <h1 className="text-3xl font-bold text-center">TODOs App</h1>
      <form
        action=""
        onSubmit={handleSubmit}
        className="flex gap-5 justify-center items-center"
      >
        <Input
          type="text"
          variant={"underlined"}
          label="Enter your task"
          labelPlacement="outside"
          className="w-2/3"
          value={task}
          onChange={handleChange}
          isRequired={true}
        />
        <Button
          color="default"
          variant="ghost"
          radius="sm"
          fullWidth={false}
          type="submit"
        >
          Add
        </Button>
      </form>
      <TaskAcc tasks={tasks} setTasks={setTasks} />
    </div>
  );
}
